#!/usr/bin/env bash

set -eax

COMMIT="6cf17303230fed52b1d62eed7e49b526603d02b6"

mkdir drawio-desktop \
    && cd drawio-desktop \
    && git init \
    && git remote add origin "https://github.com/jgraph/drawio-desktop.git" \
    && git fetch --depth 1 origin "$COMMIT" \
    && git checkout "$COMMIT"   # alternatively: git checkout FETCH_HEAD

