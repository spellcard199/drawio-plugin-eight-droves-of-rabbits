/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { DiagramData } from "../util/data/DiagramData";
import { readFileContent } from "../util/platformgeneric/read";
import { pathOrUrlToUrl } from "../util/platformgeneric/url";
import { LinkData } from "./LinkData";

export function getLinksInFile(
  filePathOrUrl: string
): Promise<Map<DiagramData, LinkData[]> | Error> {
  const fileUrl = pathOrUrlToUrl(filePathOrUrl);
  const fileContentPromise: Promise<string | Error> = readFileContent(
    fileUrl.href
  );
  return fileContentPromise.then((fileContentOrErr: string | Error) => {
    if (!(fileContentOrErr as Error).message) {
      const fileContent: string = fileContentOrErr as string;
      const diagramData: DiagramData[] =
        DiagramData.newFromMxFileContentString(fileContent);
      const res: Map<DiagramData, LinkData[]> = new Map();
      if (diagramData !== null) {
        for (let dd of diagramData) {
          res.set(dd, dd.extractLinks(fileUrl));
        }
        return res;
      } else {
        return new Error(
          "[ERROR - getLinksInDiagramFile] Cound not find diagram elements in file: " +
            filePathOrUrl
        );
      }
    } else {
      return new Error(
        "[ERROR - getLinksInDiagramFile] Could not read file: " + filePathOrUrl
      );
    }
  });
}
