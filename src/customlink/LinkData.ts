/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { PropData } from "../util/data/PropData";
import { newAnchorFromURL, newAnchorsFromParseHTML } from "../util/parse/html";
import { getEditorFileUrlOrPath } from "../util/platformgeneric/drawio/editor";
import { pathOrUrlToUrl } from "../util/platformgeneric/url";
import { EditorUi } from "../util/types/drawio/EditorUi";

export class LinkData {
  readonly propData: PropData;
  // One reason for why this class exists is that if we stick to
  // HTMLAnchorElement and we want to build new documents from the anchors,
  // when the anchor is inserted in a new document with a different baseURI
  // header the absolute href of the anchor also changes: instead, we need
  // absolute URLs to remain constant.
  readonly href: string;
  readonly innerText: string;
  readonly attributes: Map<string, string>;

  private constructor(propData: PropData, anchor: HTMLAnchorElement) {
    this.propData = propData;
    this.href = anchor.href;
    this.innerText = anchor.innerText;
    // From NamedNodeMap to just Map
    this.attributes = new Map(
      Array.from(anchor.attributes, (attr: Attr) => [attr.name, attr.value])
    );
  }

  static newFromPropData(propData: PropData, baseURL: URL): LinkData[] {
    const aElems: HTMLAnchorElement[] =
      propData.propName === "link" && propData.propValue !== ""
        ? [newAnchorFromURL(baseURL, propData.propValue)]
        : newAnchorsFromParseHTML(baseURL, propData.propValue);
    return aElems.map((a: HTMLAnchorElement) => new LinkData(propData, a));
  }

  static newFromCell(cell: mxCell, baseURL: URL): LinkData[] {
    return PropData.newFromCell(cell).flatMap((propData: PropData) =>
      LinkData.newFromPropData(propData, baseURL)
    );
  }

  static newFromGraphModel(model: mxGraphModel, baseURL: URL): LinkData[] {
    // Old: const allCells = graph.getModel().getDescendants(graph.getDefaultParent());
    return Object.values(model.cells).flatMap((cell: mxCell) =>
      LinkData.newFromCell(cell, baseURL)
    );
  }

  static newFromEditorUi(editorUi: EditorUi, cells: mxCell[]): LinkData[] {
    // Default to window.location, so at least absolute links would work even
    // with new unsaved files.
    const baseURL = pathOrUrlToUrl(
      getEditorFileUrlOrPath(editorUi) || window.location.href
    );
    return cells.flatMap((cell: mxCell) => LinkData.newFromCell(cell, baseURL));
  }

  static newFromEditorUiSelection(editorUi: EditorUi): LinkData[] {
    const graph: mxGraph = editorUi.editor.graph;
    const cells: mxCell[] = graph.getSelectionCells();
    return LinkData.newFromEditorUi(editorUi, cells);
  }
}
