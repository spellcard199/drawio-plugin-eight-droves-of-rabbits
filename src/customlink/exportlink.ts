/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { extToSVG } from "../util/parse/fileext";
import { LinkData } from "./LinkData";

export function setCellLinkFromDrawioToSVG(cell: mxCell): mxCell {
  const cellHrefs = LinkData.newFromCell(cell, new URL("file://./")).map(
    (ld: LinkData) => {
      // Strip leading file://./ that was inserted in previous
      // step if link was relative.
      var newHref = ld.href.replace(/^(file:\/\/\.\/)/, "");
      if (newHref.endsWith(".drawio") || newHref.endsWith(".drawio.xml")) {
        newHref = extToSVG(newHref);
      }
      return newHref;
    }
  );
  if (cellHrefs.length > 0) {
    cell.setAttribute("link", cellHrefs[0]);
  }
  return cell;
}

export function setGraphLinksFromDrawioToSVG(
  graph: mxGraph,
  mutateGraph?: boolean
): mxGraph {
  // TODO: make this a pure function.
  // You can use this function to other functions that take a
  // `customGraphTransform' to convert custom links extensions from drawio to
  // SVG.

  const g = mutateGraph
    ? graph
    : //@ts-ignore
      new Graph(document.createElement("div"), graph.model);
  g.model
    .getDescendants(g.getDefaultParent())
    .forEach(setCellLinkFromDrawioToSVG);
  return g;
}
