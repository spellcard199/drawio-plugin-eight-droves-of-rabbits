/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { getEditorFileUrlOrPath } from "../util/platformgeneric/drawio/editor";
import { isURLAccessible, pathOrUrlToUrl } from "../util/platformgeneric/url";
import { EditorUi } from "../util/types/drawio";
import { LinkData } from "./LinkData";

export async function checkLinksInPage(
  editorUi: EditorUi,
  checkRemote: boolean = false,
  brokenLinkCallback: (editorUi: EditorUi, ld: LinkData) => void = (
    _eui,
    _ld
  ) => {},
  validLinkCallback: (editorUi: EditorUi, ld: LinkData) => void = (
    _eui,
    _ld
  ) => {}
) {
  const fileURLOrPath: string = getEditorFileUrlOrPath(editorUi);
  const fileURL = pathOrUrlToUrl(fileURLOrPath);
  const graph: mxGraph = editorUi.editor.graph;
  const lds = LinkData.newFromGraphModel(graph.model, fileURL);
  return lds.forEach((ld: LinkData) =>
    isURLAccessible(new URL(ld.href), checkRemote)
      .then((isAccessible: boolean) => {
        if (isAccessible) {
          validLinkCallback(editorUi, ld);
        } else {
          brokenLinkCallback(editorUi, ld);
        }
      })
      .catch((reason: any) => {
        throw new Error(reason);
      })
  );
}

export async function selectCellsWithBrokenLinks(editorUi: EditorUi) {
  const g: mxGraph = editorUi.editor.graph;
  g.setSelectionCells([]);
  checkLinksInPage(editorUi, false, (_euim, ld) => {
    console.log(ld.href);
    g.setSelectionCells(g.getSelectionCells().concat([ld.propData.inCell]));
  });
}
