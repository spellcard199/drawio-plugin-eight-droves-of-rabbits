/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { LinkData } from "../../customlink/LinkData";

export class PropData {
  readonly inCell: mxCell;
  readonly propName: string;
  readonly propValue: string;

  private constructor(inCell: mxCell, propName: string, propValue: string) {
    this.inCell = inCell;
    this.propName = propName;
    this.propValue = propValue;
  }

  static newFromCell(cell: mxCell): PropData[] {
    const propMap: Map<string, string> = new Map();
    if (typeof cell.value === "undefined") {
      // do nothing
    } else if (cell.value.constructor.name === "String") {
      // TODO: try if === can be replaced by instanceof
      propMap.set("label", cell.value);
    } else if (cell.value.constructor.name === "Element") {
      Array.from((cell.value as Element).attributes).forEach((attr: Attr) =>
        propMap.set(attr.name, attr.value)
      );
    }
    return Array.from(
      propMap.entries(),
      ([propName, propValue]) => new PropData(cell, propName, propValue)
    );
  }

  static newFromGraphModel(model: mxGraphModel): PropData[] {
    // Old: const allCells = model.getDescendants(graph.getDefaultParent());
    return Object.values(model.cells).flatMap((cell: mxCell) =>
      PropData.newFromCell(cell)
    );
  }

  extractLinks(baseURL: URL): LinkData[] {
    return LinkData.newFromPropData(this, baseURL);
  }
}
