/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { LinkData } from "../../customlink/LinkData";
import {
  parseGraphModelsFromElement,
  parseInflateMxfile,
} from "../parse/drawio";

export class DiagramData {
  id: string;
  name: string;
  model: mxGraphModel;

  private constructor(id: string, name: string, model: mxGraphModel) {
    this.id = id;
    this.name = name;
    this.model = model;
  }

  static newFromDiagramElement(diagram: Element): DiagramData {
    if (diagram.tagName !== "diagram") {
      throw new Error("[ERROR] diagram.tagName !== 'diagram'");
    }
    const modelElems = diagram.getElementsByTagName("mxGraphModel");
    if (modelElems.length !== 1) {
      throw new Error(
        "[ERROR] diagram.getElementsByTagName('mxGraphModel').length !== 1: " +
          modelElems.length
      );
    }
    // (I think) 1 "diagram" elem should contain only 1 "mxGraphModel" elem.
    const modelElem = modelElems[0];
    return new DiagramData(
      diagram.getAttribute("id"),
      diagram.getAttribute("name"),
      parseGraphModelsFromElement(modelElem)
    );
  }

  static newFromMxFileXMLDocument(xmlDoc: XMLDocument): DiagramData[] {
    const codec = new mxCodec();
    const diagramElems: Element[] = Array.from(
      xmlDoc.getElementsByTagName("diagram")
    );
    return diagramElems.map(this.newFromDiagramElement);
  }

  static newFromMxFileContentString(
    xmlFileContent: string
  ): DiagramData[] | null {
    const mxFileElem: Element | null = parseInflateMxfile(xmlFileContent);
    return mxFileElem !== null
      ? Array.from(mxFileElem.getElementsByTagName("diagram")).map(
          DiagramData.newFromDiagramElement
        )
      : null;
  }

  extractLinks(baseURL: URL): LinkData[] {
    return LinkData.newFromGraphModel(this.model, baseURL);
  }
}
