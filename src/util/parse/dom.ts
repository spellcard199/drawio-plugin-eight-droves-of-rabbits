/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export const domParserInstance: DOMParser = newDOMParser();

export function newDOMParser(): DOMParser {
  if (typeof window != "undefined" && window.DOMParser) {
    return new DOMParser();
  } else if (typeof require !== 'undefined') {
    // This should only happen during testing since both electron app
    // and web app have window.DOMParser;
    // the variable is defined
    const jsdom = require("jsdom");
    const { JSDOM } = jsdom;
    return new JSDOM().window.DOMParser();
  }
}

export function getTextContent(node: Node): string {
  let textContent = "";
  if (node != null) {
    const key = node.textContent === undefined ? "text" : "textContent";
    textContent = node[key];
  }
  return textContent;
}

export function cloneNodeRec(node: Node, clonedParentOrUndefined?: Node) {
  var cloned = node.cloneNode();
  if (clonedParentOrUndefined !== undefined) {
    clonedParentOrUndefined.appendChild(cloned);
  }
  for (var i = 0; i < node.childNodes.length; i++) {
    cloneNodeRec(node.childNodes[i], cloned);
  }
  return cloned;
}
