/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { domParserInstance } from "./dom";

export function parseXml(
  xmlStr: string,
  domParser: DOMParser = domParserInstance
): XMLDocument {
  return domParser.parseFromString(xmlStr, "application/xml");
}

export function formatXmlStr(
  xml: string,
  tab: string = "  ", // tab = optional indent value, default is tab (\t)
  formatted: string = "",
  indent: string = ""
): string {
  // Adapted from from:
  // https://stackoverflow.com/questions/376373/pretty-printing-xml-with-javascript
  // - https://stackoverflow.com/a/49458964
  xml.split(/>\s*</).forEach(function (node) {
    if (node.match(/^\/\w/)) indent = indent.substring(tab.length); // decrease indent by one 'tab'
    formatted += indent + "<" + node + ">\r\n";
    if (node.match(/^<?\w[^>]*[^\/]$/)) indent += tab; // increase indent
  });
  return formatted.substring(1, formatted.length - 3).replace(/\r\n/g, "\n");
}
