/**
 * Copyright (C) 2020,2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { domParserInstance } from "./dom";

export function makeHTMLDoc(
  baseURL: URL,
  fromString: string,
  domParser: DOMParser = domParserInstance
): HTMLDocument {
  const htmlDoc = domParser.parseFromString(fromString, "text/html");
  const baseElem = htmlDoc.createElement("base");
  baseElem.href = baseURL.href;
  htmlDoc.head.appendChild(baseElem);
  return htmlDoc;
}

export function newAnchorsFromParseHTML(
  baseURL: URL,
  s: string
): Array<HTMLAnchorElement> {
  const htmlDoc = makeHTMLDoc(baseURL, s);
  const aElems: HTMLCollectionOf<HTMLAnchorElement> =
    htmlDoc.getElementsByTagName("a");
  return Array.from(aElems);
}

export function newAnchorFromURL(
  baseURL: URL,
  href: string
): HTMLAnchorElement {
  const htmlDoc = makeHTMLDoc(baseURL, "");
  const anchorElem = htmlDoc.createElement("a");
  anchorElem.href = href;
  return anchorElem;
}
