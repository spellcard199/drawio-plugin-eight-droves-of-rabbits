/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { bytesToString } from "./bytes";
import { cloneNodeRec, domParserInstance, getTextContent } from "./dom";
import { parseXml } from "./xml";

const codecInstance = new mxCodec();

function decodeInflateURIDecodeString(data: string): string {
  data = atob(data);
  // @ts-ignore: because in webpack.config.js it's declared as an external
  // package, we can't import pako. At runtime it would become undefined.
  data = bytesToString(pako.inflateRaw(data));
  data = decodeURIComponent(data);
  return data;
}

export function getRawXmlFileDataClone(
  mxfileElem: Element
): Element | undefined {
  try {
    const mxfileElemNew: Element = cloneNodeRec(
      mxfileElem,
      undefined
    ) as Element;
    if (mxfileElemNew != null && mxfileElemNew.nodeName === "mxfile") {
      const diagrams: HTMLCollectionOf<Element> =
        mxfileElemNew.getElementsByTagName("diagram");
      for (var i = 0; i < diagrams.length; i++) {
        const diagram = diagrams[i];
        const decodedInflatedURIDecoded: string = decodeInflateURIDecodeString(
          getTextContent(diagram)
        );
        const mxgraphmodelElem: HTMLElement = parseXml(
          decodedInflatedURIDecoded
        ).documentElement;
        diagram.textContent = "";
        diagram.appendChild(mxgraphmodelElem);
      }
      return mxfileElemNew;
    }
  } catch (e) {
    console.log(e);
    return;
  }
}

export function parseInflateMxfile(
  fileContent: string,
  domParser: DOMParser = domParserInstance
): Element | null {
  // TODO:
  // 1. See if line 86 is relevant to find : https://github.com/jgraph/drawio/blob/533720a8c7fd283c68275d81f222e617a34428f6/src/main/webapp/plugins/replay.js
  // 2. _Try to replace this with Graph.decompress_

  // Copied/pasted/adapted from:
  //   https://stackoverflow.com/questions/49376973/open-drow-io-file-in-mx-graph-editor

  // Note:
  // 1. Diagrams.net uses the DEFLATE compression algorithm:
  //   - source: https://stackoverflow.com/questions/59416025/format-of-draw-io-xml-file
  // 2. Why?
  //   - reason: "... a compressed file has a much better chance of writing on struggling google servers."
  //   - source: https://softwarerecs.stackexchange.com/questions/42394/is-there-an-xml-renderer-for-draw-io-graphs
  const xmlDoc: XMLDocument = domParser.parseFromString(
    fileContent,
    "text/xml"
  );
  // TODO: check for 0 or >1 mxfile
  const mxfileElem: Element = xmlDoc.getElementsByTagName("mxfile").item(0);
  const diagramElems: HTMLCollectionOf<Element> =
    mxfileElem.getElementsByTagName("diagram");
  let mxfileElemInflated: Element;
  if (diagramElems.length > 0) {
    const firstDiagram = diagramElems.item(0);
    if (firstDiagram.getElementsByTagName("root").length > 0) {
      // Already inflated
      mxfileElemInflated = mxfileElem;
    } else {
      // Should inflate first
      mxfileElemInflated = getRawXmlFileDataClone(mxfileElem);
    }
    return mxfileElemInflated;
  } else {
    return null;
  }
}

export function parseGraphModelsFromElement(
  mxGraphModelElem: Element,
  codec: mxCodec = codecInstance
) {
  return codec.decode(mxGraphModelElem);
}

export function parseGraphModelsFromMxFile(
  fileContent: string,
  codec: mxCodec = codecInstance
): mxGraphModel[] | null {
  const xmlDoc: Element | null = parseInflateMxfile(fileContent);
  if (xmlDoc !== null) {
    const graphModels: mxGraphModel[] = Array.from(
      xmlDoc.getElementsByTagName("mxGraphModel")
    ).map((mxGraphModelElem: Element) =>
      parseGraphModelsFromElement(mxGraphModelElem, codec)
    );
    return graphModels;
  } else {
    return null;
  }
}
