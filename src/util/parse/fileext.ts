/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export function extToSVG(filePath: string): string {
  return filePath.replace(/\.[^/.]+$/, "") + ".svg";
}
