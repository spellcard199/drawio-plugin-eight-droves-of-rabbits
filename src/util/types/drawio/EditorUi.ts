/**
 * Copyright (C) 2021-2023, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { Editor } from "./Editor";
import { mxKeyHandler } from "./mxKeyHandler";
import { LocalFile } from "./LocalFile";

export declare type EditorUi = {
  editor: Editor;
  currentFile: LocalFile;
  actions: any;
  keyHandler: mxKeyHandler;
  menubar: any;
  menus: any;
  fileLoaded: (localFile: LocalFile) => any;
  openDevTools: () => undefined;
  getFileData: (
    forceXml: boolean,
    forceSvg: boolean,
    forceHtml: boolean,
    embeddedCallback: any,
    ignoreSelection: boolean,
    currentPage: any,
    node: any,
    compact: any,
    file: any,
    uncompressed: boolean
  ) => string;
  onKeyPress: (keyEvt: KeyboardEvent) => void;
  showDialog(
    dialogContainer: HTMLElement,
    x: number,
    y: number,
    todo1: boolean,
    todo2: boolean
  ): void;
};
