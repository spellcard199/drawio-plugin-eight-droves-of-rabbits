/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export declare type DiagramPage = {
  // Type for Diagrams.net's DiagramPage
  node: Element;
  graphModelNode: Element;
  viewState: Object;
  root: mxCell;
};
