/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export declare type Editor = {
  autosave: boolean;
  graph: mxGraph;
  setStatus: (statusMsg: string) => void;
};
