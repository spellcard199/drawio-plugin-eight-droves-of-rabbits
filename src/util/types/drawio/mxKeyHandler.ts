/**
 * Copyright (C) 2023-2023, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export declare class mxKeyHandlerExtended extends mxKeyHandler {
    bindAction: (keyCode: number, ctrlPressed: boolean, actionKey: string, shiftPressed: boolean) => any;
}

// Needed to workaround the fact that the typings that we are using
// say that these have type Array<(event: KeyboardEvent) => void>, but their are
// actually objects:
// - mxKeyHandler.normalKeys
// - mxKeyHandler.controlKeys
// - etc...
type mxKeyHandlerKeys = {[keyCode: number]: (event: KeyboardEvent) => void}

export { mxKeyHandlerExtended as mxKeyHandler, mxKeyHandlerKeys}