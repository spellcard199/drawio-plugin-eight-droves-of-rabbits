/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { EditorUi } from "./EditorUi";

declare global {
    class TextareaDialog {
        constructor(
            eui: EditorUi,
            prompt: string,
            defaultContent: string,
            onApply: (content: string) => void,
            todo1: any | null,
            todo2: any | null,
            width: number,
            height: number,
        )
        textarea: HTMLTextAreaElement
        init: () => void
        // container: HTMLTableElement // more precise, but unsure if always the case
        container: HTMLElement
    }
}

export { }