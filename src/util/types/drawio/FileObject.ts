/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export class FileObject {
  // The mxgraph typings I'm using don't include FileObject. This class surrogates
  // just the the fields and methods I am using.
  path: string;
  name: string;
  // e.g.: "utf-8"
  encoding: string;
  public constructor(filePath: string, fileEncoding: string) {
    this.path = filePath;
    // Substitute that also works in browser for:
    //   path.basename(filePath);
    this.name = filePath.substr(filePath.lastIndexOf("/") + 1);
    this.encoding = fileEncoding;
  }
}
