/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { EditorUi } from "./EditorUi";
import { FileObject } from "./FileObject";

export declare type LocalFile = {
  constructor: (
    ui: EditorUi,
    data: string,
    title: string,
    temp: any,
    fileHandle: any,
    desc: any
  ) => void;
  eventSource: EventSource | undefined;
  fileObject: FileObject;
  // TODO: replace any
  ui: any | undefined;
  data: string;
  shadowData: string;
  // TODO: find out type instead of any
  shadowPages: any | null;
  created: number;
  // TODO?
  // - new LocalFile().stats.constructor.name returns "Object"
  // - Object.keys(new LocalFile().stats) returns:
  //   Array(18) [
  //     "opened", "merged", "fileMerged", "fileReloaded",
  //     "conflicts", "timeouts", "saved", "closed", "destroyed", "joined",
  //     "checksumErrors", "bytesSent", "bytesReceived", "msgSent",
  //     "msgReceived", "cacheHits", "cacheMiss", "cacheFail"
  //   ]
  stats: Object;
  title: string | undefined;
  // TODO: what are the alternatives to "device"
  mode: "device" | string;
  // TODO: replace any
  fileHandle: any | undefined;
  // TODO: replace any
  desc: any | undefined;
  setModified: (modified: boolean) => void;
};
