/**
 * Copyright (C) 2022-2023, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { GraphExtended } from "../mxgraph/types/graph"

export declare class WindowExtended extends Window {
    mxIsElectron: "renderer" | undefined
    Graph: GraphExtended
}

const win: WindowExtended = (window as unknown) as WindowExtended

export { win as window }