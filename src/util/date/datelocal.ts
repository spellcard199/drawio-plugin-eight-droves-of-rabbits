/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export function dateGetLocalISOString() {
    // https://stackoverflow.com/questions/3552461/how-to-format-a-javascript-date
    var date = new Date();
    date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
    return date.toISOString()
}