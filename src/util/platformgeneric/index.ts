/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

// For porbatility, code outside of `platformgeneric' and `platformspecific'
// should only call functions from `platformgeneric'.

export * as drawio from "./drawio";
export * as read from "./read";
export * as url from "./url";
