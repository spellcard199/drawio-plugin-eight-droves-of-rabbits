/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { isElectron } from "../electron";
import { readUrlUsingBrowser } from "../platformspecific/browser/readurl";
import { readUrlUsingNodeJS } from "../platformspecific/nodejs/readurl";
import { resolve } from "../platformspecific/nodejs/bridgewrap/path";
import { fileURLToPath } from "../platformspecific/nodejs/bridgewrap/url";
import { existsSync } from "../platformspecific/nodejs/bridgewrap/fs";

// Note: mxGraph already has fields and methods to work with urls:
// - fields:
//   - graph.domainPathUrl
//       file:///tmp/.mount_draw.iUMM4l1/resources/app.asar/
//   - graph.domainUrl
//       file://
//   - graph.absoluteUrlPattern
//       /^(?:[a-z]+:)?\/\//i
//   - graph.baseUrl
//      file:///tmp/.mount_draw.iUMM4l1/resources/app.asar/index.html?dev=0&test=0&gapi=0&db=0&od=0&gh=0&gl=0&tr=0&browser=0&picker=0&mode=device&export=https%3A%2F%2Fexp.draw.io%2FImageExport4%2Fexport
// - methods:
//   - graph.getAbsoluteUrl
//   - graph.isRelativeUrl
//   - graph.updateGlobalUrlVariables

export function isLocal(absolutePathOrUrl: string): boolean {
  return (
    absolutePathOrUrl.startsWith("file:") || absolutePathOrUrl.startsWith("/")
  );
}

export function getLocalFilePath(absolutePathOrUrl: string): string {
  return absolutePathOrUrl.startsWith("file:")
    ? fileURLToPath(absolutePathOrUrl)
    : absolutePathOrUrl;
}

export function getParentUrl(urlStr: string): string {
  return urlStr.substring(0, urlStr.lastIndexOf("/") + 1);
}

export function pathToFileURL(path: string): URL {
  // Since Electron cannot bridge symbols, using own function (for now)
  // instead of node's url.pathToFileURL to avoid having to write a wrapper
  // function and bridge it in Electron's preload.js:
  // - https://stackoverflow.com/questions/48638080/get-access-to-symbolcontext-properties-of-node-js-url-object
  return new URL(
    // https://tools.ietf.org/html/rfc3986#section-3.3
    encodeURI('file://' + path).replace(/[?#]/g, encodeURIComponent)
  )
}

export function pathOrUrlToUrl(pathOrUrl: string): URL {
  let fileUrl: URL;
  if (
    pathOrUrl.startsWith("file:") ||
    pathOrUrl.startsWith("http:") ||
    pathOrUrl.startsWith("https:")
  ) {
    fileUrl = new URL(pathOrUrl);
  } else {
    const absolutePath = resolve(pathOrUrl);
    if (!existsSync(absolutePath)) {
      throw new Error("File does not exist: " + absolutePath);
    }
    fileUrl = pathToFileURL(absolutePath);
  }
  return fileUrl;
}

export async function isURLAccessible(
  targetURL: URL,
  checkRemote: boolean = false
): Promise<Boolean> {
  if (isLocal(targetURL.href)) {
    if (isElectron()) {
      const targetPath = fileURLToPath(targetURL);
      return existsSync(targetPath) ? true : false;
    } else {
      console.log("Can only check local links when used from Node.js.");
      return false;
    }
  } else if (checkRemote) {
    if (isElectron()) {
      return await readUrlUsingNodeJS(targetURL.href)
        .then((_fileContent: string) => true)
        .catch((_reason: any) => false);
    } else {
      return await readUrlUsingBrowser(targetURL.href)
        .then((_fileContent: string) => true)
        .catch((_reason: any) => false);
    }
  } else {
    // Not checking at all: assume link is accessible.
    return true;
  }
}

export { fileURLToPath }