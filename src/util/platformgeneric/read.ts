/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { isElectron } from "../electron";
import { readUrlUsingBrowser } from "../platformspecific/browser/readurl";
import {
  readLocalFileSync,
  readUrlUsingNodeJS,
} from "../platformspecific/nodejs/readurl";
import { isLocal } from "./url";

export async function readRemoteFileAsync(
  absoluteUrl: string
): Promise<string> {
  // Checking if `require' is defined to know if we are in the desktop app
  // or in the we app.
  return isElectron()
    ? readUrlUsingNodeJS(absoluteUrl)
    : readUrlUsingBrowser(absoluteUrl);
}

export async function readFileContent(
  absolutePathOrUrl: string
): Promise<string | Error> {
  // In Desktop App: app can open:
  // - local files
  // - remote files: using node's http/https
  //   (desktop app's CSP does not allow using `fetch')
  // In Browser: app can open:
  // - remote files: given server's CORS allows it
  //   (web app's CSP allows using `fetch')
  // Note:
  // - Gitlab's CORS: doesn't allow `fetch'-ing raw files
  // - Github's CORS: allows `fetch'-ing raw files
  if (isLocal(absolutePathOrUrl)) {
    return readLocalFileSync(absolutePathOrUrl);
  } else {
    return readRemoteFileAsync(absolutePathOrUrl).catch(
      (reason) => new Error(reason)
    );
  }
}
