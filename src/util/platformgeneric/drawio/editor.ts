/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { isElectron } from "../../electron";
import { EditorUi } from "../../types/drawio";

export function getEditorFileUrlOrPath(editorUi: EditorUi): string | undefined {
  let editorFilePath: string;
  const currentFile = editorUi.currentFile;
  if (currentFile && currentFile.fileObject) {
    editorFilePath = currentFile.fileObject.path;
  } else {
    if (isElectron()) {
      editorFilePath = undefined;
    } else {
      const beg = window.location.href.indexOf("#U") + 2;
      const encodedAbsoluteURL = window.location.href.substring(beg);
      const decodedAbsoluteURL = decodeURIComponent(encodedAbsoluteURL);
      editorFilePath = decodedAbsoluteURL;
    }
  }
  return editorFilePath;
}
