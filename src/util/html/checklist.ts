/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

let globalIdCounter = 0

export type ChecklistHeadingLevel = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6'

export type ChecklistSection = {
    headingTitle: string
    formElementId: string | null
    headingLevel: ChecklistHeadingLevel
    checklist: string[]
}

export function checklistToHtmlMkInputAndLabel(
    value: string,
): [HTMLInputElement, HTMLLabelElement] {
    // HTMLInputElement
    const inputElem: HTMLInputElement = document.createElement('input');
    inputElem.type = 'checkbox';
    inputElem.checked = false;
    inputElem.id = "checklist-item-" + globalIdCounter++
    inputElem.value = value
    // HTMLLabelElement
    const labelElem: HTMLLabelElement = document.createElement('label');
    // labelElem['for'] = inputElem.id;
    labelElem.htmlFor = inputElem.id;
    labelElem.innerText = ' ' + inputElem.value;
    return [inputElem, labelElem];
}

export function checklistToHtmlMkForm(section: ChecklistSection): HTMLFormElement {
    // Heading:
    const heading = document.createElement(section.headingLevel)
    heading.innerText = section.headingTitle
    // Section's checklist
    const sectionChecklist: [HTMLInputElement, HTMLLabelElement][]
        = section.checklist.map((value: string) =>
            checklistToHtmlMkInputAndLabel(value))
    // formElem contains: heading, section's checklist
    // https://www.w3schools.com/tags/att_input_type_checkbox.asp
    const formElem: HTMLFormElement = document.createElement('form');
    formElem.id = section.formElementId
    formElem.appendChild(heading)
    sectionChecklist.forEach(
        (inputAndLabelElems: [HTMLInputElement, HTMLLabelElement]) => {
            const inputElem = inputAndLabelElems[0]
            const labelElem = inputAndLabelElems[1]
            formElem.appendChild(inputElem)
            formElem.appendChild(labelElem)
            formElem.appendChild(document.createElement('br'))
        })
    return formElem
}

export function checklistToHtml(checklist: ChecklistSection[]): HTMLDivElement {
    // divElem contains: formElem
    const divElem: HTMLDivElement = document.createElement('div');
    checklist.forEach((section: ChecklistSection) => {
        const formElem = checklistToHtmlMkForm(section);
        divElem.appendChild(formElem)
    })
    divElem.style.overflowY = "scroll"
    divElem.style.height = "100%"
    return divElem
}

export const checklistElemGetLabelsInputsChecked = (checklistElem: HTMLElement) => {
    return Array
        .from(checklistElem.getElementsByTagName("label"))
        .filter(labelElem => (
            document.getElementById(labelElem.htmlFor) as
            HTMLInputElement
        ).checked)
        // Need to trim: whitespace gets inserted at the beginning.
        .map(labelElem => labelElem.innerText.trim())
}