/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export function createElemOverlayDiv(
    doc: Document,
    // it's easier to set alpha to 1 and change transparency with "opacity"
    backgroundColor: string =
        "rgba(255,255,255,1)",   /* White background with opacity */
    // "rgba(1,1,1,1)", // rgb(1,1,1)
    opacity: string = "0.9"
) {
    const div = doc.createElement("div")
    // Adapted from: https://www.w3schools.com/howto/howto_css_overlay.asp
    div.style.position = "fixed"; /* Sit on top of the page content */
    div.style.display = "none"; /* Hidden by default */
    div.style.width = "100%"; /* Full width (cover the whole page) */
    div.style.height = "100%"; /* Full height (cover the whole page) */
    div.style.top = "0";
    div.style.left = "0";
    div.style.right = "0";
    div.style.bottom = "0";
    div.style.backgroundColor = backgroundColor;
    div.style.opacity = opacity;
    div.style.zIndex =
        "2"; /* Specify a stack order in case you're using a different order for other elements */
    // this.div.style.cursor = "pointer"; /* Add a pointer on hover */
    return div
}