/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

// export function divOverlayOn(elem: HTMLElement): HTMLDivElement {
//     // Adapted from: https://www.w3schools.com/howto/howto_css_overlay.asp
//     var newDiv = document.createElement("div");
//     newDiv.style.display = "block"
//     newDiv.style.position = "fixed"; /* Sit on top of the page content */
//     newDiv.style.display = "none"; /* Hidden by default */
//     newDiv.style.width = "100%"; /* Full width (cover the whole page) */
//     newDiv.style.height = "100%"; /* Full height (cover the whole page) */
//     newDiv.style.top = "0";
//     newDiv.style.left = "0";
//     newDiv.style.right = "0";
//     newDiv.style.bottom = "0";
//     newDiv.style["background-color"] = "rgba(0,0,0,0.5)"; /* Black background with opacity */
//     newDiv.style["z-index"] = "2"; /* Specify a stack order in case you're using a different order for other elements */
//     newDiv.style.cursor = "pointer"; /* Add a pointer on hover */
//     // elem.type = "file";
//     elem.style.position = "absolute";
//     elem.style.top = "50%";
//     elem.style.left = "50%";
//     elem.style["font-size"] = "50px";
//     elem.style.color = "white";
//     elem.style.transform = "translate(-50%,-50%)";
//     elem.style["-ms-transform"] = "translate(-50%,-50%)";
//     newDiv.appendChild(elem)
//     newDiv.style.display = "block";
//     document.body.appendChild(newDiv)
//     return newDiv;
// }
//
// export function divOverlayOff(div: HTMLDivElement) {
//     div.style.display = "none";
//     document.body.removeChild(div);
// }
