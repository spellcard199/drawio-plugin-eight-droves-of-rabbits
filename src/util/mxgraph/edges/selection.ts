/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export function getEdgesBetweenCells(cells: mxCell[]): Set<mxCell> {
    const edgesBetweenCells = new Set<mxCell>();
    for (let c of cells) {
        if (!c.edges) { continue }
        for (let e of c.edges) {
            if (e.source && e.target && cells.includes(e.source) && cells.includes(e.target)) {
                edgesBetweenCells.add(e)
            }
        }
    }
    return edgesBetweenCells
}

export function getEdgesOutgoing(cells: mxCell[]) {
    const edgesOutgoing = new Set<mxCell>();
    for (let c of cells) {
        if (!c.edges) { continue }
        for (let e of c.edges) {
            if (e.source === c) {
                edgesOutgoing.add(e)
            }
        }
    }
    return edgesOutgoing
}

export function getEdgesIngoing(cells: mxCell[]) {
    const edgesIngoing = new Set<mxCell>();
    for (let c of cells) {
        if (!c.edges) { continue }
        for (let e of c.edges) {
            if (e.target === c) {
                edgesIngoing.add(e)
            }
        }
    }
    return edgesIngoing
}