/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

type EdgeData = {
    parent: mxCell
    esource: mxCell
    etarget: mxCell
    id?: string
    value?: string
    style?: string
    custom?: any
}

export function reverseEdges(graph: mxGraph, edges: mxCell[]) {
    const model: mxGraphModel = graph.getModel();
    model.beginUpdate();
    try {
        edges.forEach((edge: mxCell) => {
            var newTarget: mxCell = model.getTerminal(edge, true);
            var newSource: mxCell = model.getTerminal(edge, false);
            // Also works:
            // model.setTerminal(edge, newSource, true);
            // model.setTerminal(edge, newTarget, false);
            model.setTerminals(edge, newSource, newTarget);
        })
        graph.refresh();
    } finally {
        model.endUpdate();
    }
}

export function insertEdges(graph: mxGraph, edgesData: EdgeData[]) {
    // Function to avoid boilerplate with model.beginUpdate(),
    // model.endUpdate()...
    const model = graph.getModel();
    const newEdges = []
    model.beginUpdate()
    try {
        edgesData.forEach((ed: EdgeData) =>
            newEdges.push(
                graph.insertEdge(
                    ed.parent, ed.id, ed.value,
                    ed.esource, ed.etarget, ed.style
                )))
    } finally {
        model.endUpdate()
    }
    return newEdges;
}

export function getEdgesDataProduct(
    sources: mxCell[],
    targets: mxCell[],
): EdgeData[] {
    return sources.flatMap(
        (source: mxCell) => targets.map(
            (target: mxCell) => ({
                parent: target.getParent(),
                esource: source,
                etarget: target,
                id: null,
                value: null,
                style: "rounded=0",
                custom: null,
            })))
}

export function replaceEdges(
    graph: mxGraph,
    oldEdges: mxCell[],
    newEdgesData: EdgeData[]
): mxCell[] {
    const model = graph.getModel()
    const newEdges = insertEdges(graph, newEdgesData)
    // Remove old edges
    model.beginUpdate();
    try {
        graph.removeCells(oldEdges, true)
    } finally {
        model.endUpdate()
    }
    return newEdges
}



