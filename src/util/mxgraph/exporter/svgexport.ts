/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { getGeometryAbsolute } from "../geometry/absolute";

export function graphGetSvg(
  // Actual graph type: Graph. Descends from mxGraph. Not present current typings library.
  graph: any,
  background: string = null,
  scale: number = null,
  border: number = null,
  nocrop: boolean = true,
  crisp: boolean = false,
  ignoreSelection: boolean = true,
  showText: boolean = true,
  // Actual imgExport type: mxImageExport. Not present in current typings library.
  imgExport: any = null,
  linkTarget: "self" | "blank" | "auto" = "auto",
  hasShadow: boolean = false,
  incExtFonts: boolean = true,
  keepTheme: boolean = true,
  exportType: "page" | "diagram" = "diagram"
): SVGSVGElement {
  // Wrapper for graph.getSvg(..), since it's not present in the typings
  // library currently used.
  // TODO: maybe use a color type instead of string for `background'?
  // TODO: consider defaulting all to null instead.
  //@ts-ignore
  return graph.getSvg(
    background,
    scale,
    border,
    nocrop,
    crisp,
    ignoreSelection,
    showText,
    imgExport,
    linkTarget,
    hasShadow,
    incExtFonts,
    keepTheme,
    exportType
  );
}

export function exportGraphModelToSVG(
  model: mxGraphModel,
  embedImages: boolean,
  // `customGraphTransform' takes a copy of the graph so that the output can
  // be transformed before being exported to SVG.
  customGraphTransform: (graph: mxGraph) => void
) {
  const dummyDiv = document.createElement("div");
  //@ts-ignore
  const graph = new Graph(dummyDiv, model);

  // Convert:
  // - links: only first link is converted
  // - images
  const svgImageElems: Element[] = [];
  // tmpDoc is used to create <image> elems to be added to the resulting svg.
  const tmpDoc = mxUtils.createXmlDocument();
  graph.model
    .getDescendants(graph.getDefaultParent())
    .forEach((cell: mxCell) => {
      if (embedImages) {
        const cellState: mxCellState = graph.view.getState(cell);
        if (cellState) {
          const imageMaybe = cellState.style[mxConstants.STYLE_IMAGE] || null;
          const widthMaybe =
            cellState.style[mxConstants.STYLE_IMAGE_WIDTH] || null;
          const heightMaybe =
            cellState.style[mxConstants.STYLE_IMAGE_HEIGHT] || null;
          if (imageMaybe != null) {
            const g = graph as mxGraph;
            // An <image> inside a svg file is only shown if it has the correct namespace.
            const imageElem = tmpDoc.createElementNS(
              "http://www.w3.org/2000/svg",
              "image"
            );
            const cellGeomAbs = getGeometryAbsolute(cell);
            const graphBounds = g.getGraphBounds();
            // The + 6 comes from:
            // https://github.com/jgraph/drawio/blob/master/src/main/webapp/js/diagramly/Editor.js
            imageElem.setAttribute(
              "x",
              "" + (cellGeomAbs.x - graphBounds.x + 6)
            );
            imageElem.setAttribute(
              "y",
              "" + (cellGeomAbs.y - graphBounds.y + 6)
            );
            if (widthMaybe != null) {
              imageElem.setAttribute("width", widthMaybe);
            }
            if (heightMaybe != null) {
              imageElem.setAttribute("height", heightMaybe);
            }
            imageElem.setAttribute("xlink:href", imageMaybe);
            svgImageElems.push(imageElem);
          }
        }
      }
    });
  customGraphTransform(graph);
  // Convert graph to svg
  const svgSvgElem: SVGSVGElement = graphGetSvg(graph);
  // Add embedded images. If embedImages is false, at this point
  // svgImageElems is empty and the following line does nothing.
  // svgSvgElem.children[1] should be the outer <g> element.
  svgImageElems.forEach((imageElem: Element) =>
    svgSvgElem.children[1].appendChild(imageElem)
  );
  return svgSvgElem;
}
