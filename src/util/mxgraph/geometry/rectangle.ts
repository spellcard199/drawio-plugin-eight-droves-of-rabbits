/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { isPointContainedInRectangle } from "./point";
import { intersects } from "./segment";

export function isRectanglesOverlapping(
  r1: mxRectangle,
  r2: mxRectangle
): boolean {
  // https://stackoverflow.com/questions/2752349/fast-rectangle-to-rectangle-intersection
  const r1Left = r1.x;
  const r1Right = r1.x + r1.width;
  const r1Top = r1.y;
  const r1Bottom = r1.y + r1.height;
  const r2Left = r2.x;
  const r2Right = r2.x + r2.width;
  const r2Top = r2.y;
  const r2Bottom = r2.y + r2.height;
  return !(
    r2Left > r1Right ||
    r2Right < r1Left ||
    r2Top > r1Bottom ||
    r2Bottom < r1Top
  );
}

export function isRectangleContained(rout: mxRectangle, rin: mxRectangle) {
  // https://stackoverflow.com/questions/27768039/find-out-if-a-rectangle-is-inside-another-rectangle-c
  if (
    rin.x + rin.width < rout.x + rout.width &&
    rin.x > rout.x &&
    rin.y > rout.y &&
    rin.y + rin.height < rout.y + rout.height
  ) {
    return true;
  } else {
    return false;
  }
}

export function isRectanglesIntersecting(
  r1: mxRectangle,
  r2: mxRectangle
): boolean {
  return (
    isRectanglesOverlapping(r1, r2) &&
    !isRectangleContained(r1, r2) &&
    !isRectangleContained(r2, r1)
  );
}

export function isRectangleOverlappingPath(
  r: mxRectangle,
  path: mxPoint[],
  intersectOnly: boolean
): boolean {
  type RectangleSide = { p1: mxPoint, p2: mxPoint }
  type RectangleSides = {
    top: RectangleSide,
    left: RectangleSide, right: RectangleSide,
    bottom: RectangleSide,
  }
  const rectangleSides: RectangleSides = {
    top: {
      p1: new mxPoint(r.x, r.y),
      p2: new mxPoint(r.x + r.width, r.y)
    },
    left: {
      p1: new mxPoint(r.x, r.y),
      p2: new mxPoint(r.x, r.y + r.height)
    },
    right: {
      p1: new mxPoint(r.x + r.width, r.y),
      p2: new mxPoint(r.x + r.width, r.y + r.height)
    },
    bottom: {
      p1: new mxPoint(r.x, r.y + r.height),
      p2: new mxPoint(r.x + r.width, r.y + r.height)
    },
  }
  for (let i = 1; i < path.length; i++) {
    // Check point: if intersectOnly, skip and just check for segments:
    if (!intersectOnly && isPointContainedInRectangle(path[i - i], r)) {
      return true
    }
    // Check segment:
    for (let side of Object.values(rectangleSides)) {
      const isSegmentsIntersecting = intersects(
        side.p1.x, side.p1.y, side.p2.x, side.p2.y,
        path[i - 1].x, path[i - 1].y, path[i].x, path[i].y
      )
      if (isSegmentsIntersecting) {
        return true
      }
    }
  }
  return false
}