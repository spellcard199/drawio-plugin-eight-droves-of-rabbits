/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { getGeometryAbsolute } from "./absolute";

export function scaleSelectionDistances(graph: mxGraph, scaleFactor: number) {
  const selectionCells: mxCell[] = graph.getSelectionCells();
  const selectionVertices: mxCell[] = selectionCells.filter(
    (cell: mxCell) => cell.isVertex
  );
  const boundingBox: mxRectangle =
    graph.getBoundingBoxFromGeometry(selectionVertices);
  graph.model.beginUpdate();
  try {
    selectionVertices.forEach((vert: mxCell) => {
      const geometryAbsolute = getGeometryAbsolute(vert);
      graph.model.setGeometry(
        vert,
        new mxGeometry(
          boundingBox.x + scaleFactor * (geometryAbsolute.x - boundingBox.x),
          boundingBox.y + scaleFactor * (geometryAbsolute.y - boundingBox.y),
          geometryAbsolute.width,
          geometryAbsolute.height
        )
      );
    });
  } finally {
    graph.model.endUpdate();
  }
}
