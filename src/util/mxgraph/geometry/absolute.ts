/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export function getGeometryAbsolute(cell: mxCell): mxGeometry {
  if (typeof cell.geometry !== "undefined") {
    var deltaX = 0;
    var deltaY = 0;
    for (
      var parent = cell.getParent();
      parent != null && typeof parent.geometry !== "undefined";
      parent = parent.getParent()
    ) {
      deltaX += parent.geometry.x;
      deltaY += parent.geometry.y;
    }
    const newGeometry = cell.geometry.clone();
    // Is this right or wrong?
    newGeometry.relative = false;
    newGeometry.x += deltaX;
    newGeometry.y += deltaY;
    if (newGeometry.sourcePoint) {
      newGeometry.sourcePoint.x += deltaX
      newGeometry.sourcePoint.y += deltaY
    }
    if (newGeometry.points) {
      newGeometry.points.forEach((p: mxPoint) => {
        p.x += deltaX;
        p.y += deltaY;
      });
    }
    if (newGeometry.targetPoint) {
      newGeometry.targetPoint.x += deltaX
      newGeometry.targetPoint.y += deltaY
    }
    return newGeometry;
  }
}
