/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { getGeometryAbsolute } from "./absolute";
import { isPointContainedInRectangle } from "./point";

export function getWaypointsContainedByRectangle(
  graph: mxGraph,
  rectangle: mxRectangle
): Map<mxCell, number[]> {
  const allCells = graph.getModel().getDescendants(graph.getDefaultParent());
  let edgesWaypoints: Map<mxCell, number[]> = new Map();
  allCells.forEach((cell: mxCell) => {
    if (cell.edge && cell.geometry.points && cell.geometry.points.length > 0) {
      let waypoints: number[] = [];
      const geometryForComparison: mxGeometry = cell.geometry.relative
        ? getGeometryAbsolute(cell)
        : cell.geometry;
      geometryForComparison.points.forEach(
        (waypoint: mxPoint, waypointIndex: number) => {
          if (isPointContainedInRectangle(waypoint, rectangle)) {
            waypoints.push(waypointIndex);
          }
        }
      );
      if (waypoints.length > 0) {
        edgesWaypoints.set(cell, waypoints);
      }
    }
  });
  return edgesWaypoints;
}
