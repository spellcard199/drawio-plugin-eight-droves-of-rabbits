/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

type n = number

export function intersects(a: n, b: n, c: n, d: n, p: n, q: n, r: n, s: n) {
    // https://stackoverflow.com/questions/9043805/test-if-two-lines-intersect-javascript-function
    // returns true if the line from (a,b)->(c,d) intersects with (p,q)->(r,s)
    var det, gamma, lambda;
    det = (c - a) * (s - q) - (r - p) * (d - b);
    if (det === 0) {
        return false;
    } else {
        lambda = ((s - q) * (r - a) + (p - r) * (s - b)) / det;
        gamma = ((b - d) * (r - a) + (c - a) * (s - b)) / det;
        return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
    }
}

export function isSegmentsIntersecting(s1: [mxPoint, mxPoint], s2: [mxPoint, mxPoint]) {
    return intersects(
        s1[0].x, s1[0].y, s1[1].x, s1[1].y,
        s2[0].x, s2[0].y, s2[1].x, s2[1].y,
    )
}