/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { waypointPlaceholderIs } from "../cell/waypointplaceholder";
import { getGeometryAbsolute } from "./absolute";

export function isGroup(graph: mxGraph, cell: mxCell): boolean {
  return (
    cell &&
    cell.style &&
    // && cell.style.includes('group;')  // groups don't have to have "group" in style
    // I think (not sure) cell.children is null for non-groups
    cell.children &&
    !graph.model.isLayer(cell) &&
    !graph.model.isRoot(cell)
  );
}

type Margins = {
  left: number;
  top: number;
  bottom: number;
  right: number;
};

type LabelSize = {
  width: number;
  height: number;
};

function labelGetSize(graph: mxGraph, cell: mxCell): LabelSize {
  const cellStyle = graph.getCellStyle(cell);
  const cellFontFamily: string =
    cellStyle[mxConstants.STYLE_FONTFAMILY] ||
    graph.getStylesheet().getDefaultVertexStyle()[mxConstants.STYLE_FONTFAMILY];
  const cellFontSize: number =
    cellStyle[mxConstants.STYLE_FONTSIZE] ||
    graph.getStylesheet().getDefaultVertexStyle()[mxConstants.STYLE_FONTSIZE];

  const tmpDiv = document.createElement("div");
  tmpDiv.innerHTML = graph
    .convertValueToString(cell)
    // For some reason I don't understand, graph.convertValueToString:
    // - does: translate newlines of non-group cells to <br>
    // - does not: translate newlines of group cells to <br>
    .replace(/\n/g, "<br>");
  tmpDiv.style.fontFamily = cellFontFamily;
  tmpDiv.style.fontSize = cellFontSize.toString();
  tmpDiv.style.font = cellFontSize.toString();
  tmpDiv.style.visibility = "hidden";

  // Without these width isn't calculated right
  tmpDiv.style.height = "auto";
  tmpDiv.style.width = "auto";
  tmpDiv.style.position = "absolute";
  tmpDiv.style.whiteSpace = "no-wrap";

  document.body.appendChild(tmpDiv);
  let labelHeight: number;
  let labelWidth: number;
  try {
    labelHeight = Math.ceil(tmpDiv.clientHeight);
    labelWidth = Math.ceil(tmpDiv.clientWidth);
  } finally {
    document.body.removeChild(tmpDiv);
  }
  return {
    width: labelWidth,
    height: labelHeight,
  };
}

function fitGroupMkDefaultMargins(
  graph: mxGraph,
  groupCell: mxCell,
  labelHeight: number
): Margins {
  const groupStyle = graph.getCellStyle(groupCell);
  const groupVerticalLabelPosition: string =
    groupStyle[mxConstants.STYLE_VERTICAL_LABEL_POSITION] ||
    graph.getStylesheet().getDefaultVertexStyle()[
      mxConstants.STYLE_VERTICAL_LABEL_POSITION
    ] ||
    mxConstants.ALIGN_MIDDLE; // I think app defaults to "middle"

  let margins: Margins;
  // If true: add margins so that group's label is not overlapped by
  // children.
  // Else: default to margin of 2 in all directions.
  if (groupVerticalLabelPosition === mxConstants.ALIGN_MIDDLE) {
    const groupVerticalAlign: string =
      groupStyle[mxConstants.STYLE_VERTICAL_ALIGN] ||
      graph.getStylesheet().getDefaultVertexStyle()[
        mxConstants.STYLE_VERTICAL_ALIGN
      ] ||
      mxConstants.ALIGN_TOP;
    const groupSpacingGlobal: number =
      groupStyle[mxConstants.STYLE_SPACING] ||
      graph.getStylesheet().getDefaultVertexStyle()[
        mxConstants.STYLE_SPACING
      ] ||
      2;
    const groupSpacingTop: number =
      groupStyle[mxConstants.STYLE_SPACING_TOP] ||
      graph.getStylesheet().getDefaultVertexStyle()[
        mxConstants.STYLE_SPACING_TOP
      ] ||
      0;
    const groupSpacingBottom: number =
      groupStyle[mxConstants.STYLE_SPACING_BOTTOM] ||
      graph.getStylesheet().getDefaultVertexStyle()[
        mxConstants.STYLE_SPACING_BOTTOM
      ] ||
      0;
    if (groupVerticalAlign === mxConstants.ALIGN_TOP) {
      margins = {
        left: 2,
        top:
          2 +
          labelHeight +
          groupSpacingGlobal * 2 +
          groupSpacingTop +
          groupSpacingBottom,
        bottom: 2,
        right: 2,
      };
    } else if (groupVerticalAlign === mxConstants.ALIGN_BOTTOM) {
      margins = {
        left: 2,
        top: 2,
        bottom:
          2 +
          labelHeight +
          groupSpacingGlobal * 2 +
          groupSpacingTop +
          groupSpacingBottom,
        right: 2,
      };
    }
  }
  if (!margins) {
    // Default
    margins = { left: 2, top: 2, bottom: 2, right: 2 };
  }
  return margins;
}

export function fitGroupToChildren(
  graph: mxGraph,
  groupCell: mxCell,
  recursively: boolean = false,
  groupMargins:
    | Margins
    | "for_middle_vertical_label_position" = "for_middle_vertical_label_position"
): mxGeometry {
  if (!isGroup(graph, groupCell)) {
    return;
  }
  graph.model.beginUpdate();
  try {
    const childVertices = groupCell.children.filter((cell: mxCell) =>
      graph.model.isVertex(cell)
    );
    if (recursively) {
      childVertices.forEach((child: mxCell) => {
        if (isGroup(graph, child)) {
          fitGroupToChildren(graph, child, recursively, groupMargins);
        }
      });
    }
    const labelSize = labelGetSize(graph, groupCell);
    const margins =
      groupMargins === "for_middle_vertical_label_position" || !groupMargins
        ? fitGroupMkDefaultMargins(graph, groupCell, labelSize.height)
        : groupMargins;

    const groupRelativeChildrenBounds =
      graph.getBoundingBoxFromGeometry(
        // Do not consider waypointPlaceholders.
        childVertices.filter((cell: mxCell) => !waypointPlaceholderIs(cell))
      ) ||
      // Default when group doesn't have children and null is returned. In
      // the following lines, we calculate newX and newY to obtain left
      // and top margins. When children are not found we don't want
      // margins because they would make the fitting of the group
      // not-idempotent without adding more calculations. `margins.left'
      // and `margins.top' here compensate for the same values in newX and
      // newY.
      new mxRectangle(margins.left, margins.top, 0, 0);

    const newX =
      groupRelativeChildrenBounds.x + groupCell.geometry.x - margins.left;
    const newY =
      groupRelativeChildrenBounds.y + groupCell.geometry.y - margins.top;
    const newCollapsedWidth = margins.left + labelSize.width + margins.right;
    const newCollapsedHeight = margins.top + labelSize.height + margins.bottom;
    const newExpandedWidth =
      margins.left +
      Math.max(groupRelativeChildrenBounds.width, labelSize.width) +
      margins.right;
    const newExpandedHeight =
      margins.top +
      Math.max(groupRelativeChildrenBounds.height, labelSize.height) +
      margins.bottom;

    // Rectangle for expanded group
    const newGeometry = new mxGeometry(
      newX,
      newY,
      newExpandedWidth,
      newExpandedHeight
    );
    // Rectangle for collapsed group
    newGeometry.alternateBounds = new mxRectangle(
      newX,
      newY,
      newCollapsedWidth,
      newCollapsedHeight
    );

    if (graph.model.isCollapsed(groupCell)) {
      newGeometry.swap();
    }

    // Delta to translate children back to their position in global coords
    const dx = newGeometry.x - groupCell.geometry.x;
    const dy = newGeometry.y - groupCell.geometry.y;

    // Actually set group geometry
    graph.model.setGeometry(groupCell, newGeometry);
    // When you change the x and y of a group all the children get
    // translated too. Here we are compensating for the previous step.
    groupCell.children.forEach((child: mxCell) => {
      const newGeom = child.geometry.clone();
      if (child.isVertex()) {
        newGeom.x -= dx;
        newGeom.y -= dy;
      }
      if (newGeom.points) {
        newGeom.points.forEach((p: mxPoint) => {
          p.x -= dx;
          p.y -= dy;
        });
      }
      graph.model.setGeometry(child, newGeom);
    });
  } finally {
    graph.model.endUpdate();
  }
  return groupCell.geometry;
}

export function addCellToGroupKeepAbsolutePosition(
  graph: mxGraph,
  cell: mxCell,
  parent: mxCell
) {
  // Eg: graph.getDefaultParent().geometry is undefined
  if (!parent || !parent.geometry) {
    return;
  }
  const childGeomAbsolute = getGeometryAbsolute(cell);
  const parentGeometryAbsolute = getGeometryAbsolute(parent);
  // 1. Child's absolute coords: a vector from absolute origin to cell:
  //    should remain constant after being added to parent
  // 2. Child's absolute coords == sum of 2 vectors:
  //   - parent's absolute coords
  //   - child's coords relstive to new parent
  // 3. Rearranging equation: child's coords relative to new parent == difference between:
  //   1. child's absolute coords
  //   2. parent's absolute coords
  const xRelativeToTargetParent =
    childGeomAbsolute.x - parentGeometryAbsolute.x;
  const yRelativeToTargetParent =
    childGeomAbsolute.y - parentGeometryAbsolute.y;
  const newGeometry = cell.geometry.clone()
  newGeometry.x = xRelativeToTargetParent
  newGeometry.y = yRelativeToTargetParent
  newGeometry.width = childGeomAbsolute.width
  newGeometry.height = childGeomAbsolute.height
  graph.model.beginUpdate();
  try {
    graph.model.add(parent, cell);
    graph.model.setGeometry(cell, newGeometry);
  } finally {
    graph.model.endUpdate();
  }
}
