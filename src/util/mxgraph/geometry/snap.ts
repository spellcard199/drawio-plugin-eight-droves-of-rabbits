/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { getGeometryAbsolute } from "./absolute";
import { getNearestGridPoint } from "./coord";

export function getSnapToGridDelta(cell: mxCell, gridSize: number): mxPoint {
  const oldGeom = cell.getGeometry();
  var oldCenter = new mxPoint(oldGeom.getCenterX(), oldGeom.getCenterY());
  for (
    var parent = cell.getParent();
    parent && parent.getGeometry();
    parent = parent.getParent()
  ) {
    oldCenter.x += parent.getGeometry().x;
    oldCenter.y += parent.getGeometry().y;
  }
  const newCenter = getNearestGridPoint(oldCenter, gridSize);
  return new mxPoint(newCenter.x - oldCenter.x, newCenter.y - oldCenter.y);
}

export function snapVertexToGrid(
  graph: mxGraph,
  vertex: mxCell,
  gridSize: number = graph.gridSize
) {
  graph.model.beginUpdate();
  try {
    if (!graph.model.isVertex(vertex)) {
      throw new Error("cell " + vertex.id + " is not a vertex.");
    }
    const delta = getSnapToGridDelta(vertex, gridSize);
    graph.translateCell(vertex, delta.x, delta.y);
  } finally {
    graph.model.endUpdate();
  }
}

export function snapEdgeToGrid(
  graph: mxGraph,
  edge: mxCell,
  gridSize: number = graph.gridSize
) {
  graph.model.beginUpdate();
  try {
    if (!graph.model.isEdge(edge)) {
      throw new Error("cell " + edge.id + " is not an edge.");
    }
    const tmpGeometry = edge.geometry.clone();
    const tmpGeometryAbsolute = getGeometryAbsolute(edge);
    const deltaXtoAbs = tmpGeometryAbsolute.x - tmpGeometry.x;
    const deltaYtoAbs = tmpGeometryAbsolute.y - tmpGeometry.y;
    if (tmpGeometry.points) {
      tmpGeometry.points.forEach((point: mxPoint) => {
        const absPoint = new mxPoint(
          point.x + deltaXtoAbs,
          point.y + deltaYtoAbs
        );
        const nearestGridPoint = getNearestGridPoint(absPoint, gridSize);
        point.x = nearestGridPoint.x - deltaXtoAbs;
        point.y = nearestGridPoint.y - deltaYtoAbs;
      });
    }
    graph.model.setGeometry(edge, tmpGeometry);
  } finally {
    graph.model.endUpdate();
  }
}

export function snapCellToGrid(
  graph: mxGraph,
  cell: mxCell,
  gridSize: number = graph.gridSize
) {
  // If cell.children is not null, it's a group: do not snap groups.
  graph.model.beginUpdate();
  try {
    if (graph.model.isVertex(cell) && cell.children === null) {
      snapVertexToGrid(graph, cell, gridSize);
    } else if (graph.model.isEdge(cell)) {
      snapEdgeToGrid(graph, cell, gridSize);
    }
  } finally {
    graph.model.endUpdate();
  }
}

export function snapCellsToGrid(
  graph: mxGraph,
  cells: mxCell[],
  gridSize: number = graph.gridSize
) {
  graph.model.beginUpdate();
  try {
    cells.forEach((cell: mxCell) => snapCellToGrid(graph, cell, gridSize));
  } finally {
    graph.model.endUpdate();
  }
}

export function snapAllVertsToGrid(
  graph: mxGraph,
  gridSize: number = graph.gridSize
) {
  const model = graph.getModel();
  model.beginUpdate();
  try {
    const vertices: mxCell[] = model.filterDescendants(
      (cell: mxCell) => model.isVertex(cell),
      graph.getDefaultParent()
    );
    snapCellsToGrid(graph, vertices, gridSize);
  } finally {
    model.endUpdate();
  }
}

export function snapAllCellsToGrid(
  graph: mxGraph,
  gridSize: number = graph.gridSize
) {
  snapCellsToGrid(
    graph,
    graph.model.getDescendants(graph.getDefaultParent()),
    gridSize
  );
}

export function snapSelectionToGrid(
  graph: mxGraph,
  gridSize: number = graph.gridSize
) {
  snapCellsToGrid(graph, graph.getSelectionCells(), gridSize);
}
