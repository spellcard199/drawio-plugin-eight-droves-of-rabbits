/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { getGeometryAbsolute } from "./absolute";

export function getCellsAt(
  graph: mxGraph,
  point: mxPoint,
  parent?: mxCell,
  vertices: boolean = true,
  edges: boolean = true,
  ignoreFn: (state: mxCellState, x: number, y: number) => boolean = () => false,
  onlyVisible: boolean = true,
): mxCell[] {
  if (!parent) {
    parent = graph.getDefaultParent();
  }
  var s: number = graph.view.scale;
  var t: mxPoint = graph.view.translate;
  var dx = t.x * s;
  var dy = t.y * s;
  var x = point.x * s + dx;
  var y = point.y * s + dy;

  let model = graph.getModel()
  parent = parent
    ? parent
    : graph.getCurrentRoot()
  parent = parent
    ? parent
    : model.getRoot()

  if (parent) {
    let descendants = graph.model.getDescendants(parent)
    return descendants.filter(
      (cell: mxCell, _i: number, _descs: mxCell[]) => {
        if (onlyVisible && !graph.isCellVisible(cell)) {
          return false
        }
        if (!(edges && graph.model.isEdge(cell))
          && !(vertices && graph.model.isVertex(cell))) {
          return false
        }
        let state = graph.view.getState(cell)
        if (state == null) {
          return false
        }
        if (!ignoreFn || ignoreFn(state, x, y)) {
          return false
        }
        return graph.intersects(state, x, y)
      }
    )
  }
}

export function viewCoordToGraphCoord(
  pointInViewCoord: mxPoint,
  scale: number,
  translate: mxPoint
): mxPoint {
  return new mxPoint(
    pointInViewCoord.x / scale - translate.x,
    pointInViewCoord.y / scale - translate.y
  );
}

export function viewCoordToGraphCoordRectangle(
  rectangleInViewCoord: mxRectangle,
  scale: number,
  translate: mxPoint
): mxRectangle {
  const topLeftPoint = viewCoordToGraphCoord(
    new mxPoint(rectangleInViewCoord.x, rectangleInViewCoord.y),
    scale,
    translate
  );
  const bottomDownPoint = viewCoordToGraphCoord(
    new mxPoint(
      rectangleInViewCoord.x + rectangleInViewCoord.width,
      rectangleInViewCoord.y + rectangleInViewCoord.height
    ),
    scale,
    translate
  );
  const x = topLeftPoint.x;
  const y = topLeftPoint.y;
  const w = bottomDownPoint.x - topLeftPoint.x;
  const h = bottomDownPoint.y - topLeftPoint.y;
  return new mxRectangle(x, y, w, h);
}

export function graphCoordToViewCoord(
  pointInGraphCoord: mxPoint,
  scale: number,
  translate: mxPoint
): mxPoint {
  return new mxPoint(
    scale * (pointInGraphCoord.x + translate.x),
    scale * (pointInGraphCoord.y + translate.y)
  );
}

export function getNearestMultipleOf(num: number, space: number) {
  return Math.floor(num / space) * space;
}

export function getNearestGridPoint(point: mxPoint, gridSize: number): mxPoint {
  const xLeft = getNearestMultipleOf(point.x, gridSize);
  const xRight = getNearestMultipleOf(point.x + gridSize, gridSize);
  const nearestX =
    Math.abs(xLeft - point.x) <= Math.abs(xRight - point.x) ? xLeft : xRight;
  const yUp = getNearestMultipleOf(point.y, gridSize);
  const yDown = getNearestMultipleOf(point.y + gridSize, gridSize);
  const nearestY =
    Math.abs(yUp - point.y) <= Math.abs(yDown - point.y) ? yUp : yDown;
  return new mxPoint(nearestX, nearestY);
}

export function getDistance(p1: mxPoint, p2: mxPoint): number {
  return Math.sqrt(Math.pow((p2.x - p1.x), 2) + Math.pow((p2.y - p1.y), 2))
}

export function getCellCenterPoint(cell: mxCell, absolute: boolean): mxPoint {
  const geom = absolute
    ? getGeometryAbsolute(cell)
    : cell.geometry
  return new mxPoint(geom.x, geom.y)
}

export function getNearestCellCenter(
  graph: mxGraph, point: mxPoint,
  filterFunc = (_graph: mxGraph, _cell: mxCell, _point: mxPoint) => true
): mxCell | null {
  const cells: mxCell[] = graph.model.getDescendants(graph.getDefaultParent());
  if (!(cells && cells.length > 0)) {
    return
  }
  const nearest: { cell: mxCell, distance: number } = {
    cell: null,
    distance: null,
  };
  cells.forEach((cell: mxCell) => {
    if (
      !(cell.geometry && cell.geometry.x && cell.geometry.y)
      || !filterFunc(graph, cell, point)
    ) {
      return
    }
    const distance = getDistance(point, getCellCenterPoint(cell, true))
    if (!nearest.distance || distance < nearest.distance) {
      nearest.cell = cell
      nearest.distance = distance
    }
  })
  return nearest.cell
}