/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export function isPointContainedInRectangle(
  p: mxPoint,
  r: mxRectangle
): boolean {
  return r.x < p.x && p.x < r.x + r.width && r.y < p.y && p.y < r.y + r.height;
}
