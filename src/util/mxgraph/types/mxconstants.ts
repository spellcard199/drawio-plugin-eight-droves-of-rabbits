/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

type STYLE_ALIGN_VERTICAL =
  | typeof mxConstants.ALIGN_TOP
  | typeof mxConstants.ALIGN_CENTER
  | typeof mxConstants.ALIGN_BOTTOM;
type STYLE_ALIGN_HORIZONTAL =
  | typeof mxConstants.ALIGN_LEFT
  | typeof mxConstants.ALIGN_MIDDLE
  | typeof mxConstants.ALIGN_RIGHT;

// Combining STYLE_LABEL_POSITION and STYLE_VERTICAL_LABEL_POSITION we get the 9
// positions where the label can be relative to the cell.
// - Top Left, Top, Top Right
// - Left, Center, Right
// - Bottom Left, Bottom, Bottom Right
// Center is the position where the label is inside the cell and it is obtainied with:
// - STYLE_LABEL_POSITION = mxConstants.ALIGN_MIDDLE
// - STYLE_VERTICAL_LABEL_POSITION = mxConstants.ALIGN_CENTER
export type STYLE_VERTICAL_LABEL_POSITION = STYLE_ALIGN_VERTICAL;
export type STYLE_LABEL_POSITION = STYLE_ALIGN_HORIZONTAL;

// Justification inside LABEL_POSITION.
export type STYLE_VERTICAL_ALIGN = STYLE_ALIGN_VERTICAL;
export type STYLE_ALIGN = STYLE_ALIGN_HORIZONTAL;
