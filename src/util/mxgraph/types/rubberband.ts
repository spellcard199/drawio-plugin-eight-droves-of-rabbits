/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

class EdrRubberband extends mxRubberband {
    first: mxPoint | null
    x: number
    y: number
    height: number
    width: number
    currentX: number
    currentY: number
}

export { EdrRubberband as mxRubberband }
