/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

// export const popup: (content: string, isInternalWindow: boolean) => undefined
//     = window["mxUtils"].popup

export declare class GraphExtended extends mxGraph {
    zoomWheel: boolean
}

export { };