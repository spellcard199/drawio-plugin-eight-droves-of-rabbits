/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export function getAncestors(c: mxCell) {
    let parent = c.getParent()
    const ancestors = []
    while (parent) {
        ancestors.push(parent)
        parent = parent.getParent()
    }
    return ancestors
}
