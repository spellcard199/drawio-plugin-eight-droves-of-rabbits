/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { getGeometryAbsolute } from "../geometry/absolute";

type waypointPlaceholderAttrsValue = {
  "waypointPlaceholder": "true" | "false",
  "forEdge": string,
  "forWaypointIndex": string,
}

export const WAYPOINT_PLACEHOLDER_CONSTANTS = {
  keyIsWaypointPlaceholder: "waypointPlaceholder",
  keyForEdgeId: "forEdge",
  keyForWaypointIndex: "forWaypointIndex",
};

export function waypointPlaceholderIs(cell: mxCell) {
  if (
    cell.value &&
    cell.value.getAttribute &&
    cell.value.getAttribute(
      WAYPOINT_PLACEHOLDER_CONSTANTS.keyIsWaypointPlaceholder
    )
  ) {
    return true;
  } else {
    return false;
  }
}

export function waypointPlaceholdersMkFromEdge(
  edge: mxCell,
  waypointIndices: number[]
) {
  const waypointPlaceholdersToAdd: mxCell[] = [];
  if (edge && edge.geometry.points && edge.geometry.points.length > 0) {
    const style =
      "editable=0;resizable=0;noLabel=1;fillColor=#FFFFFF;strokeColor=#000000;";
    if (edge.geometry.points && edge.geometry.points.length > 0) {
      //
      waypointIndices.forEach((waypointIndex: number) => {
        if (edge.geometry.points.length > waypointIndex) {
          const p: mxPoint = edge.geometry.points[waypointIndex];
          const placeholderCellValue = mxUtils
            .createXmlDocument()
            .createElement("object");
          placeholderCellValue.setAttribute(
            WAYPOINT_PLACEHOLDER_CONSTANTS.keyIsWaypointPlaceholder,
            "true"
          );
          placeholderCellValue.setAttribute(
            WAYPOINT_PLACEHOLDER_CONSTANTS.keyForEdgeId,
            edge.id
          );
          placeholderCellValue.setAttribute(
            WAYPOINT_PLACEHOLDER_CONSTANTS.keyForWaypointIndex,
            waypointIndex.toString()
          );
          // NOTE:
          // 1. Precision is needed to avoid unsynced snapping to grid
          //    between waypoints and placeholders.
          // 2. Snapping to grid uses geometry.getCenterX() and
          //    geometry.getCenterY().
          // 3. We want waypointPlaceholder's center to coincide with waypoint.
          // 4. To avoid decimals in center (i.e. w/2, h/2), prefer values
          //    for w and h that are even.
          const w = 2;
          const h = 2;
          const x = p.x - w / 2;
          const y = p.y - h / 2;
          const newWaypointPlaceholder = new mxCell(
            placeholderCellValue,
            new mxGeometry(x, y, w, h),
            style
          );
          newWaypointPlaceholder.setVertex(true);
          waypointPlaceholdersToAdd.push(newWaypointPlaceholder);
        }
      });
    }
  }
  return waypointPlaceholdersToAdd;
}

export function waypointPlaceholdersInsertFromEdge(
  graph: mxGraph,
  edge: mxCell,
  waypointIndices: number[]
): mxCell[] {
  const waypointPlaceholdersToAdd: mxCell[] = waypointPlaceholdersMkFromEdge(
    edge,
    waypointIndices
  );
  const waypointPlaceholdersAdded: mxCell[] = [];
  // graph.insertVertex changes geometry of parents to contain children
  // cells.  Store ancestors' geometries and re-set these after
  // waypointPlaceholders are inserted.
  const ancestorsGeometrySaved: Map<mxCell, mxGeometry> = new Map();
  for (
    let ancestor = edge.parent;
    ancestor && ancestor.geometry && ancestor.geometry.clone;
    ancestor = ancestor.parent
  ) {
    ancestorsGeometrySaved.set(ancestor, ancestor.geometry.clone());
  }
  // Insert waypointPlaceholders without changing ancestors' geometries.
  graph.model.beginUpdate();
  try {
    graph
      .addCells(waypointPlaceholdersToAdd, edge.parent)
      .forEach((wph: mxCell) => waypointPlaceholdersAdded.push(wph));
    // Undo ancestors' geometry change done by graph.insertVertex
    ancestorsGeometrySaved.forEach((geom: mxGeometry, ancestor: mxCell) => {
      graph.model.setGeometry(ancestor, geom);
    });
  } finally {
    graph.model.endUpdate();
  }
  return waypointPlaceholdersAdded;
}

export function waypointPlaceholderGetWaypointFromPlaceholder(
  waypointPlaceholder: mxCell,
): null | { edgeId: string, waypointIndex: number } {
  if (waypointPlaceholderIs(waypointPlaceholder)) {
    const value: Element = waypointPlaceholder.value as Element;
    const edgeId = value.getAttribute(
      WAYPOINT_PLACEHOLDER_CONSTANTS.keyForEdgeId
    );
    const waypointIndex: number = parseInt(
      value.getAttribute(WAYPOINT_PLACEHOLDER_CONSTANTS.keyForWaypointIndex)
    );
    return {
      edgeId: edgeId,
      waypointIndex: waypointIndex,
    }
  } else {
    return null
  }
}

export function waypointPlaceholderMoveWaypointToPlaceholder(
  graph: mxGraph,
  waypointPlaceholder: mxCell
): { edge: mxCell; waypointIndex: number } | null {
  const waypoint = waypointPlaceholderGetWaypointFromPlaceholder(waypointPlaceholder);
  if (!waypoint) {
    return null
  }
  const edgeId = waypoint.edgeId
  const waypointIndex = waypoint.waypointIndex
  const edge = graph.model.getCell(edgeId);
  if (!edge) {
    // Can happen when (eg) you copy-paste from one diagram to another.
    return null
  }
  if (edge.geometry.points && edge.geometry.points.length > waypointIndex) {
    // Calculate delta
    const edgeGeometryAbsolute = getGeometryAbsolute(edge);
    const waypointPlaceholderGeometryAbsolute =
      getGeometryAbsolute(waypointPlaceholder);
    const deltaX =
      waypointPlaceholderGeometryAbsolute.getCenterX() -
      edgeGeometryAbsolute.points[waypointIndex].x;
    const deltaY =
      waypointPlaceholderGeometryAbsolute.getCenterY() -
      edgeGeometryAbsolute.points[waypointIndex].y;
    // Move points
    const edgeGeometryCloned = edge.geometry.clone();
    edgeGeometryCloned.points[waypointIndex].x += deltaX;
    edgeGeometryCloned.points[waypointIndex].y += deltaY;
    // .clone(): workaround to trigger redraw.
    // TODO: Is graph.refresh() called anyway? If yes: we could just
    // call graph.refresh() once at the end instead of cloning a new
    // geometry and setting it for each edge.
    graph.model.setGeometry(edge, edgeGeometryCloned);
    return {
      edge: edge,
      waypointIndex: waypointIndex,
    };
  }
}

export function waypointPlaceholderRemoveWaypointsForPlaceholders(
  graph: mxGraph,
  cells: mxCell[],
) {
  graph.model.beginUpdate()
  try {
    cells.forEach((placeholderMaybe: mxCell) => {
      const waypoint = waypointPlaceholderGetWaypointFromPlaceholder(placeholderMaybe);
      if (!waypoint) {
        return null
      }
      const edgeId = waypoint.edgeId
      const waypointIndex = waypoint.waypointIndex
      const edge = graph.model.getCell(edgeId);
      if (edge.geometry.points && edge.geometry.points.length > waypointIndex) {
        const edgeGeometryCloned = edge.geometry.clone();
        edgeGeometryCloned.points.splice(waypointIndex, 1);
        graph.model.setGeometry(edge, edgeGeometryCloned);
        graph.model.remove(placeholderMaybe);
      }
    })
  } finally {
    graph.model.endUpdate()
  }
}