/**
 * Copyright (C) 2021-2023, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { isGroup } from "../geometry/group";

export function cellGeometrySetPreferredSize(
  graph: mxGraph,
  cell: mxCell,
  keepCenter: boolean
): void {
  // Autosize changes width and height keeping x and y constant and this causes
  // cell's center to shift.  What one may want instead is to have the cell's
  // center not changed when autosizing, to preserve the alignment with other
  // cells.
  const centerX = cell.geometry.getCenterX();
  const centerY = cell.geometry.getCenterY();
  graph.model.beginUpdate();
  try {
    graph.cellSizeUpdated(cell, false); // does the resizing
    if (keepCenter) {
      const geom = cell.geometry.clone();
      const deltaX = geom.getCenterX() - centerX;
      const deltaY = geom.getCenterY() - centerY;
      geom.setRect(geom.x - deltaX, geom.y - deltaY, geom.width, geom.height);
      graph.model.setGeometry(cell, geom);
    }
  } finally {
    graph.model.endUpdate();
  }
}

export function cellGeometrySetPreferredSizeIfAutosizeAndNotGroup(
  graph: mxGraph,
  cells: mxCell[],
  keepCenter: boolean
): void {
  const cellStates = graph.view.getCellStates(cells)
  for (let cellState of cellStates) {
    if (isGroup(graph, cellState.cell)) {
      // If cell is a group, graph.cellSizeUpdated causes the unwanted behavior
      // that the group gets translated to the left (I don't know why).
      continue
    }
    const style = cellState.style
    if (style && style['autosize'] === 1) {
      cellGeometrySetPreferredSize(graph, cellState.cell, keepCenter)
    }
  }
}