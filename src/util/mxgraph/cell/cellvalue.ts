/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export function labelFromString(s: string): string {
  return s.replace(/  /g, "  ").replace(/  /g, "  ");
}

export function stringFromLabel(label: string): string {
  return label.replace(/  /g, "  ").replace(/  /g, "  ");
}

export function cellValueGetLabelString(cell: mxCell) {
  // Abstract over value's type, which can be both string and Element
  var labelText: string;
  if (cell.value && cell.value.attributes) {
    labelText = cell.value.getAttribute("label") || "";
  } else if (typeof cell.value === "string") {
    labelText = cell.value;
  } else {
    labelText = "";
  }
  return stringFromLabel(labelText);
}

export function cellValueSetLabel(
  graph: mxGraph,
  cell: mxCell,
  newLabelString: string
) {
  // Abstract over value's type, which can be both string and Element
  const newLabel: string = labelFromString(newLabelString);
  var newValue: string | Element;
  if (cell.value && cell.value.constructor.name === "Element") {
    newValue = (cell.value as Element).cloneNode() as Element;
    newValue.setAttribute("label", newLabel);
  } else if (!cell.value || typeof cell.value === "string") {
    newValue = newLabel;
  }
  graph.model.beginUpdate();
  try {
    graph.model.setValue(cell, newValue);
  } finally {
    graph.model.endUpdate();
  }
}

export function cellValueSetAttributes(
  graph: mxGraph,
  cell: mxCell,
  attributes: { [attrName: string]: string }
) {
  let newValue: HTMLObjectElement =
    cell.value && cell.value.attributes
      ? cell.value.cloneNode()
      : mxUtils.createXmlDocument().createElement("object");
  if (typeof cell.value === "string") {
    newValue.setAttribute("label", labelFromString(cell.value));
  }
  Object.entries(attributes).forEach((entry) => {
    const attrName: string = entry[0];
    const attrValue: string = entry[1];
    if (attrName === "label") {
      newValue.setAttribute(attrName, labelFromString(attrValue));
    } else {
      newValue.setAttribute(attrName, attrValue);
    }
  });
  const newLabelMaybe: string = newValue.getAttribute("label");
  graph.getModel().beginUpdate();
  try {
    // If just we are setting just the label in a cell with a string value...
    if (
      typeof cell.value === "string" &&
      newLabelMaybe &&
      newValue.attributes.length === 1
    ) {
      // ... then:
      // - set a string value (that will also behave as a label)
      // - discard newValue
      graph.model.setValue(cell, newLabelMaybe);
    } else {
      // ... otherwise: set cell.value to the new HTMLObjectElement.
      graph.model.setValue(cell, newValue);
    }
  } finally {
    graph.getModel().endUpdate();
  }
}
