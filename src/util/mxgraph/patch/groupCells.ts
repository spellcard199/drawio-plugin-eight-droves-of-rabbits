/**
 * Copyright (C) 2021-2023, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { getGeometryAbsolute } from "../geometry/absolute";

export function groupCellsPatchForWaypoints(
  group: mxCell,
  border?: number,
  cells?: mxCell[]
): mxCell {
  // How to use: since this function contains...:
  //   graph: mxGraph = this;
  // ... it can only work after being bound to an object of type mxGraph. For
  // instance if you wanted to override the behavior of graph.groupCells:
  //   graph.groupCells = groupCellsPatchForWaypoints
  //
  // Behavior of graph.groupCells without this patch: when 2 vertices connected
  // by an edge are added to a group, if the connecting edge is:
  // - also passed to graph.groupCells (works as expected):
  //   1. edge.parent is set to group.
  //   2. Coordinates for waypoints are converted into coordinates relative to
  //      group.
  //   3. Waypoints remain in their absolute positions (as expected).
  // - *NOT* passed to graph.groupCells:
  //   1. edge.parent is set to group.
  //   2. Coordinates for waypoints are *NOT* converted into coordinates
  //      relative to group.
  //   3. Waypoints *DO NOT* remain in their absolute positions
  //
  // This function patches the case where 2 vertices are passed to
  // graph.groupCells but the edge connecting them is not.

  // Store needed data before applying original function
  const graph: mxGraph = this;
  const cellsAsSet = new Set(cells);
  // Descendants of each cell in `cells' are also affected, so we add them.
  cells.forEach(
    (cell: mxCell) => graph.model.getDescendants(cell)
      .forEach((descendant: mxCell) => cellsAsSet.add(descendant)))
  const edgesToPatch: Map<mxCell, mxGeometry> = new Map();
  cellsAsSet.forEach((cell: mxCell) => {
    graph.model.getEdges(cell).forEach((edge: mxCell) => {
      if (
        edge.geometry &&
        edge.geometry.points &&
        edge.geometry.points.length > 0 &&
        cellsAsSet.has(edge.source) &&
        cellsAsSet.has(edge.target) &&
        !cellsAsSet.has(edge)
      ) {
        edgesToPatch.set(edge, getGeometryAbsolute(edge));
      }
    });
  });
  graph.model.beginUpdate();
  try {
    // Apply original function
    const groupingCell: mxCell = mxGraph.prototype.groupCells.apply(this, [
      group,
      border,
      cells,
    ]);
    // Apply patch
    edgesToPatch.forEach((wantedAbsoluteGeometry: mxGeometry, edge: mxCell) => {
      if (
        wantedAbsoluteGeometry.points.length === 0 ||
        edge.geometry.points.length === 0
      ) {
        // This code path shouldn't happen since previous check. Keeping just to
        // remember in case code is copy-pasted elsewhere.
        return;
      }
      // Use first waypoint to calculate delta to apply to all waypoints.
      const p0wanted = wantedAbsoluteGeometry.points[0];
      const currentAbsoluteGeometry = getGeometryAbsolute(edge);
      const p0current = currentAbsoluteGeometry.points[0];
      const dx = p0wanted.x - p0current.x;
      const dy = p0wanted.y - p0current.y;
      const newGeometry = edge.geometry.clone();
      newGeometry.points.forEach((p: mxPoint) => {
        p.x += dx;
        p.y += dy;
      });
      graph.model.setGeometry(edge, newGeometry);
    });
    return groupingCell;
  } finally {
    graph.model.endUpdate();
  }
}
