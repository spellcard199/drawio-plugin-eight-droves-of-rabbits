/**
 * Copyright (C) 2022-2023, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export function enterGroupPatch(groupArg?: mxCell) {
    // TODO: start again from scratch.
    // 1. First of all: write a function that returns the same value when a cell
    // occupies the same position on the screen (eg: at center) in entered and
    // exited group view.
    // 2. Instaed of trying to re-implement centering ourselves, try to combine
    //    the various scrollTo... functions in mxGraph.prototype, so that we do
    //    not have to consider both cases for when scrollbars are used and when
    //    they are not.
    //    1. https://github.com/jgraph/mxgraph-js/blob/dd603ceb0fba0b38bfbe0eea83d4faa0ed72d99e/javascript/src/js/view/mxGraph.js#L8262
    //    2. scrollVertToVisible(cell, true): when I tried to call this it
    //       worked, but it requires a cell as argument.
    //    3. scrollToRect(rect): this is what we would actually need, but I
    //       can't make it to scroll to the correct rectangle, and I don't
    //       understand why. Same thing for mxGraph.prototype.center. Learn how
    //       to use it by studying how scrollVertToVisible uses it.
    // 2. Study also:
    //    1. Action "fitWindow": in drawio-desktop/drawio/src/main/webapp/js/grapheditor/Actions.js
    // 3. graph.fitWindow works, but changes zoom:
    //    - graph.fitWindow(graph.view.getState(graph.getSelectionCell()).getCellBounds()).
    //    - can study code
    // 3. Alternatively, we need function that:
    //    - takes: cell
    //    - returns: position of cell in screen:
    //      - Note: graph.view.translate is only used when scrollbars are not:
    //        - See: https://github.com/jgraph/mxgraph/blob/ff141aab158417bd866e2dfebd06c61d40773cd2/javascript/src/js/view/mxGraph.js#L3077
    // Note: problems I encountered while implementing it myself:
    // - when you enter in group these things change:
    //   - graph coordinates
    //   - container size
    //   - scrolls' width and height.

    const graph: mxGraph = this
    const groupMaybe: mxCell | undefined = groupArg || graph.getSelectionCell()
    if (!groupMaybe || !graph.isValidRoot(groupMaybe)) {
        return
    }
    const group = groupMaybe;

    // function centerOnGraph() {
    //     let rootX = 0
    //     let rootY = 0
    //     if (graph.view.currentRoot) {
    //         // const rootAbs = window['edr'].util.geometry.absolute.getGeometryAbsolute(graph.view.currentRoot)
    //         const state = graph.view.getState(group)
    //         rootX = state.getCellBounds().x
    //         rootY = state.getCellBounds().y
    //     }
    //     let me = new MouseEvent("",
    //         {
    //             clientX: -rootX + (graph.container.scrollLeft + graph.container.scrollWidth / 2),
    //             clientY: -rootY + (graph.container.scrollTop + graph.container.scrollHeight / 2)
    //         });
    //     return graph.getPointForEvent(me, false);
    //     return new mxPoint(
    //         rootX + ((graph.container.scrollLeft + graph.container.scrollWidth / 2) / graph.view.scale),
    //         rootY + ((graph.container.scrollTop + graph.container.scrollHeight / 2) / graph.view.scale)
    //     )
    // }
    // const b1 = graph.view.getState(group).getCellBounds();

    // // TODO: use some other point as reference woso we don't have to use data in euim.
    // const euim: EditorUiManager = window["euim"];
    // const p1 = euim.mouseManager.lastMouseMoveData.getPointOnGraph()
    // const p1 = graph.getPointForEvent(new MouseEvent("", { clientX: 0, clientY: 0 }), false)
    // const saveTranslate = graph.view.translate
    // const z1 = mxUtils.convertPoint(graph.container, 0, 0)

    // const graphContainerCenterX1 =
    //     graph.container.scrollLeft + graph.container.scrollWidth / 2;
    // const graphContainerCenterY1 =
    //     graph.container.scrollTop + graph.container.scrollHeight / 2;
    // const bounds1 = graph.view.getBounds([group]);

    mxGraph.prototype.enterGroup.apply(this, group);
    // Using the coordinate system of the container, after applying delta the
    // distance between center and group bounds should remain equal:
    // - graphContainerCenterX1 - groupBounds1
    // - graphContainerCenterX2 - groupBounds2
    // graph.view.refresh();
    // const graphContainerCenterX2 =
    //     graph.container.scrollLeft + graph.container.scrollWidth / 2;
    // const graphContainerCenterY2 =
    //     graph.container.scrollTop + graph.container.scrollHeight / 2;
    // const bounds2 = graph.view.getBounds([group]);
    // const dx = (graphContainerCenterX1 - bounds1.x) - (graphContainerCenterX2 - bounds2.x);
    // const dy = (graphContainerCenterY1 - bounds1.y) - (graphContainerCenterY2 - bounds2.y);
    // graph.container.scrollBy(dx, dy);
    // graph.view.refresh();
}

export function exitGroupPatch() {
    // const graph: mxGraph = this
    // const saveTranslate = graph.view.translate
    mxGraph.prototype.exitGroup.apply(this)
    // graph.view.setTranslate(saveTranslate.x, saveTranslate.y)
}
