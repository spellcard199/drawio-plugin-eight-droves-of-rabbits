/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

// Unused
// export function insertRandomVertex(graph: mxGraph) {
//     // TODO: delete this function. Keeping it for now to remember how to insert vertices.
//     function getRandomInt(max) {
//         return Math.floor(Math.random() * Math.floor(max));
//     }
//     var bounds: mxRectangle = graph.view.getGraphBounds();
//     var x: number = getRandomInt(bounds.height);
//     var y: number = getRandomInt(bounds.width);
//     return graph.insertVertex(
//         graph.getDefaultParent(), null, "Random vertex",
//         x, y, 80, 20,
//         'boxstyle'
//     );
// }
