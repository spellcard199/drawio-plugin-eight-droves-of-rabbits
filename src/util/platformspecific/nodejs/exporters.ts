/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { exportGraphModelToSVG } from "../../mxgraph/exporter/svgexport";
import { parseGraphModelsFromMxFile } from "../../parse/drawio";
import { extToSVG } from "../../parse/fileext";
import { existsSync, mkdirSync, readFileSync, statSync, writeFileSync } from "./bridgewrap/fs";
import { dirname, isAbsolute, relative, resolve } from "./bridgewrap/path";
import { lsFilesRecursive } from "./lsfiles";

export function mkConversionDirMap(
  sourceDir: string,
  targetDir: string,
  applyToFilepath: (targetFileNameToTransform: string) => string
): Map<string, string> {
  const arrayOfFiles: string[] = lsFilesRecursive(
    sourceDir,
    (descendantPath: string) =>
      descendantPath.endsWith(".drawio") || descendantPath.endsWith(".xml")
  );
  const sourceTargetMap: Map<string, string> = new Map();
  arrayOfFiles.forEach((sourceFile: string) => {
    const sourceFileRelative = relative(sourceDir, sourceFile);
    const targetFile = applyToFilepath(resolve(targetDir, sourceFileRelative));
    sourceTargetMap.set(sourceFile, targetFile);
  });
  return sourceTargetMap;
}

export function exportDrawioFileToSVG(
  sourceFileAbsolute: string,
  destFileAbsolute: string,
  embedImages: boolean = true,
  // `customGraphTransform': passed to `convertGraphModelToSVG'
  customGraphTransform: (graph: mxGraph) => void
) {
  if (!isAbsolute(sourceFileAbsolute)) {
    throw new Error(
      "'sourceFileAbsolute' should be an absolute path. Actual path: " +
        sourceFileAbsolute
    );
  }
  if (!isAbsolute(destFileAbsolute)) {
    throw new Error(
      "'destFileAbsolute' should be an absolute path. Actual path: " +
        destFileAbsolute
    );
  }

  var codec = new mxCodec();
  const xmlString = readFileSync(sourceFileAbsolute).toString();
  const graphModels: mxGraphModel[] = parseGraphModelsFromMxFile(
    xmlString,
    codec
  );
  const firstDiagram: mxGraphModel = graphModels[0];
  const svgSvgElem = exportGraphModelToSVG(
    firstDiagram,
    embedImages,
    customGraphTransform
  );

  // Convert svg object to string
  const svgXmlString: string = mxUtils.getXml(svgSvgElem as any);

  // Make targetFile's parent dir if it doesn't exist
  const targetFileDir = dirname(destFileAbsolute);
  if (!existsSync(targetFileDir)) {
    mkdirSync(targetFileDir, { recursive: true });
  }
  // Write to file
  writeFileSync(destFileAbsolute, svgXmlString);
}

export function exportDirTo(
  sourceDir: string,
  targetDir: string,
  toFormat: string = "svg",
  embedImages: boolean = true,
  // `customGraphTransform': passed to `convertGraphModelToSVG'
  customGraphTransform: (graph: mxGraph) => mxGraph
) {
  // TODO:
  // - Add more choices toFormat choices
  // - Currently, only the first diagram in each file gets exported: To
  //   account for multiple diagrams: for each source file, for each diagram in
  //   the file: generate a file name:
  //   - maybe: each diagram following the first could have an incremental
  //     number before the extension. The first one would have just the extension
  //     changed: this way at least links to the first diagram of each file would
  //     continue work (given we convert those here)
  var exporterFunc;
  if (toFormat === "svg") {
    exporterFunc = exportDrawioFileToSVG;
  } else {
    throw new Error("toFormat '" + toFormat + "' not supported.");
  }
  const sourceTargetMap: Map<string, string> = mkConversionDirMap(
    sourceDir,
    targetDir,
    extToSVG
  );

  if (existsSync(targetDir)) {
    if (!statSync(targetDir).isDirectory()) {
      throw new Error(
        "'" + targetDir + "' already exists and is not a directory."
      );
    }
  } else {
    mkdirSync(targetDir);
  }

  var counter = 1;
  sourceTargetMap.size;
  sourceTargetMap.forEach(
    (
      destinationFile: string,
      sourceFile: string,
      _sourceTargetMap: Map<string, string>
    ) => {
      console.log(
        `[${counter}/${sourceTargetMap.size}] ${sourceFile} -> ${destinationFile}`
      );
      exporterFunc(
        sourceFile,
        destinationFile,
        embedImages,
        customGraphTransform
      );
      counter += 1;
    }
  );
}
