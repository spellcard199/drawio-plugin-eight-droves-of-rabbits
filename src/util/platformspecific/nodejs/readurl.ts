/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import * as http from "../nodejs/bridgewrap/http"
import * as https from "../nodejs/bridgewrap/https"
import { isElectron } from "../../electron";
import { getLocalFilePath } from "../../platformgeneric/url";
import { existsSync, readFileSync } from "./bridgewrap/fs";

export function readUrlUsingNodeJS(url: string): Promise<string> {
  // Desktop app has a different CSP from web app. Desktop app's CSP does
  // not allow us to use `fetch' to get remote files so we have to use
  // nodejs's http/https.

  // Copy/pasted from:
  // https://stackoverflow.com/questions/6287297/reading-content-from-url-with-node-js/48729712#48729712
  // Sample usage:
  // (async (url) => { console.log(await nodeReadUrl(url)); })('https://sidanmor.com/');
  return new Promise((resolve, reject) => {
    const client = url.toString().indexOf("https") === 0 ? https : http;
    client
      .get(url, (resp) => {
        let data = "";
        // A chunk of data has been recieved.
        resp.on("data", (chunk) => {
          data += chunk;
        });
        // The whole response has been received. Print out the result.
        resp.on("end", () => {
          resolve(data);
        });
      })
      .on("error", (err) => {
        reject(err);
      });
  });
}

export function readLocalFileSync(absolutePathOrUrl: string): string | Error {
  if (!isElectron()) {
    // Web browsers can't open local files from the app (app and local
    // files have different origins).
    const errMsg =
      "Web browsers can't access local " +
      "files from web app (different origins). " +
      "If you want to follow local links you should use " +
      "diagrams.net's desktop app.";
    new Error(errMsg);
  }
  const filePath = getLocalFilePath(absolutePathOrUrl);
  if (!filePath.startsWith("/")) {
    const errMsg = "`filePath' does not start with a '/': " + filePath;
    return new Error(errMsg);
  } else if (!existsSync(filePath)) {
    const errMsg = "File does not exist: " + filePath;
    return new Error(errMsg);
  }
  const content = new TextDecoder().decode(readFileSync(filePath))
  return content;
}
