/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { execFileSync } from "./bridgewrap/child_process";
import { existsSync, mkdirSync, readFileSync, rmSync, writeFileSync } from "./bridgewrap/fs";
import { tmpdir } from "./bridgewrap/os";
import { date } from "../..";
import { window } from "../../types/window"

export let inkscapeExecutable = "inkscape";
export let inkscapeTmpPrefix = "drawioInkscape-";

export function inkscapeOpenFile(file: string, args: string[] = []) {
  // Arguments can be either a single String or in an Array
  const fullArgs = [...args, file];
  // console.log(fullArgs)
  const x = execFileSync(inkscapeExecutable, fullArgs);
}

export function inkscapeMkDefaultSvgString() {
  const svgsvgElem: SVGSVGElement = <any>document.createElement('svg');
  // If you don't set some of these attrs, the svg remains fixed in a certain
  // position in the page, regardless of where its cell's location in the graph.
  svgsvgElem.setAttribute("style", "enable-background:new 0 0 209.322 209.322;")
  svgsvgElem.setAttribute("viewBox", "0 0 209.322 209.322")
  svgsvgElem.setAttribute("height", "209.322px")
  svgsvgElem.setAttribute("width", "209.322px")
  svgsvgElem.setAttribute("y", "0px")
  svgsvgElem.setAttribute("x", "0px")
  return svgsvgElem.outerHTML
}

export function inkscapeOpenCurrentCellSVG(graph: mxGraph) {
  const currentCell = graph.getSelectionCell();
  const cellState = graph.view.getState(currentCell);
  const cellStateStyle = cellState.style;
  const dataImageSvgXmlPrefix = "data:image/svg+xml";
  const imageMaybe: string =
    cellStateStyle.image ||
    dataImageSvgXmlPrefix + "," + inkscapeMkDefaultSvgString()
  // TODO: check if imageMaybe is in a bundle as image value (get first match)
  if (imageMaybe && imageMaybe.startsWith(dataImageSvgXmlPrefix)) {
    const strippedPrefix1 = imageMaybe.replace(dataImageSvgXmlPrefix, "");
    let svgDecoded: string;
    let isBase64: boolean;
    if (strippedPrefix1.startsWith(";base64,")) {
      // Decode from base64:
      svgDecoded = atob(strippedPrefix1.replace(";base64,", ""));
      isBase64 = true;
    } else {
      // Decode from URI:
      svgDecoded = decodeURIComponent(strippedPrefix1.replace(/^,/, ""));
      isBase64 = false;
    }

    const tmpInkscapeDir = tmpdir() + "/eight_droves_of_rabbits/inkscape/"
    mkdirSync(tmpInkscapeDir, { recursive: true })
    if (!existsSync(tmpInkscapeDir)) {
      throw new Error("Couldn't create tmpInkscapeDir: " + tmpInkscapeDir)
    }
    const tmpFile = tmpInkscapeDir
      + date.datelocal.dateGetLocalISOString()
      + window.crypto.randomUUID() + ".svg"
    writeFileSync(tmpFile, svgDecoded)

    // Actually edit file in inkscape
    inkscapeOpenFile(tmpFile);

    // When inkscape is closed read tmpFile, where inkscape may have saved its edit.
    const fileContent: string = new TextDecoder().decode(readFileSync(tmpFile)).trim();
    if (existsSync(tmpFile)) {
      rmSync(tmpFile)
    }
    // NOTE: even if the literal style does not contain "base64", if it is a
    // base64-encoded image, cellStateStyle.image WILL contain
    // "data:image/svg+xml;base64,...". Here we are removing the base64 part
    // that is auto-added by mxgraph's style parser because it seems to
    // break the parsing itself, probably because ";" is also a separator
    // between the attributes in style.
    const newSvgContent: string = fileContent.startsWith(";base64,")
      ? fileContent.replace(";base64,", "")
      : fileContent;
    const newSvgData: string = isBase64
      ? // Is this needed? Keeping for reference in case we need it:
      // dataImageSvgXmlPrefix + "," + btoa(unescape(encodeURIComponent(newSvgContent)));
      // - source: https://stackoverflow.com/questions/28450471/convert-inline-svg-to-base64-string
      dataImageSvgXmlPrefix + "," + btoa(newSvgContent)
      : dataImageSvgXmlPrefix + "," + encodeURIComponent(newSvgContent);
    mxUtils.setCellStyles(graph.model, [currentCell], "image", newSvgData);
    // Without shape=image, image is not shown:
    mxUtils.setCellStyles(graph.model, [currentCell], "shape", "image")
  }
}
