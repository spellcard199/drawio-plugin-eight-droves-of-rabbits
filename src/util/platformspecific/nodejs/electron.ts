/**
 * Copyright (C) 2021-2023, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { EditorUi } from "../../types/drawio";
import { WindowExtended } from "../../types/window";

export function electronEnableF12OpenDevTools(editorUi: EditorUi): void {
    if (((window as unknown) as WindowExtended).mxIsElectron) {
        document.addEventListener(
            "keyup",
            ({ key, ctrlKey, shiftKey, metaKey, altKey }) => {
                metaKey; altKey;  // silence compiler warnings about unused vars
                if (ctrlKey && shiftKey && key === "F12") {
                    editorUi.openDevTools();
                }
            }
        );
    }
}


// Since electron moved to nodeIntegration: false, contextIsolation: true,
// this does not work anymore.  Keeping as a note for future.

// import { remote } from "electron";
// import { isElectron } from "../../electron";

// export function electronEnableF12OpenDevTools(): void {
//   return
//   if (isElectron()) {
//     // Electron-specific stuff
//     const currentWebContents = remote.getCurrentWebContents();
//     document.addEventListener(
//       "keyup",
//       ({ key, ctrlKey, shiftKey, metaKey, altKey }) => {
//         if (
//           key === "F12" ||
//           (ctrlKey && shiftKey && key === "I") ||
//           (metaKey && altKey && key === "i")
//         ) {
//           currentWebContents.openDevTools();
//         }
//       }
//     );
//   }
// }

// https://discuss.atom.io/t/how-to-make-developer-tools-appear/16232/4
// require('electron').remote.getCurrentWindow().toggleDevTools();
// @ts-ignore: I don't know why toggleDevTools is not in type defs.
// try {
//     electron.remote.getCurrentWindow().toggleDevTools();
// } catch (err) {
//     console.log(err);
// }
