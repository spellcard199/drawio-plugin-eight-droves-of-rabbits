/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { ClientRequest, IncomingMessage } from "http"
import { URL } from "url"
import * as http from "http"

// export function get(
//     url: string | URL,
//     options: http.RequestOptions,
//     callback?: (res: IncomingMessage) => void,
// ): ClientRequest {
//     return http.get(url, options, callback)
// }

export function get(
    url: string | URL,
    callback?: (res: IncomingMessage) => void,
): ClientRequest {
    return http.get(url, callback)
}