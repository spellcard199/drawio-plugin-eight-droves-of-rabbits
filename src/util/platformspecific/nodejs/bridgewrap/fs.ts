/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import * as nodeFS from "fs";

export function existsSync(path: string): boolean {
    return nodeFS.existsSync(path)
}

export function mkdirSync(path: string, options?: nodeFS.MakeDirectoryOptions) {
    return nodeFS.mkdirSync(path, options)
}

export function readFileSync(path: nodeFS.PathLike, options?: { encoding?: null, flag?: string }) {
    // TODO: consider replacing return type.
    return nodeFS.readFileSync(path, options)
}

export function statSync(path: string, options = undefined) {
    return nodeFS.statSync(path, options)
}

export function close(fd: number, callback: nodeFS.NoParamCallback) {
    return nodeFS.close(fd, callback)
}

export function readdirSync(
    path: nodeFS.PathLike,
    options?: { encoding: BufferEncoding, withFileTypes?: false }
        | BufferEncoding
) {
    return nodeFS.readdirSync(path, options)
}

export function rmSync(path: nodeFS.PathLike, options?: nodeFS.RmOptions) {
    return nodeFS.rmSync(path, options)
}

export function write(
    fd: number,
    buffer: any,
    callback: (err: NodeJS.ErrnoException, written: number, buffer: any) => void,
): void {
    return nodeFS.write(fd, buffer, callback)
}

export function writeFileSync(
    path: number | nodeFS.PathLike,
    data: string | NodeJS.ArrayBufferView,
    options?: nodeFS.WriteFileOptions
) {
    return nodeFS.writeFileSync(path, data, options)
}