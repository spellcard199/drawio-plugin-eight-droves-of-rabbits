/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import * as childprocess from "child_process"

export function execFileSync(command: string, args: string[]) {
    // Note: child_proecess.execFileSync has 6 overloads.
    return childprocess.execFileSync(command, args)
};