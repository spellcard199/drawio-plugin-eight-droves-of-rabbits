/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import * as os from "os"

export function tmpdir() {
    return os.tmpdir()
}