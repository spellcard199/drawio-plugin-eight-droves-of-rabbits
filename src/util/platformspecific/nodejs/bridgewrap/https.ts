/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { ClientRequest, IncomingMessage } from "http"
import { URL } from "url"
import * as https from "https"

// export function get(
//     url: string | URL,
//     options: https.RequestOptions,
//     callback?: (res: IncomingMessage) => void,
// ): ClientRequest {
//     return https.get(url, options, callback)
// }

export function get(
    url: string | URL,
    callback?: (res: IncomingMessage) => void,
): ClientRequest {
    return https.get(url, callback)
}