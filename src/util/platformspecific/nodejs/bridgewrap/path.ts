/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import * as nodePath from "path"

export function resolve(...pathSegments: string[]) {
    return nodePath.resolve(...pathSegments)
}

export function dirname(p: string) {
    return nodePath.dirname(p)
}

export function isAbsolute(p: string) {
    return nodePath.isAbsolute(p)
}

export function relative(from: string, to: string): string {
    return nodePath.relative(from, to)
}
