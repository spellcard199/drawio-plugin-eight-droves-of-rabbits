/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import * as nodeURL from "url"

export function fileURLToPath(url: string | URL) {
    let urlStr;
    if (url instanceof URL) {
        urlStr = url.toString()
    } else if (typeof url == "string") {
        urlStr = url
    } else {
        throw new Error(
            "url should be a String or an URL: \n"
            + "- typeof url: " + typeof url + "\n"
            + "- url (value): " + url
        )
    }
    return nodeURL.fileURLToPath(urlStr)
}

// Currently unused:
// export function pathToFileURL(path: string) {
//     return nodeURL.pathToFileURL(path)
// }