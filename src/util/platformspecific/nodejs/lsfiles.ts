/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { readdirSync, statSync } from "./bridgewrap/fs";
import { isAbsolute, resolve } from "./bridgewrap/path";

function lsFilesRec(
  dirPathAbsolute: string,
  arrayOfFiles: string[],
  filterFunc: (descendentPathFull: string) => boolean = (_descentantPathFull) =>
    true
): string[] {
  if (!isAbsolute(dirPathAbsolute)) {
    throw new Error("'" + dirPathAbsolute + "' is not an absolute path");
  }
  var files = readdirSync(dirPathAbsolute);

  arrayOfFiles = arrayOfFiles || [];

  files.forEach(function (file) {
    const descendantPathFull = resolve(dirPathAbsolute, file);
    if (statSync(dirPathAbsolute + "/" + file).isDirectory()) {
      arrayOfFiles = lsFilesRec(descendantPathFull, arrayOfFiles);
    } else {
      if (filterFunc(descendantPathFull)) {
        // NOPE: gives dir/filename/filename
        //   arrayOfFiles.push(resolve(descendantPathFull, file))
        arrayOfFiles.push(descendantPathFull);
      }
    }
  });

  return arrayOfFiles;
}

export function lsFilesRecursive(
  dirPathAbsolute: string,
  filterFunc: (descendentPathFull: string) => boolean = (_descentantPathFull) =>
    true
) {
  return lsFilesRec(dirPathAbsolute, [], filterFunc);
}
