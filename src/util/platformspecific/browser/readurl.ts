/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export function readUrlUsingBrowser(url: string): Promise<string> {
  // TODO-MAYBE?: consider if it's worth to support proxies like:
  // - https://cors-escape.herokuapp.com/
  // - https://cors-anywhere.herokuapp.com/
  // This project (apache-2.0) shows the basic logic on how to
  // do it:
  // - https://github.com/niutech/x-frame-bypass

  // How to check if resource exists but is behind CORS:
  // https://stackoverflow.com/questions/19325314/how-to-detect-cross-origin-cors-error-vs-other-types-of-errors-for-xmlhttpreq/40683747#40683747
  // var external = 'your url';
  // if (window.fetch) {
  //     // must be chrome or firefox which have native fetch
  //     fetch(external, {'mode':'no-cors'})
  //         .then(function () {
  //             // external is reachable; but failed due to cors
  //             // fetch will pass though if it's a cors error
  //         })
  //         .catch(function () {
  //             // external is _not_ reachable
  //         });
  // } else {
  //     // must be non-updated safari or older IE...
  //     // I don't know how to find error type in this case
  // }

  // TODO: Add above in a catch call. If the resource is reachable use
  // proxy.

  return fetch(url)
    .then((res) => res.text())
    .then((txt) => txt);
}
