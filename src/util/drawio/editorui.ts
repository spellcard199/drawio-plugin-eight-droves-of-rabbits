/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { isElectron } from "../electron";
import { readFileContent } from "../platformgeneric/read";
import { fileURLToPath } from "../platformgeneric/url";
import { EditorUi } from "../types/drawio/EditorUi";
import { FileObject } from "../types/drawio/FileObject";

export function getFileDataRaw(editorUi: any) {
  return editorUi.getFileData(
    true, // forceXml
    null, // forceSvg
    null, // forceHtml
    null, // embeddedCallback
    true, // ignoreSelection
    false, // currentPage
    null, // node
    null, // compact
    null, // file
    true // uncompressed
  );
}

export function getEditorFileUrlOrPath(editorUi: any): string | undefined {
  let editorFilePath: string;
  const currentFile = editorUi.currentFile;
  if (currentFile && currentFile.fileObject) {
    editorFilePath = currentFile.fileObject.path;
  } else {
    if (isElectron()) {
      editorFilePath = undefined;
    } else {
      const beg = window.location.href.indexOf("#U") + 2;
      const encodedAbsoluteURL = window.location.href.substring(beg);
      const decodedAbsoluteURL = decodeURIComponent(encodedAbsoluteURL);
      editorFilePath = decodedAbsoluteURL;
    }
  }
  return editorFilePath;
}

export function confirmSaveIfModified(editorUi: any): Boolean | Error {
  // Sould return an Error if we should not load a new file.
  if (editorUi.currentFile.modified) {
    const wantsToDiscardCnanges = confirm(
      "Following the link will cause loss of the changes. Discard changes?"
    );
    const reallyWantsToDiscardChanges =
      wantsToDiscardCnanges &&
      confirm("Discarding changes to current file. Are you really sure?");
    if (!reallyWantsToDiscardChanges) {
      return Error("Aborted by user.");
    }
  }
  return true;
}

export async function openDiagramFile(
  editorUi: EditorUi,
  absoluteDiagramPathOrUrl: string
): Promise<void> {
  // TODO - investigate: when using web app, is it possible to set URL instead?
  // Try checking
  // 1. If both are true:
  //   - not electron
  //   - absoluteDiagramPathOrUrl is not local
  // 2. Then: window.location = absoluteDiagramPathOrUrl

  // console.log("[editorUiOpenFile] absoluteDiagramPathOrUrl: " + absoluteDiagramPathOrUrl)
  const filePathAbsolute = absoluteDiagramPathOrUrl.startsWith("file:")
    ? fileURLToPath(absoluteDiagramPathOrUrl)
    : absoluteDiagramPathOrUrl;
  const fileObj = new FileObject(filePathAbsolute, "utf-8");
  // console.log("[editorUiOpenFile] fileObj: " + fileObj)

  let fileContentEither: string | Error = await readFileContent(
    absoluteDiagramPathOrUrl
  );
  // (long output)
  // console.log("[editorUiOpenFile] fileContentEither: " + fileContentEither)
  if ((fileContentEither as Error).message) {
    alert((fileContentEither as Error).message);
    return;
  }
  const fileContent: string = fileContentEither as string;
  //@ts-ignore
  const lFile = new LocalFile(
    editorUi, // ui
    fileContent, // data
    "" // title
    // temp
  );
  lFile.fileObject = fileObj;
  // console.log("lFile: ");
  // console.log(lFile);

  // Ready to load: ask for confirmation before actually doing it.
  const confirmResult: Boolean | Error = confirmSaveIfModified(editorUi);
  if ((confirmResult as Error).message) {
    alert((confirmResult as Error).message);
    return;
  }

  editorUi.fileLoaded(lFile);
}

export function removeActions(editorUi: EditorUi, actionsToRemove: Array<any>) {
  const boundActions = editorUi.actions.actions;
  for (let i = 0; i < actionsToRemove.length; i++) {
    const actionToRemove = actionsToRemove[i];
    actionToRemove.setEnabled(false);
    Object.keys(editorUi.actions.actions).forEach((actionName: string) => {
      if (actionToRemove === boundActions[actionName]) {
        boundActions[actionName] = undefined;
      }
    });
  }
}

export function getActionName(
  editorUi: EditorUi,
  action: any
): string | undefined {
  for (let actionName of Object.keys(editorUi.actions.actions)) {
    if (editorUi.actions.actions[actionName] === action) {
      return actionName;
    }
  }
}
