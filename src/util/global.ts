/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

function getGlobal(): any {
  // Adapted from:
  // https://stackoverflow.com/questions/3277182/how-to-get-the-global-object-in-javascript
  var global: any;
  try {
    global = Function("return this")();
  } catch (e) {
    global = window;
  }
  return global;
}
