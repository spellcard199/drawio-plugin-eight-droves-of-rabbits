/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

// This module is not needed for this project. It's just to have at hand some
// function to explore objects when trying to write typings for classes present
// at runtime (i.e. using the REPL).

export class TypeData {
  obj: any;
  parentProto: any;
  fields: string[] = new Array();
  methods: string[] = new Array();

  constructor(obj) {
    this.obj = obj;
    this.parentProto = Object.getPrototypeOf(obj);
    Object.getOwnPropertyNames(obj).forEach((fieldOrMethod) => {
      if (typeof fieldOrMethod === "function") {
        this.methods.push(fieldOrMethod);
      } else {
        this.fields.push(fieldOrMethod);
      }
    });
  }
}

export const getPrototypeDataRec = (objOrProto) => {
  let properties: TypeData[] = [];
  let currentObj = objOrProto;
  do {
    properties.push(new TypeData(currentObj));
  } while ((currentObj = Object.getPrototypeOf(currentObj)));
  return properties;
};
