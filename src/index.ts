/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

// When using es6 target for typescript:
// 1. You can't import mxgraph-type-definitions explicitly
// 2. the line below becomes necessary
/// <reference path="../node_modules/mxgraph-type-definitions/index.d.ts" />

import { DrawioPluginEightDrovesOfRabbits } from "./DrawioPluginEightDrovesOfRabbits";

DrawioPluginEightDrovesOfRabbits.loadPlugin();
