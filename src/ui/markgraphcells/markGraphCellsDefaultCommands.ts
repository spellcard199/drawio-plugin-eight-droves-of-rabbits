/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { checklistElemGetLabelsInputsChecked } from "../../util/html/checklist"
import { KeyboardEventWrapper } from "../keyboard/KeyEventWrapper"
import { Command } from "../modal/Command"
import { ShowMode } from "../modal/showmode/ShowMode"
import { MarkGraphCells } from "./MarkGraphCells"

export const mkCommandSetSelection = (
    markGraphCells: MarkGraphCells,
    graph: mxGraph,
    checklistElem: HTMLElement
): Command<ShowMode> => ({
    name: "markSetSelection",
    doc: "",
    run: ((_kew: KeyboardEventWrapper, showMode: ShowMode) => {
        const markNames = checklistElemGetLabelsInputsChecked(checklistElem)
        markGraphCells.selectMarkedCells(graph, markNames)
        showMode.quit()
    })
})

export const mkCommandMarkDelete = (
    markGraphCells: MarkGraphCells,
    graph: mxGraph,
    checklistElem: HTMLElement
): Command<ShowMode> => ({
    "name": "markDelete",
    "doc": "",
    run: (_kew: KeyboardEventWrapper, showMode: ShowMode) => {
        const markNames = checklistElemGetLabelsInputsChecked(checklistElem)
        markGraphCells.deleteNamesForGraph(graph, markNames)
        showMode.quit()
    }
})