/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { EditorUiManager } from ".."
import { dateGetLocalISOString } from "../../util/date/datelocal"
import { ChecklistSection } from "../../util/html/checklist"
import { KeyBinding } from "../keyboard"
import { Mode } from "../modal/Mode"
import { ShowMode } from "../modal/showmode"
import { mkCommandMarkDelete, mkCommandSetSelection } from "./markGraphCellsDefaultCommands"

export class MarkGraphCells {

    editorUiManager: EditorUiManager
    data: Map<mxGraph, Map<string, mxCell[]>> = new Map()

    constructor(euim: EditorUiManager) {
        this.editorUiManager = euim
    }

    addCells(graph: mxGraph, markName: string, cells: mxCell[]) {
        // Ensure graph in in this.data
        const maybeMarks = this.data.get(graph)
        let graphMarks: Map<string, mxCell[]> = maybeMarks
            ? maybeMarks
            : new Map()
        this.data.set(graph, graphMarks)
        // Add cells
        graphMarks.set(markName, cells)
    }

    addCurrentSelectionWithPrompt() {
        const editorUi = this.editorUiManager.editorUi
        const graph = this.editorUiManager.getCurrentGraph()
        if (graph.getSelectionCells().length === 0) {
            return
        }
        const maybeMarks = this.data.get(graph)
        // Make default name
        let i = 1;
        const markGraphCells = this
        if (maybeMarks) {
            // Set i to the first free number
            for (null; maybeMarks.get(i.toString()); i++) { }
        }
        const markNameDefault = i.toString()
        // Show text dialog
        const dlg: TextareaDialog = new TextareaDialog(
            editorUi,
            "Insert name of mark:",
            markNameDefault,
            (markName: string) => {
                if (markName != null) {
                    // I'm unsure which behavior I prefer. Keeping as note.
                    // if (maybeMarks && maybeMarks.get(markName)) {
                    //     markName = markName + " - " + dateGetLocalISOString()
                    // }
                    markName = dateGetLocalISOString() + " - " + markName
                    markGraphCells.addCells(
                        graph,
                        markName.trim(),
                        graph.getSelectionCells(),
                    )
                }
            },
            null, null, 400, 220
        )
        editorUi.showDialog(dlg.container, 420, 300, true, true);
        // Workaround to avoid unwanted insertion after hotkey is triggered:
        setTimeout(() => dlg.init(), 0.2)
        const ta = dlg.textarea
        dlg.textarea.setSelectionRange(0, dlg.textarea.value.length)
        // Type Alt+Enter for "clicking" on "Apply"
        dlg.textarea.addEventListener(
            "keydown", (evt) => {
                if (evt.altKey && evt.key === "Enter") {
                    const maybeApplyBtn = Array.from(
                        dlg.container.getElementsByTagName("button")
                    ).filter(
                        btn => btn.className == "geBtn gePrimaryBtn"
                    )
                    if (maybeApplyBtn.length > 0) {
                        maybeApplyBtn[0].click()
                    }
                }
            })
    }

    selectMarkedCells(graph: mxGraph, names: string[]) {
        const maybeMarks = this.data.get(graph)
        if (!maybeMarks) { return }
        const cellsToSelect: mxCell[] = []
        names.forEach((name: string) => {
            const cells = maybeMarks.get(name)
            cells.forEach((cell) => cellsToSelect.push(cell))
        })
        graph.setSelectionCells(cellsToSelect)
    }

    selectMarkedCellsWithPicker(graph: mxGraph, restoreModeAfterShow: Mode) {
        const showMode = ShowMode.getInstanceOrNew(this.editorUiManager)
        const marksMaybe = this.data.get(graph)
        if (!marksMaybe) { return }
        const markNames: string[] = Array.from(marksMaybe.keys(), markName => markName)
        markNames.reverse()  // Most recent first
        const checklist: ChecklistSection[] = [{
            headingLevel: "h5",
            headingTitle: "Check marks to set selection cells for:",
            formElementId: "markElemId",
            checklist: markNames,
        }]
        const checklistElem = showMode.showChecklist(restoreModeAfterShow, checklist)
        showMode.keySeqMap.push(
            new KeyBinding(
                ["Enter"], mkCommandSetSelection(this, graph, checklistElem),
            ),
            new KeyBinding(
                ["Shift", "D"], mkCommandMarkDelete(this, graph, checklistElem),
            )
        )
    }

    deleteNamesForGraph(graph: mxGraph, names: string[]) {
        const maybeGraphMarks = this.data.get(graph)
        if (maybeGraphMarks) {
            names.forEach((name: string) => {
                maybeGraphMarks.delete(name)
            })
        }
    }

}