/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { EditorUiManager } from "../EditorUiManager";
import { MouseEventData } from "./MouseEventData";

export class MouseManager {
  euiManager: EditorUiManager;
  lastMouseMoveData: MouseEventData = new MouseEventData();

  private mouseMoveListener: {
    [key: string]: (es: mxEventSource, me: mxMouseEvent) => void;
  } = {
      mouseDown: (_sender: mxGraph, _mxEvt: mxMouseEvent) => { },
      mouseMove: mxUtils.bind(this, (sender: mxGraph, meEvt: mxMouseEvent) => {
        this.lastMouseMoveData.senderGraph = sender;
        this.lastMouseMoveData.mxMouseEvt = meEvt;
      }) as (es: mxEventSource, me: mxMouseEvent) => void,
      mouseUp: (_sender: mxGraph, _mxEvt: mxMouseEvent) => { },
    };

  constructor(euiManager: EditorUiManager) {
    this.euiManager = euiManager;
  }

  enable() {
    this.installGraphMouseListener(this.euiManager.editorUi.editor.graph);
  }

  disable() {
    this.uninstallGraphMouseListener(this.euiManager.editorUi.editor.graph);
  }

  installGraphMouseListener(graph: mxGraph) {
    graph.addMouseListener(this.mouseMoveListener);
  }

  uninstallGraphMouseListener(graph: mxGraph) {
    const listenerIndex = graph.mouseListeners.indexOf(this.mouseMoveListener);
    if (listenerIndex > -1) {
      graph.mouseListeners.splice(listenerIndex, 1);
    }
  }
}
