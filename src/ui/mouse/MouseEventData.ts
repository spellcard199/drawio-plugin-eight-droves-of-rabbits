/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { getCellsAt, getNearestCellCenter } from "../../util/mxgraph/geometry/coord";

export class MouseEventData {
  senderGraph: mxGraph;
  mxMouseEvt: mxMouseEvent;
  getPointOnGraph(): mxPoint | undefined {
    if (typeof this.senderGraph !== "undefined") {
      const senderClass = this.senderGraph.constructor.name;
      if (senderClass == "Graph" || senderClass == "mxGraph") {
        const g: mxGraph = this.senderGraph;
        const pointerEvent: PointerEvent = this.mxMouseEvt.evt as PointerEvent;
        return g.getPointForEvent(pointerEvent, true);
      }
    }
  }
  getCellNearestToPointOnGraph(
    filterFunc = (_graph: mxGraph, _cell: mxCell, _point: mxPoint) => true
  ): mxCell | null {
    return getNearestCellCenter(this.senderGraph, this.getPointOnGraph(), filterFunc)
  }
  getCellsAtMousePos(): mxCell[] {
    const p: mxPoint = this.getPointOnGraph() || undefined;
    return p
      ? getCellsAt(this.senderGraph, p)
      : []
  }
}
