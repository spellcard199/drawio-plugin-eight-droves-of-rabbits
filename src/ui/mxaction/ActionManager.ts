/**
 * Copyright (C) 2020-2023, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { removeActions } from "../../util/drawio/editorui";
import { EditorUi } from "../../util/types/drawio/EditorUi";
import { EditorUiManager } from "../EditorUiManager";

export type KeyShortcutData = {
  keyCode: number;
  ctrlPressed: boolean;
  actionKey: string;
  shiftPressed: boolean;
};

export type ActionData = {
  label: string;
  actionKey: string;
  funct: () => void;
  enabled?: boolean;
  iconCls?: string;
  // Description of keybinding: you can also put a wrong keybinding
  // here (like in this case). The actual keybinding depends on
  // editorUi.keyHandler.bindAction.
  keyShortcutDescription: string;
  keyShortcuts: KeyShortcutData[];
};

function mkActionDataToggle(euiManager: EditorUiManager): ActionData {
  const actionKey = "edrToggle";
  const shortcuts = [
    {
      // 32 is the keycode for Spacebar
      keyCode: 32,
      ctrlPressed: true,
      actionKey: actionKey,
      shiftPressed: true,
    },
  ];
  return {
    label: "Toggle edr",
    actionKey: actionKey,
    funct: () => euiManager.toggle(),
    enabled: null,
    iconCls: null,
    keyShortcutDescription: "Ctrl+Shift+Spacebar",
    keyShortcuts: shortcuts,
  };
}

function mkActionDataQuit(euiManager: EditorUiManager): ActionData {
  const actionKey = "edrQuit";
  const shortcuts: KeyShortcutData[] = [];
  return {
    label: "Quit edr",
    actionKey: actionKey,
    funct: () => euiManager.quit(),
    enabled: null,
    iconCls: null,
    keyShortcutDescription: "",
    keyShortcuts: shortcuts,
  };
}

export class ActionManager {
  private euiManager: EditorUiManager;
  private actions: Array<any> = new Array();

  constructor(euiManager: EditorUiManager) {
    this.euiManager = euiManager;
  }

  public getActions() {
    return this.actions;
  }

  public installActions() {
    this.addActionToggle();
    this.addActionQuit();
  }

  private addActionToggle() {
    const euiManager = this.euiManager;
    const toggleAction = ActionManager.addAction(
      euiManager.editorUi,
      mkActionDataToggle(euiManager),
      false
    );
    this.actions.push(toggleAction);
    return toggleAction;
  }

  private addActionQuit() {
    const euiManager = this.euiManager;
    const quitAction = ActionManager.addAction(
      euiManager.editorUi,
      mkActionDataQuit(euiManager),
      false
    );
    this.actions.push(quitAction);
    return quitAction;
  }

  private static addAction(
    editorUi: EditorUi,
    actionData: ActionData,
    replace?: boolean
  ): any {
    const oldActionMaybe = editorUi.actions.get(actionData.actionKey);

    if (replace && typeof oldActionMaybe !== "undefined") {
      oldActionMaybe.setEnabled(false);
      editorUi.actions.actions[actionData.actionKey] = undefined;
    }

    if (typeof oldActionMaybe === "undefined") {
      // @ts-ignore
      mxResources.parse(actionData.actionKey + "=" + actionData.label);
      editorUi.actions.addAction(
        actionData.actionKey,
        actionData.funct,
        actionData.enabled,
        actionData.iconCls,
        actionData.keyShortcutDescription
      );
      actionData.keyShortcuts.forEach((shortcutData: KeyShortcutData) => {
        editorUi.keyHandler.bindAction(
          shortcutData.keyCode,
          shortcutData.ctrlPressed ? !0 : !1,
          shortcutData.actionKey,
          shortcutData.shiftPressed ? !0 : !1
        );
      });
    }
    return editorUi.actions.get(actionData.actionKey);
  }

  public uninstallActions() {
    removeActions(this.euiManager.editorUi, this.actions);
    // empty this.actions array
    while (this.actions.pop()) { }
  }
}
