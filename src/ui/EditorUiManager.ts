/**
 * Copyright (C) 2020-2023, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { getFileDataRaw } from "../util/drawio/editorui";
import { getEdgesDataProduct, replaceEdges, reverseEdges } from "../util/mxgraph/edges/editing";
import { groupCellsPatchForWaypoints } from "../util/mxgraph/patch/groupCells";
import { mxRubberband } from "../util/mxgraph/types/rubberband";
import { EditorUi } from "../util/types/drawio/EditorUi";
import { KeyManager } from "./keyboard/KeyManager";
import { MenuManager } from "./menu/MenuManager";
import { Mode } from "./modal/Mode";
import { ViLikeMode } from "./modal/vilikemode/ViLikeMode";
import { MouseManager } from "./mouse/MouseManager";
import { MarkGraphCells } from "./markgraphcells/MarkGraphCells";
import { electronEnableF12OpenDevTools } from "../util/platformspecific/nodejs/electron";
import { window } from "../util/types/window";
import { isGroup } from "../util/mxgraph/geometry/group";
import { enterGroupPatch, exitGroupPatch } from "../util/mxgraph/patch/groupEnterExit";


const EDGE_DEFAULT_STYLE = "rounded=0"

type ListenerType = {
  name: string,
  f: Function,
}

class SavedGraphContainerMargins {
  resizeListener: ListenerType;
  graphContainerMargins: {
    left: string
    right: string
    top: string
    bottom: string
  }
  left: string;
  right: string;
  top: string;
  bottom: string;
  constructor(resizeListener: ListenerType, left: string, right: string, top: string, bottom: string) {
    this.resizeListener = resizeListener;
    this.left = left;
    this.right = right;
    this.top = top;
    this.bottom = bottom;
  }
}

type EdgeTerminals = {
  source: mxCell
  target: mxCell
}

class LastInsertedEdges {
  // Warning: note that this is a very naive implementation: when using undo
  // these fields do not get undo-ed, and relying on them can give the wrong
  // result.
  graph: mxGraph
  terminals: EdgeTerminals[]
  edges: mxCell[] = [];
  alternativeTargets: mxCell[][] = [];

  public constructor(graph: mxGraph) {
    this.graph = graph
  }

  public reverseDirection() {
    reverseEdges(this.graph, this.edges)
  }

  public amendTargets(
    newTargets: mxCell[],
    alternativeTargets?: mxCell[][]
  ): mxCell[] | undefined {
    // Replaces last edges with new edges with:
    // 1. same sources
    // 2. newTargets
    const model = this.graph.getModel()
    if (!this.terminals) {
      return
    }
    const newEdgesData = getEdgesDataProduct(this.terminals.map(t => t.source), newTargets)
    const newEdges = replaceEdges(this.graph, this.edges, newEdgesData)
    this.edges = newEdges
    this.terminals = newEdges.map((newEdge: mxCell) => ({
      source: model.getTerminal(newEdge, true),
      target: model.getTerminal(newEdge, false)
    }))
    if (alternativeTargets) {
      this.alternativeTargets = alternativeTargets
    }
    return newEdges
  }

  public cycleAlternativeTargets() {
    if (!this.terminals
      || !this.alternativeTargets
      || this.alternativeTargets.length < 2) {
      return
    }
    const fst = this.alternativeTargets.shift()
    this.alternativeTargets.push(fst)
    const newTargets = this.alternativeTargets[0]
    const newEdgesData = getEdgesDataProduct(
      this.terminals.map(t => t.source), newTargets
    )
    const newEdges = replaceEdges(this.graph, this.edges, newEdgesData)
    this.edges = newEdges
    const model = this.graph.getModel();
    this.terminals = newEdges.map((e: mxCell) => ({
      source: model.getTerminal(e, true),
      target: model.getTerminal(e, false)
    }))
  }
}

export class EditorUiManager {
  private enabled = false;
  static letters: Array<String> = "abcdefghijklmnopqrstuvwxyz".split("");
  static numbers: Array<String> = "0123456789".split("");
  static editorUiManagers: Array<EditorUiManager> = [];
  editorUi: EditorUi;
  lastInsertedEdges: LastInsertedEdges
  saveAutosave: boolean;
  saveCompressXml: boolean;
  markGraphCellsSelections: MarkGraphCells = new MarkGraphCells(this);
  // activeModes is a Map of: mode.name -> mode
  modes: Map<string, Mode> = new Map();
  keyManager: KeyManager = new KeyManager(this);
  mouseManager: MouseManager = new MouseManager(this);
  menuManager: MenuManager = new MenuManager(this);
  uiHidden: boolean = false;
  // User can set own graph mapper here (can be bound to keybindings).
  // TODO: maybe make graphMapper a list of mappers (with a name) the user can
  //       choose from?
  // Default: apply no changes;
  graphMapper: (graph: mxGraph) => void = (_g: mxGraph) => { };
  rubberband: mxRubberband;

  savedForHideUi: SavedGraphContainerMargins = null;

  private constructor(editorUi: any) {
    this.editorUi = editorUi;
    // this.getCurrentGraph() works only after this.editorUi has been set:
    this.lastInsertedEdges = new LastInsertedEdges(this.getCurrentGraph())
    const viLikeMode = new ViLikeMode(this);
    this.modes.set(viLikeMode.modeName, viLikeMode);
  }

  public static new(editorUi: EditorUi): EditorUiManager | undefined {
    if (
      EditorUiManager.editorUiManagers
        .map((euim) => euim.editorUi)
        .indexOf(editorUi) !== -1
    ) {
      console.log("[UIManager-ERROR] editorUi already has an EditorUiManager.");
      return;
    } else {
      const editorUiManager: EditorUiManager = new EditorUiManager(editorUi);
      EditorUiManager.editorUiManagers.push(editorUiManager);
      return editorUiManager;
    }
  }

  public isEnabled() {
    return this.enabled;
  }

  public enable() {
    // TODO: check if already enabled.
    this.keyManager.enable();
    this.mouseManager.enable();
    this.menuManager.enable();
    this.menuManager.setTitleUnderlined(true);
    this.modes.forEach(mode => mode.enable())
    // Default to uncompressed xml
    //@ts-ignore
    this.saveCompressXml = window.Editor.compressXml;
    this.saveAutosave = this.editorUi.editor.autosave;
    //@ts-ignore
    window.Editor.compressXml = false;
    // Default to not autosave
    this.editorUi.editor.autosave = false;
    document.onvisibilitychange
    const graph = this.getCurrentGraph();
    if (graph.stylesheet.styles && graph.stylesheet.styles.group) {
      const styleForGroup = graph.stylesheet.styles.group;
      if (styleForGroup.strokeColor) {
        styleForGroup.strokeColor = "#000000";
      }
      if (styleForGroup.recursiveResize) {
        styleForGroup.recursiveResize = 0;
      }
    }
    if (window.mxIsElectron) {
      // Electron-specific
      electronEnableF12OpenDevTools(this.editorUi);
    }
    graph.groupCells = groupCellsPatchForWaypoints;
    graph.enterGroup = enterGroupPatch;
    graph.exitGroup = exitGroupPatch;
    this.enabled = true;
    console.log("[EditorUiManager.enable] Done.");
    return this;
  }

  public disable() {
    this.showUi();
    this.keyManager.disable();
    this.mouseManager.disable();
    this.modes.forEach(mode => mode.disable())
    this.menuManager.setTitleUnderlined(false);
    //@ts-ignore
    window.Editor.compressXml = this.saveCompressXml;
    const graph = this.getCurrentGraph();
    if (graph.stylesheet.styles && graph.stylesheet.styles.group) {
      const styleForGroup = graph.stylesheet.styles.group;
      if (styleForGroup.strokeColor) {
        styleForGroup.strokeColor = "none";
      }
      if (styleForGroup.recursiveResize) {
        delete styleForGroup.recursiveResize;
      }
    }
    graph.groupCells = mxGraph.prototype.groupCells;
    graph.enterGroup = mxGraph.prototype.enterGroup;
    graph.exitGroup = mxGraph.prototype.exitGroup;
    this.enabled = false;
    console.log("[EditorUiManager.disable] Done.");
    return this;
  }

  public toggle() {
    this.enabled ? this.disable() : this.enable();
  }

  quit() {
    this.disable();
    this.menuManager.disable();
    const index = EditorUiManager.editorUiManagers.indexOf(this);
    if (index > -1) {
      EditorUiManager.editorUiManagers.splice(index, 1);
    }
    console.log("[EditorUiManager.quit] Done.");
  }

  public getCurrentGraph(): mxGraph {
    return this.editorUi.editor.graph;
  }

  insertDefaultVertex(graph: mxGraph): mxCell | undefined {
    // Add vertex with center at mouse pointer
    const pointOnGraph = this.mouseManager.lastMouseMoveData.getPointOnGraph();
    let newVertex: mxCell;
    if (typeof pointOnGraph !== "undefined") {
      const model = graph.getModel();
      const w = 80;
      const h = 30;
      const x = pointOnGraph.x - w / 2;
      const y = pointOnGraph.y - h / 2;
      // "Monospace" font works on both Linux and Windows. "DejaVu Sans Mono" only on Linux.
      // Note to self:
      // - Monospace
      // - Liberation Serif
      const style =
        "autosize=1;spacing=2;fontFamily=Liberation Serif;align=left;fillColor=#FFFFFF;strokeColor=#000000;";
      // const style = "spacing=2;fontFamily=Monospace;align=left;fillColor=#FFF2CC;strokeColor=#D6B656;"
      model.beginUpdate();
      try {
        // Note: if you need cell's value to be an Element, the following works and is used by:
        //   drawio/src/main/webapp/js/diagramly/sidebar/Sidebar-C4.js
        // mxUtils.createXmlDocument().createElement('object')
        // If you use a string instead of null as cell's value it messes up with autosize=1.
        newVertex = graph.insertVertex(graph.getDefaultParent(), null, null, x, y, w, h, style);
        // This also works:
        //   const cell: mxCell = new mxCell("", new mxGeometry(x, y, w, h), style)
        //   cell.setVertex(true)
        //   graph.addCell(cell, graph.defaultParent)
        graph.refresh();
      } finally {
        model.endUpdate();
      }
    }
    return newVertex;
  }

  insertDefaultEdges(graph: mxGraph): mxCell[] | undefined {

    const getTargetCells = (euim: EditorUiManager): mxCell[] => {
      // .reverse: inner before outer cells
      const cellsAtMousePos = euim.mouseManager.lastMouseMoveData.getCellsAtMousePos().reverse()
      const targetCells: mxCell[] = []
      targetCells.push(...cellsAtMousePos)
      if (targetCells.length === 0) {
        const nearestCell = euim.mouseManager.lastMouseMoveData
          .getCellNearestToPointOnGraph()
        targetCells.push(nearestCell)
      }
      if (targetCells.map((cell) => isGroup(graph, cell)).every((b: boolean) => b)) {
        const nearestNonGroupCell = euim.mouseManager.lastMouseMoveData
          .getCellNearestToPointOnGraph(
            (g: mxGraph, c: mxCell, _p: mxPoint) => !isGroup(g, c)
          )
        targetCells.push(nearestNonGroupCell)
      }
      return targetCells
    }

    const targetCells = getTargetCells(this);

    if (targetCells.length > 0) {
      const newEdges: mxCell[] = [];
      const newTerminals: EdgeTerminals[] = [];
      const model = graph.getModel();
      const selectionCells: mxCell[] = graph.getSelectionCells();
      const target = targetCells[0];
      const parent = target.getParent();
      model.beginUpdate();
      try {
        for (var i = 0; i < selectionCells.length; i++) {
          const source = selectionCells[i];
          newTerminals.push({ source: source, target: target })
          newEdges.push(
            graph.insertEdge(parent, null, null, source, target, EDGE_DEFAULT_STYLE)
          );
        }
      } finally {
        model.endUpdate();
      }
      this.lastInsertedEdges.graph = graph;
      this.lastInsertedEdges.terminals = newTerminals;
      this.lastInsertedEdges.edges = newEdges;
      this.lastInsertedEdges.alternativeTargets = targetCells.map(c => [c]);
      graph.refresh();
      return newEdges;
    }
  }

  centerOnPointer(graph: mxGraph) {
    const pointOnGraph: mxPoint =
      this.mouseManager.lastMouseMoveData.getPointOnGraph() || null;
    if (pointOnGraph) {
      // Note: differently from the Java Swing implementation of
      // JGraphX, the container for a mxGraph object (javascript):
      // - is not: a GraphComponent
      // - is: a DOM element (div.geDiagramContainer.geDiagramBackdrop)
      const graphContainerCenterX =
        graph.container.offsetLeft + graph.container.offsetWidth / 2;
      const graphContainerCenterY =
        graph.container.offsetTop + graph.container.offsetHeight / 2;
      const mouseFromCenterX =
        graphContainerCenterX -
        this.mouseManager.lastMouseMoveData.mxMouseEvt.getX();
      const mouseFromCenterY =
        graphContainerCenterY -
        this.mouseManager.lastMouseMoveData.mxMouseEvt.getY();
      // Note: these are different concepts:
      // - graph.view.translate
      // - graph.container.scroll
      // Differently from our (discontinued) java implementation, we do not need to:
      // - convert to graph coordinates
      // - use graph.view.translate
      // - use graph.view.scale
      graph.container.scrollBy(-mouseFromCenterX, -mouseFromCenterY);
    }
  }

  toggleUi() {
    if (this.uiHidden) {
      this.showUi();
    } else {
      this.hideUi();
    }
    this.uiHidden = this.savedForHideUi !== null ? true : false;
  }

  static uitoggleCalculateDeltaScroll(
    graphBoundsBefore: mxRectangle,
    graphBoundsAfter: mxRectangle
  ) {
    // Coefficients are found by trial and error: I don'r really
    // understand what's going on.
    return new mxPoint(
      Math.abs(graphBoundsBefore.x - graphBoundsAfter.x) * 0.519,
      Math.abs(graphBoundsBefore.y - graphBoundsAfter.y) * 0.2696
    );
  }

  hideUi() {
    const graph: mxGraph = this.editorUi.editor.graph;
    const graphContainer: HTMLElement = graph.container;
    if (
      this.savedForHideUi === null &&
      document.fullscreenElement != graphContainer
    ) {
      const resizeListener = window["mxListenerList"].find(
        (l: ListenerType) => l.name === "resize"
      );
      this.savedForHideUi = new SavedGraphContainerMargins(
        resizeListener,
        graphContainer.style.left,
        graphContainer.style.right,
        graphContainer.style.top,
        graphContainer.style.bottom
      );

      const scrollLeft = graphContainer.scrollLeft;
      const scrollTop = graphContainer.scrollTop;

      mxEvent.removeListener((window as Window), "resize", resizeListener["f"]);
      // Specify a stack order in case you're using a different order for other elements
      // Using zIndex="1" the element with class geToolbarContainer remains in
      // front of the graph.
      graphContainer.style.zIndex = "2";
      graphContainer.style.position = "fixed";
      graphContainer.style.left = "0";
      graphContainer.style.right = "0";
      graphContainer.style.top = "0";
      graphContainer.style.bottom = "0";

      // Better (still not prefect) centering after UI is hidden.
      const gb1 = graph.view.graphBounds;
      graph.refresh();
      const gb2 = graph.view.graphBounds;
      const deltaScroll = EditorUiManager.uitoggleCalculateDeltaScroll(
        gb1,
        gb2
      );
      graphContainer.scrollLeft = scrollLeft + deltaScroll.x;
      graphContainer.scrollTop = scrollTop + deltaScroll.y;
    }
  }

  showUi() {
    const graph: mxGraph = this.editorUi.editor.graph;
    const container: HTMLElement = graph.container;
    if (
      this.savedForHideUi !== null &&
      document.fullscreenElement !== container
    ) {
      const scrollLeft = container.scrollLeft;
      const scrollTop = container.scrollTop;

      container.style.zIndex = "";
      container.style.position = "";
      mxEvent.addListener(
        window,
        "resize",
        this.savedForHideUi.resizeListener["f"]
      );
      container.style.left = this.savedForHideUi.left;
      container.style.right = this.savedForHideUi.right;
      container.style.top = this.savedForHideUi.top;
      container.style.bottom = this.savedForHideUi.bottom;

      // Better (still not prefect) centering after UI is hidden.
      const gb1 = graph.view.graphBounds;
      graph.refresh();
      const gb2 = graph.view.graphBounds;
      const deltaScroll = EditorUiManager.uitoggleCalculateDeltaScroll(
        gb1,
        gb2
      );
      container.scrollLeft = scrollLeft - deltaScroll.x;
      container.scrollTop = scrollTop - deltaScroll.y;

      this.savedForHideUi = null;
    }
  }

  getFileDataRawXmlStr() {
    // Reference:
    // https://github.com/jgraph/drawio/blob/16d9dab274eb440926def4d53b9e7d4d52b7763a/src/main/webapp/js/diagramly/EditorUi.js#L1542
    return this.editorUi.getFileData(
      true, // forceXml
      null, // forceSvg
      null, // forceHtml
      null, // embeddedCallback
      true, // ignoreSelection
      false, // currentPage
      null, // node
      null, // compact
      null, // file
      true // uncompressed
    );
  }

  saveFileAsRawXml() {
    // Only Electron app is supported
    if (window.mxIsElectron) {
      if (typeof this.editorUi.currentFile.fileObject === "undefined") {
        alert(
          "currentFile.fileObject === 'undefined'.\n" +
          "Maybe use saveAs first to give the file a name?"
        );
        return;
      }
      var currentFilePath: string = this.editorUi.currentFile.fileObject.path;
      if (typeof currentFilePath === "undefined" || currentFilePath === null) {
        alert("currentFile has no path: can't save.");
      } else {
        const xmlRawStr = getFileDataRaw(this.editorUi);
        const fs = require("fs");
        try {
          fs.writeFileSync(currentFilePath, xmlRawStr, "utf-8");
          this.editorUi.currentFile.setModified(false);
          this.editorUi.editor.setStatus(
            //@ts-ignore
            mxUtils.htmlEntities(mxResources.get("saved")) + " " + new Date()
          );
        } catch (e) {
          alert("Failed to save the file !");
        }
      }
    }
  }

  static toggleGraphContainerFullscreen(graph: mxGraph) {
    if (document.fullscreenElement === graph.container) {
      document.exitFullscreen();
    } else {
      graph.container.requestFullscreen();
    }
  }

  static setNewRubberbandStartBoxSelection(euiManager: EditorUiManager) {
    euiManager.rubberband = new mxRubberband(euiManager.editorUi.editor.graph);
    const startX = euiManager.mouseManager.lastMouseMoveData.mxMouseEvt.graphX;
    const startY = euiManager.mouseManager.lastMouseMoveData.mxMouseEvt.graphY;
    euiManager.rubberband.start(startX, startY);
    //@ts-ignore
    const rubberbandMouseMove = euiManager.rubberband.mouseMove;
    //@ts-ignore
    euiManager.rubberband.mouseMove = function (
      _sender: mxGraph,
      _me: mxMouseEvent
    ) {
      rubberbandMouseMove.apply(euiManager.rubberband, arguments);
      // Workaround for Electron: for some reason rubberband.mouseMove
      // receives events only when the mouse pointer is outside of its
      // div. Here we are "connecting" rubberband's div with
      // rubberband's mouseMove using as a bridge:
      // 1. a new mousemove listener on rubberband's div itself
      // 2. graph.fireMouseEvent: the fired event eventually gets
      //    propagated to rubberband.mouseMove
      //@ts-ignore
      const div: HTMLDivElement = euiManager.rubberband.div;
      // Add listener only once.
      if (div !== null && typeof div["customListenerAdded"] === "undefined") {
        // TODO: maybe instead of modifying div use one of the following
        // approaches?
        // - RubberbandManager class that keeps track of rubberband's div
        // - Custom listener object with recognizable field
        div["customListenerAdded"] = true;
        mxEvent.addListener(div, "mousemove", function (evt: MouseEvent) {
          // We know evt has MouseEvent type because it's
          // triggered by "mousemove".
          const newMxMouseEvt = new mxMouseEvent(evt);
          newMxMouseEvt.consumed = false;
          newMxMouseEvt.graphX =
            euiManager.mouseManager.lastMouseMoveData.mxMouseEvt.graphX;
          newMxMouseEvt.graphY =
            euiManager.mouseManager.lastMouseMoveData.mxMouseEvt.graphY;
          const graph: mxGraph = euiManager.editorUi.editor.graph;
          graph.fireMouseEvent(mxEvent.MOUSE_MOVE, newMxMouseEvt, graph);
        });
      }
    };
  }

  static togglePageVisible(graph: mxGraph) {
    graph.pageVisible = !graph.pageVisible;
    graph.refresh();
  }

  static toggleGrid(graph: mxGraph) {
    graph.setGridEnabled(!graph.isGridEnabled());
    graph.refresh();
  }
}
