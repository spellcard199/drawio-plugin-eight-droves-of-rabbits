/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { getActionName } from "../../util/drawio/editorui";
import { EditorUiManager } from "../EditorUiManager";
import { ActionManager } from "../mxaction/ActionManager";

export class MenuManager {
  private euiManager: EditorUiManager;
  private menuName = "edr";
  private actionManager: ActionManager;

  constructor(euiManager: EditorUiManager) {
    this.euiManager = euiManager;
    this.actionManager = new ActionManager(euiManager);
  }

  // Most of these methods take an EditorUiManager instead of an EditorUi
  // so we can have better type checking, not because they need to (as of
  // now, mxgraph's type definitions do not include EditorUi).

  public enable() {
    this.installActionsAndMenu();
  }

  public disable() {
    this.uninstallActionsAndMenu();
  }

  private installActionsAndMenu() {
    if (MenuManager.menuAlreadyExists(this.euiManager, this.menuName)) {
      return;
    }
    const editorUi = this.euiManager.editorUi;
    this.actionManager.installActions();
    // TODO:
    // 1. Find menu and parent types with console.log
    // 2. Add type annotation
    // Add menu
    const menuManager = this;
    editorUi.menubar.addMenu(
      this.menuName,
      function (menu: mxPopupMenu, _parentMaybeNull) {
        menuManager.actionManager.getActions().forEach((action: any) => {
          const actionName = getActionName(
            menuManager.euiManager.editorUi,
            action
          );
          editorUi.menus.addMenuItem(menu, actionName);
        });
      }
    );

    // Reorder menubar
    editorUi.menubar.container.insertBefore(
      editorUi.menubar.container.lastChild,
      editorUi.menubar.container.lastChild.previousSibling.previousSibling
    );
  }

  public setTitleUnderlined(underlined: boolean) {
    Array.from(
      this.euiManager.editorUi.menubar.container.getElementsByTagName("a")
    )
      .filter((a: HTMLAnchorElement) => a.innerText === this.menuName)
      .forEach((a: HTMLAnchorElement) => {
        underlined
          ? (a.innerHTML = "<u>" + this.menuName + "</u>")
          : (a.innerHTML = "" + this.menuName + "");
      });
  }

  private uninstallActionsAndMenu() {
    this.actionManager.uninstallActions();
    for (let menu of this.euiManager.editorUi.menubar.container.children) {
      const menuElem: HTMLElement = menu;
      if (menuElem.innerText === this.menuName) {
        menuElem.remove();
      }
    }
  }

  static menuAlreadyExists(euiManager: EditorUiManager, menuName) {
    // editorUi.menubarContainer contains both:
    // - file name line
    // - menubar line
    // editorUi.menubar.container contains just:
    // - menubar line
    for (let menu of euiManager.editorUi.menubar.container.children) {
      const menuElem: HTMLElement = menu;
      if (menuElem.innerText === menuName) {
        return true;
      }
    }
    return false;
  }
}
