/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { KeyboardEventWrapper } from "../keyboard/KeyEventWrapper";
import { KeySequenceMap } from "../keyboard/KeySequenceMap";

export type Command<KSM extends KeySequenceMap> = {
  run: (kew: KeyboardEventWrapper, ksm: KSM) => any;
  name: string | null;
  doc: string | null;
};
