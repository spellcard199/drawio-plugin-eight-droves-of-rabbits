/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { KeyBinding } from "../../keyboard/KeyBinding";
import { ShowMode } from "./ShowMode";
import * as smComm from "./showModeDefaultCommands";

export const showModeDefaultMap: Array<KeyBinding<ShowMode>> = [
  [["q"], smComm.smQuit],
  [["+"], smComm.smOpacityIncrease],
  [["-"], smComm.smOpacityDecrease],
  [[" ", " ", "h"], smComm.smHelpKeybindings],
].map(KeyBinding.fromTuple);
