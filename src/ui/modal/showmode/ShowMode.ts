/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { ChecklistSection, checklistToHtml } from "../../../util/html/checklist";
import { EditorUiManager } from "../../EditorUiManager";
import { defaultHandleKeySequence } from "../../keyboard/defaultHandleKeySequence";
import { KeyBinding } from "../../keyboard/KeyBinding";
import { KeyboardEventWrapper } from "../../keyboard/KeyEventWrapper";
import { KeySequenceMap } from "../../keyboard/KeySequenceMap";
import { Command } from "../Command";
import { Mode } from "../Mode";
import { DivOverlay } from "./DivOverlay";
import { showModeDefaultMap } from "./showModeDefaultMap";

export class ShowMode extends Mode {
  modeName: string = ShowMode.name;
  active: boolean = true;
  keySeqMap: KeyBinding<KeySequenceMap>[] = [...showModeDefaultMap];
  divOverlay = new DivOverlay();
  previousMode: Mode | undefined = undefined;
  onQuit: (showMode: ShowMode) => void = (_showMode: ShowMode) => { };

  public static getInstanceMaybe(editorUiManager: EditorUiManager): ShowMode | null {
    for (let m of editorUiManager.modes.values()) {
      if (m.constructor.name === "ShowMode") {
        return m as ShowMode;
      }
    }
    return null;
  }

  public static getInstanceOrNew(euim: EditorUiManager): ShowMode {
    var showMode: ShowMode;
    showMode = ShowMode.getInstanceMaybe(euim);
    if (!showMode) {
      showMode = new ShowMode(euim);
      euim.modes.set(ShowMode.name, showMode);
    }
    return showMode;
  }

  resetKeySeqMap() {
    this.keySeqMap = [...showModeDefaultMap];
  }

  resetOnQuit() {
    this.onQuit = (_showMode: ShowMode) => { };
  }

  show(
    savePreviousMode: Mode,
    elem: HTMLElement,
    backgroundColor: string = "rgba(255,255,255,1)",
    opacity: string = "0.9"
  ) {
    this.previousMode = savePreviousMode;
    this.editorUiManager.modes.delete(savePreviousMode.modeName);
    this.divOverlay.show(elem, backgroundColor, opacity);
  }

  showChecklist(
    savePreviousMode: Mode,
    checklist: ChecklistSection[],
    backgroundColor?: string | "default",
  ): HTMLDivElement {
    // Create Element to show
    const divToShow = checklistToHtml(checklist)
    // Set up keybindings
    const moveUpDown = (ancestor: HTMLElement, n: number) => {
      const inputElems = Array.from(
        ancestor.getElementsByTagName("input")
      )
      const inputElemsAsElements = inputElems as Element[];
      if (inputElems.length > 0) {
        const curIndex = inputElemsAsElements.indexOf(document.activeElement);
        const newIndex = Math.min(Math.max(0, curIndex + n), inputElems.length - 1)
        inputElems[newIndex].focus()
        // If ancestor elem is scrollable, .focus() already scrolls into view,
        // hence this is not needed:
        // document.activeElement.scrollIntoView({
        //   behavior: 'auto',
        //   block: 'center',
        //   inline: 'center',
        // })
      }
    }
    const smFocusPreviousElement: Command<ShowMode> = {
      name: "smFocusPreviousElement",
      doc: "",
      run: (kew, _showMode) => {
        moveUpDown(divToShow, -1)
        kew.stopPropagation = true
      }

    }
    const smFocusNextElement: Command<ShowMode> = {
      name: "smFocusNextElement",
      doc: "",
      run: (kew, _showMode) => {
        moveUpDown(divToShow, 1)
        kew.stopPropagation = true
      }
    }
    this.keySeqMap.push(
      new KeyBinding(["k"], smFocusPreviousElement),
      new KeyBinding(["j"], smFocusNextElement),
    )
    // Actually show
    this.show(
      savePreviousMode,
      divToShow,
      backgroundColor,
      "0.9"
    );
    const inputElems = divToShow.getElementsByTagName("input")
    if (inputElems.length > 0) {
      inputElems[0].focus()
    }
    return divToShow
  }

  restorePreviousMode() {
    if (this.previousMode) {
      this.editorUiManager.modes.set(
        this.previousMode.modeName,
        this.previousMode
      );
    }
  }

  quit() {
    this.editorUiManager.modes.delete(this.modeName);
    this.resetKeySeqMap();
    this.divOverlay.removeDivFromDocument();
    this.onQuit && this.onQuit(this);
    this.resetOnQuit();
    this.restorePreviousMode();
    const graph: mxGraph = this.editorUiManager.getCurrentGraph();
    graph.container.focus();
  }

  handleKeySequence(kew: KeyboardEventWrapper): KeyboardEventWrapper {
    return defaultHandleKeySequence(kew, this, this.editorUiManager);
  }
}
