/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { createElemOverlayDiv } from "../../../util/html/elemoverlay";

export class DivOverlay {
  document: Document
  div: HTMLDivElement = undefined
  showedElem: HTMLElement

  constructor(doc: Document = document) {
    this.setNewDivFromDocument(doc)
  }

  show(
    elem: HTMLElement,
    backgroundColor: string = "rgba(255,255,255,1)",
    opacity: string = "0.9",
    doc: Document = document
  ) {
    if (!this.isDivInDocument()) {
      this.setNewDivFromDocument(doc, backgroundColor, opacity)
    }
    this.div.style.backgroundColor = backgroundColor
    this.showedElem = elem
    this.div.appendChild(elem)
    this.unhide()
  }

  setNewDivFromDocument(
    doc: Document = document,
    backgroundColor?: string,
    opacity?: string,
  ) {
    this.document = doc
    this.div = createElemOverlayDiv(doc, backgroundColor, opacity)
    this.document.body.appendChild(this.div)
    return this.div
  }

  addDivToDocument() {
    this.document.body.appendChild(this.div)
  }

  removeDivFromDocument() {
    this.div.remove()
  }

  isDivInDocument() {
    return this.div.parentElement === this.document.body
  }

  clear() {
    this.showedElem.remove()
  }

  unhide() {
    this.div.style.display = "block"
  }

  hide() {
    this.div.style.display = "none"
  }

}
