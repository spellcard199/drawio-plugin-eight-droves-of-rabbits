/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { ChecklistSection, checklistToHtml } from "../../../util/html/checklist";
import { createElemOverlayDiv } from "../../../util/html/elemoverlay";
import { KeyBinding } from "../../keyboard/KeyBinding";
import { Command } from "../Command";
import { ShowMode } from "./ShowMode";

export const smQuit: Command<ShowMode> = {
  name: "smQuit",
  doc: "",
  run: (kew, showMode) => {
    showMode.quit();
    kew.stopPropagation = true;
  },
};
export const smOpacityIncrease: Command<ShowMode> = {
  name: "smOpacityIncrease",
  doc: "",
  run: (kew, showMode) => {
    const div = showMode.divOverlay.div;
    const currentOpacity = parseFloat(div.style.opacity);
    if (currentOpacity <= 0.9) {
      div.style.opacity = (currentOpacity + 0.1).toString();
    }
    kew.stopPropagation = true;
  },
};
export const smOpacityDecrease: Command<ShowMode> = {
  name: "smOpacityDecrease",
  doc: "",
  run: (_kew, showMode) => {
    const div = showMode.divOverlay.div;
    const currentOpacity = parseFloat(div.style.opacity);
    if (currentOpacity >= 0.1) {
      div.style.opacity = (currentOpacity - 0.1).toString();
    }
  },
};

export const smHelpKeybindings: Command<ShowMode> = {
  name: "smHelpKeybindings",
  doc: "",
  run: (_kew, showMode) => {
    // For simplicity, avoid using a new ShowMode to display help for ShowMode.
    const ELEM_ID = "showModeKeybindings"
    if (document.getElementById(ELEM_ID)) {
      return
    }
    const newOverlayDivElem = createElemOverlayDiv(document)
    newOverlayDivElem.style.fontFamily = "Monospace"
    newOverlayDivElem.id = "showModeKeybindings"
    newOverlayDivElem.style.display = "block"
    newOverlayDivElem.style.zIndex = (
      parseInt(showMode.divOverlay.div.style.zIndex) + 1
    ).toString()
    const checklist: ChecklistSection[] = [{
      headingTitle: "Current show mode keybindings",
      formElementId: "showModeChecklist",
      headingLevel: "h5",
      checklist: showMode.keySeqMap.map(
        (kb: KeyBinding<ShowMode>) => kb.formatSimple()
      ),
    }]
    const innerDiv = checklistToHtml(checklist)
    newOverlayDivElem.appendChild(innerDiv)
    document.body.appendChild(newOverlayDivElem)
    const smQuitHelpCommand: Command<ShowMode> = {
      name: "smQuitHelp",
      doc: "",
      run: (_kew, showMode) => {
        if (newOverlayDivElem.parentElement === document.body) {
          newOverlayDivElem.remove()
        }
        showMode.keySeqMap.forEach((keybinding) => {
          if (keybinding.command === smQuitHelpCommand) {
            keybinding.command = smQuit
          }
        })
      }
    }
    showMode.keySeqMap.forEach((keybinding) => {
      if (keybinding.command === smQuit) {
        keybinding.command = smQuitHelpCommand
      } else if (keybinding.command === smHelpKeybindings) {

      }
    })
    // popup(content, true)
  },
};