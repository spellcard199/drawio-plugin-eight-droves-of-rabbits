/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { EditorUiManager } from "../../EditorUiManager";
import { KeyboardEventWrapper } from "../../keyboard/KeyEventWrapper";
import { Mode } from "../Mode";
import { EdgeState } from "./states/edge";
import { GrabState } from "./states/grab/GrabState";
import { MovementState } from "./states/movement/MovementState";
import { NormalState } from "./states/normal/NormalState";
import { RubberbandState } from "./states/rubberband/RubberbandState";
import { ScaleState } from "./states/scale/ScaleState";
import { ViLikeState } from "./states/ViLikeState";

export class ViLikeMode extends Mode {
  modeName: string = ViLikeMode.name;
  active: boolean = true;

  normal: NormalState = new NormalState(this);
  rubberband: RubberbandState = new RubberbandState(this);
  grab: GrabState = new GrabState(this);
  scale: ScaleState = new ScaleState(this);
  edgeState: EdgeState = new EdgeState(this);
  movement: MovementState = new MovementState(this);

  currentState: ViLikeState = this.normal;

  statusDiv: HTMLDivElement = (() => {
    const div = document.createElement("div");
    div.style.zIndex = "3";
    div.style.display = "none";
    div.style.position = "absolute";
    div.style.transform = "translate(-50%, -50%)";
    div.style.height = "35px";
    div.style.width = "35px";
    div.style.borderRadius = "50%";
    // statusDiv.style.border = "2px solid black"
    div.innerText = "N";
    return div;
  })();

  public static getInstance(
    editorUiManager: EditorUiManager
  ): ViLikeMode | null {
    for (let m of editorUiManager.modes.values()) {
      if (m.constructor.name === "ViLikeMode") {
        return m as ViLikeMode;
      }
    }
    return null;
  }

  private getTextForScale() {
    return "s: " + this.scale.currentScaleOptions.scaleFactorString;
  }

  displayStateIndicator(graph: mxGraph) {
    if (this.currentState === this.normal) {
      this.statusDiv.style.display = "none";
    } else {
      this.statusDiv.style.display = "";
      const leftOffset = graph.container.offsetLeft + 20;
      const topOffset = graph.container.offsetTop + 12;
      this.statusDiv.style.left = `${leftOffset}px`;
      this.statusDiv.style.top = `${topOffset}px`;
      if (this.currentState === this.rubberband) {
        this.statusDiv.innerText = "b";
      } else if (this.currentState === this.grab) {
        this.statusDiv.innerText = "g";
      } else if (this.currentState == this.movement) {
        this.statusDiv.innerText = "m"
      } else if (this.currentState === this.scale) {
        this.statusDiv.innerText = this.getTextForScale();
      } else if (this.currentState === this.edgeState) {
        this.statusDiv.innerText = "e"
      }
    }
  }

  constructor(editorUiManager: EditorUiManager) {
    super(editorUiManager);
    document.body.appendChild(this.statusDiv);
  }

  handleKeySequence(kew: KeyboardEventWrapper): KeyboardEventWrapper {
    if (
      document.activeElement ===
      this.editorUiManager.getCurrentGraph().container
    ) {
      return this.currentState.handleKeySequence(kew);
    } else {
      return kew;
    }
  }

  enable() {
    super.enable()
    this.movement.enable()
  }

  disable() {
    super.disable()
    this.movement.disable()
  }
}
