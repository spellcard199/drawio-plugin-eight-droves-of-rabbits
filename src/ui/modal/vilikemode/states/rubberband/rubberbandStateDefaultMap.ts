/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { KeyBinding } from "../../../../keyboard/KeyBinding";
import * as vlsComm from "../ViLikeStateDefaultCommands";
import { RubberbandState } from "./RubberbandState";
import * as rbComm from "./rubberbandStateDefaultCommands";

export const rubberbandStateDefaultMap: Array<KeyBinding<RubberbandState>> = [
  [["R"], rbComm.rbResetSelection],
  [["r"], rbComm.rbResetSelectionSwitchToNormal],
  [["Escape"], rbComm.rbResetSelectionSwitchToNormal],
  [["b"], rbComm.rbSetSelection],
  [["q"], rbComm.rbSwitchToNormal],
  [["e"], rbComm.rbCycleOptVerticesOnlySetSelection],
  [["a"], rbComm.rbCycleOptOperationSetSelection],
  [["x"], rbComm.rbCycleOptRubberbandRelationSetSelection],
  [["g"], rbComm.rbCycleOptGroupSetSelection],
  [["f"], vlsComm.vlsCenterOnPointer],
].map(KeyBinding.fromTuple);
