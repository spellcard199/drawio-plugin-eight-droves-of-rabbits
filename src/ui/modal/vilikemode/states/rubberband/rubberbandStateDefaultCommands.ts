/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { Command } from "../../../Command";
import { RubberbandState } from "./RubberbandState";

export const rbResetSelection: Command<RubberbandState> = {
  name: "rbResetSelection",
  doc: "",
  run: (kew, rubberbandState) => {
    rubberbandState.resetSelection();
    kew.stopPropagation = true;
  },
};
export const rbResetSelectionSwitchToNormal: Command<RubberbandState> = {
  name: "rbResetSelectionSwitchToNormal",
  doc: "",
  run: (kew, rubberbandState) => {
    rubberbandState.resetSelectionSwitchToNormal();
    kew.stopPropagation = true;
  },
};
export const rbSetSelection: Command<RubberbandState> = {
  name: "rbSetSelection",
  doc: "",
  run: (kew, rubberbandState) => {
    rubberbandState.setSelection();
    kew.stopPropagation = true;
  },
};
export const rbSwitchToNormal: Command<RubberbandState> = {
  name: "rbSwitchToNormal",
  doc: "",
  run: (kew, rubberbandState) => {
    rubberbandState.switchToNormal();
    kew.stopPropagation = true;
  },
};
export const rbCycleOptVerticesOnlySetSelection: Command<RubberbandState> = {
  name: "rbCycleOptVerticesOnlySetSelection",
  doc: "",
  run: (kew, rubberbandState) => {
    rubberbandState.cycleOptVerticesOnlySetSelection();
    kew.stopPropagation = true;
  },
};
export const rbCycleOptOperationSetSelection: Command<RubberbandState> = {
  name: "rbCycleOptOperationSetSelection",
  doc: "",
  run: (kew, rubberbandState) => {
    rubberbandState.cycleOptOperationSetSelection();
    kew.stopPropagation = true;
  },
};
export const rbCycleOptRubberbandRelationSetSelection: Command<RubberbandState> =
  {
    name: "rbCycleOptRubberbandRelationSetSelection",
    doc: "",
    run: (kew, rubberbandState) => {
      rubberbandState.cycleOptRubberbandRelationSetSelection();
      kew.stopPropagation = true;
    },
  };
export const rbCycleOptGroupSetSelection: Command<RubberbandState> = {
  name: "rbCycleOptGroupSetSelection",
  doc: "",
  run: (kew, rubberbandState) => {
    rubberbandState.cycleOptGroupSetSelection();
    kew.stopPropagation = true;
  },
};
export const rbCycleOptWaypointPlaceholdersSetSelection: Command<RubberbandState> =
  {
    name: "rbCycleOptWaypointPlaceholdersSetSelection",
    doc: "",
    run: (kew, rubberbandState) => {
      rubberbandState.cycleOptWaypointPlaceholdersSetSelection();
      kew.stopPropagation = true;
    },
  };
