/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export enum RubberbandRelation {
  contained,
  overlap,
  intersect,
}

export enum Operation {
  set,
  append,
  deselect,
}

export enum GroupWithChildren {
  include,
  exclude,
}

export enum VerticesOrEdges {
  vertices,
  edges,
  both,
}

export enum WaypointPlaceholders {
  yes,
  no,
}

export class RubberbandStateOptions {
  verticesOrEdges: VerticesOrEdges = VerticesOrEdges.vertices;
  rubberbandRelation: RubberbandRelation = RubberbandRelation.contained;
  operation: Operation = Operation.set;
  groupWithChildren: GroupWithChildren = GroupWithChildren.include;
  waypointPlaceholders: WaypointPlaceholders = WaypointPlaceholders.yes;
}
