/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { viewCoordToGraphCoordRectangle } from "../../../../../util/mxgraph/geometry/coord";
import {
  isRectangleOverlappingPath,
  isRectanglesIntersecting,
  isRectanglesOverlapping,
} from "../../../../../util/mxgraph/geometry/rectangle";
import { getWaypointsContainedByRectangle } from "../../../../../util/mxgraph/geometry/waypoint";
import { EditorUiManager } from "../../../../EditorUiManager";
import { mxRubberband } from "../../../../../util/mxgraph/types/rubberband";
import { getGeometryAbsolute } from "../../../../../util/mxgraph/geometry/absolute";

export function startBoxSelection(editorUiManager: EditorUiManager) {
  stopRubberband(editorUiManager);
  EditorUiManager.setNewRubberbandStartBoxSelection(editorUiManager);
}

export function stopRubberband(editorUiManager: EditorUiManager) {
  // At startup euiManager.rubberband is not set.
  if (
    typeof editorUiManager.rubberband !== "undefined" &&
    // Using div === null to know if previous box selection ended by
    // clicking. Note that in webapp clicking also triggers end of box
    // and selection change, and rubberband.div === null catches both
    // the cases where:
    // - we ended previous selection action from here
    // - clicking ended it
    //@ts-ignore
    editorUiManager.rubberband.div !== null
  ) {
    // destroy-ing rubberband each time is easier to think about.
    //@ts-ignore
    editorUiManager.rubberband.reset();
    //@ts-ignore
    editorUiManager.rubberband.destroy();
  }
}

export function isRubberbandRect(rb: mxRubberband) {
  return rb && rb.first && rb.x && rb.y && rb.width && rb.height
}

export function getRubberbandRect(rb: mxRubberband): mxRectangle | null {
  return isRubberbandRect(rb)
    ? new mxRectangle(rb.x, rb.y, rb.width, rb.height)
    : null
}

export function getCellsOverlappingRubberband(
  editorUiManager: EditorUiManager,
  intersectOnly: boolean
): mxCell[] {
  const graph: mxGraph = editorUiManager.editorUi.editor.graph;
  const rb: mxRubberband = editorUiManager.rubberband;
  const cellsOverlapping: mxCell[] = [];
  const rubberbandRect: mxRectangle | null = getRubberbandRect(rb);
  if (rubberbandRect) {
    const allCells = graph.getModel().getDescendants(graph.getDefaultParent());
    const model = graph.getModel();
    for (let cell of allCells) {
      if (cell.geometry) {
        if (model.isEdge(cell)) {
          const edgeState = graph.view.getState(cell)
          const edgePath = []
          if (edgeState) {
            // Prefer edgeState.absolutePoints.
            edgePath.push(...edgeState.absolutePoints);
          } else {
            // edgeState == undefined: happened once, don't know why.
            //
            // TODO: if the issue presents again, adapt the following code. I'm
            // unsure about what sourcePoint and targetPoint mean, since they do
            // not correspond to points in edgeState.absolutePoints, when state
            // exists, hence why I left this block with just "continue" instead
            // of this fallback code.
            //
            //   if (cell.geometry.sourcePoint && cell.geometry.targetPoint) {
            //     const ga = getGeometryAbsolute(cell)
            //     edgePath.push(ga.sourcePoint)
            //     ga.points.forEach(p => edgePath.push(p)),
            //     edgePath.push(ga.targetPoint)
            //   }
            continue
          }
          const isOverlapping = isRectangleOverlappingPath(
            rubberbandRect,
            edgePath,
            intersectOnly,
          )
          if (isOverlapping) {
            cellsOverlapping.push(cell)
          }
        } else if (model.isVertex(cell)) {
          const vertState = graph.view.getState(cell)
          let cellRect: mxRectangle;
          if (vertState) {
            // Prefer vertState.getPerimeterBounds().
            cellRect = vertState.getPerimeterBounds()
          } else {
            // Sometimes vertState is undefined.
            const ga = getGeometryAbsolute(cell);
            cellRect = new mxRectangle(ga.x, ga.y, ga.width, ga.height);
          }
          if (
            !intersectOnly &&
            isRectanglesOverlapping(rubberbandRect, cellRect)
          ) {
            cellsOverlapping.push(cell);
          } else if (
            intersectOnly &&
            isRectanglesIntersecting(rubberbandRect, cellRect)
          ) {
            cellsOverlapping.push(cell);
          }
        }
      }
    }
  }
  return cellsOverlapping;
}

export function getCellsContainedByRubberband(
  editorUiManager: EditorUiManager
): mxCell[] {
  const graph: mxGraph = editorUiManager.editorUi.editor.graph
  const rb: any = editorUiManager.rubberband
  const rect = getRubberbandRect(rb)
  return rect
    ? graph.getCells(rect.x, rect.y, rect.width, rect.height)
    : []
}

export function getWaypointsContainedByRubberband(
  editorUiManager: EditorUiManager
): Map<mxCell, number[]> {
  const graph: mxGraph = editorUiManager.editorUi.editor.graph;
  const rubberband: any = editorUiManager.rubberband;
  if (isRubberbandRect(rubberband)) {
    const rubberbandRectangle = getRubberbandRect(rubberband);
    const boundsRectangle = viewCoordToGraphCoordRectangle(
      rubberbandRectangle,
      graph.view.scale,
      graph.view.translate
    );
    return getWaypointsContainedByRectangle(graph, boundsRectangle);
  } else {
    return new Map();
  }
}

export function selectCellsAndStopBoxSelection(
  editorUiManager: EditorUiManager,
  cells: mxCell[]
) {
  const graph: mxGraph = editorUiManager.editorUi.editor.graph;
  if (cells.length > 0) {
    graph.setSelectionCells(cells);
  } else {
    // Don't change selection if no cells are selected: probably
    // user pressed the key twice erroneously.
  }
}
