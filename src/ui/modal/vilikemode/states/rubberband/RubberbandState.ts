/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { waypointPlaceholdersInsertFromEdge } from "../../../../../util/mxgraph/cell/waypointplaceholder";
import { EditorUiManager } from "../../../../EditorUiManager";
import { KeyBinding } from "../../../../keyboard/KeyBinding";
import { KeySequenceMap } from "../../../../keyboard/KeySequenceMap";
import { ViLikeState } from "../ViLikeState";
import { rubberbandStateDefaultMap } from "./rubberbandStateDefaultMap";
import {
  GroupWithChildren,
  Operation,
  RubberbandRelation,
  RubberbandStateOptions,
  VerticesOrEdges,
  WaypointPlaceholders,
} from "./RubberbandStateOptions";
import {
  getCellsContainedByRubberband,
  getCellsOverlappingRubberband,
  getWaypointsContainedByRubberband,
  startBoxSelection,
  stopRubberband,
} from "./util";

export class RubberbandState extends ViLikeState {
  keySeqMap: KeyBinding<KeySequenceMap>[] = rubberbandStateDefaultMap;
  selectionCellsBeforeRubberbandState: mxCell[] = [];
  defaultOptions: RubberbandStateOptions = new RubberbandStateOptions();
  currentSelectionOptions: RubberbandStateOptions =
    new RubberbandStateOptions();
  currentSelectionWaypointPlaceholders: mxCell[] = [];

  startRubberband() {
    this.viLikeMode.currentState = this;
    this.displayStatusIndicator();
    startBoxSelection(this.viLikeMode.editorUiManager);
    this.setSelectionCellsBeforeRubberbandState(
      this.viLikeMode.editorUiManager.editorUi.editor.graph.getSelectionCells()
    );
    this.currentSelectionOptions = { ...this.defaultOptions };
  }

  switchToNormal() {
    stopRubberband(this.viLikeMode.editorUiManager);
    this.viLikeMode.normal.startNormal();
  }

  setSelectionCellsBeforeRubberbandState(cells: mxCell[]) {
    this.selectionCellsBeforeRubberbandState = cells;
  }

  selectAndSwitchToNormal() {
    this.setSelection();
    this.switchToNormal();
  }

  removeCurrentSelectionWaypointPlaceholders() {
    const graph: mxGraph = this.viLikeMode.editorUiManager.getCurrentGraph();
    graph.model.beginUpdate();
    try {
      graph.removeCells(this.currentSelectionWaypointPlaceholders, false);
    } finally {
      graph.model.endUpdate();
    }
  }

  getSelectionCellsWithOpts(): mxCell[] {
    this.removeCurrentSelectionWaypointPlaceholders();
    const euim: EditorUiManager = this.viLikeMode.editorUiManager;
    const graph: mxGraph = euim.getCurrentGraph();
    let selectionCells: mxCell[] = [];
    switch (this.currentSelectionOptions.rubberbandRelation) {
      case RubberbandRelation.contained: {
        selectionCells = getCellsContainedByRubberband(euim);
        break;
      }
      case RubberbandRelation.overlap: {
        selectionCells = getCellsOverlappingRubberband(euim, false);
        break;
      }
      case RubberbandRelation.intersect: {
        selectionCells = getCellsOverlappingRubberband(euim, true);
        break;
      }
    }
    switch (this.currentSelectionOptions.verticesOrEdges) {
      case VerticesOrEdges.vertices: {
        selectionCells = selectionCells.filter((cell: mxCell) => cell.vertex);
        break;
      }
      case VerticesOrEdges.edges: {
        selectionCells = selectionCells.filter((cell: mxCell) => cell.edge);
        break;
      }
      case VerticesOrEdges.both: {
        break;
      }
    }
    switch (this.currentSelectionOptions.operation) {
      case Operation.set: {
        selectionCells = selectionCells;
        break;
      }
      case Operation.append: {
        selectionCells = selectionCells.concat(
          this.selectionCellsBeforeRubberbandState
        );
        break;
      }
      case Operation.deselect: {
        selectionCells = this.selectionCellsBeforeRubberbandState.filter(
          (cell: mxCell) => !selectionCells.includes(cell)
        );
        break;
      }
    }
    switch (this.currentSelectionOptions.groupWithChildren) {
      case GroupWithChildren.include: {
        break;
      }
      case GroupWithChildren.exclude: {
        selectionCells = selectionCells.filter(
          (cell: mxCell) => cell.getChildCount() === 0
        );
        break;
      }
    }
    switch (this.currentSelectionOptions.waypointPlaceholders) {
      case WaypointPlaceholders.yes: {
        const edgeWaypoints: Map<mxCell, number[]> =
          getWaypointsContainedByRubberband(this.viLikeMode.editorUiManager);
        edgeWaypoints.forEach((waypointIndices: number[], edge: mxCell) => {
          graph.model.beginUpdate();
          try {
            const waypointPlaceholders = waypointPlaceholdersInsertFromEdge(
              graph,
              edge,
              waypointIndices
            );
            waypointPlaceholders.forEach((wp: mxCell) => {
              selectionCells.push(wp);
              this.currentSelectionWaypointPlaceholders.push(wp);
            });
          } finally {
            graph.model.endUpdate();
          }
        });
        break;
      }
      case WaypointPlaceholders.no: {
        break;
      }
    }
    return selectionCells;
  }

  setSelection() {
    const selectionCells = this.getSelectionCellsWithOpts();
    const graph: mxGraph =
      this.viLikeMode.editorUiManager.editorUi.editor.graph;
    graph.setSelectionCells(selectionCells);
  }

  resetSelection() {
    const graph: mxGraph =
      this.viLikeMode.editorUiManager.editorUi.editor.graph;
    graph.setSelectionCells(this.selectionCellsBeforeRubberbandState);
  }

  resetSelectionSwitchToNormal() {
    this.resetSelection();
    this.switchToNormal();
  }

  cycleOptVerticesOnly() {
    switch (this.currentSelectionOptions.verticesOrEdges) {
      case VerticesOrEdges.vertices: {
        this.currentSelectionOptions.verticesOrEdges = VerticesOrEdges.edges;
        break;
      }
      case VerticesOrEdges.edges: {
        this.currentSelectionOptions.verticesOrEdges = VerticesOrEdges.both;
        break;
      }
      case VerticesOrEdges.both: {
        this.currentSelectionOptions.verticesOrEdges = VerticesOrEdges.vertices;
        break;
      }
    }
  }

  cycleOptVerticesOnlySetSelection() {
    this.cycleOptVerticesOnly();
    this.setSelection();
  }

  cycleOptRubberbandRelation() {
    switch (this.currentSelectionOptions.rubberbandRelation) {
      case RubberbandRelation.contained: {
        this.currentSelectionOptions.rubberbandRelation =
          RubberbandRelation.overlap;
        break;
      }
      case RubberbandRelation.overlap: {
        this.currentSelectionOptions.rubberbandRelation =
          RubberbandRelation.intersect;
        break;
      }
      case RubberbandRelation.intersect: {
        this.currentSelectionOptions.rubberbandRelation =
          RubberbandRelation.contained;
        break;
      }
    }
  }

  cycleOptRubberbandRelationSetSelection() {
    this.cycleOptRubberbandRelation();
    this.setSelection();
  }

  cycleOptOperation() {
    switch (this.currentSelectionOptions.operation) {
      case Operation.set: {
        this.currentSelectionOptions.operation = Operation.append;
        break;
      }
      case Operation.append: {
        this.currentSelectionOptions.operation = Operation.deselect;
        break;
      }
      case Operation.deselect: {
        this.currentSelectionOptions.operation = Operation.set;
        break;
      }
    }
  }

  cycleOptOperationSetSelection() {
    this.cycleOptOperation();
    this.setSelection();
  }

  cycleOptGroupWithChildren() {
    switch (this.currentSelectionOptions.groupWithChildren) {
      case GroupWithChildren.include: {
        this.currentSelectionOptions.groupWithChildren =
          GroupWithChildren.exclude;
        break;
      }
      case GroupWithChildren.exclude: {
        this.currentSelectionOptions.groupWithChildren =
          GroupWithChildren.include;
        break;
      }
    }
  }

  cycleOptGroupSetSelection() {
    this.cycleOptGroupWithChildren();
    this.setSelection();
  }

  cycleOptWaypointPlaceholders() {
    switch (this.currentSelectionOptions.waypointPlaceholders) {
      case WaypointPlaceholders.yes: {
        this.currentSelectionOptions.waypointPlaceholders =
          WaypointPlaceholders.no;
        break;
      }
      case WaypointPlaceholders.no: {
        this.currentSelectionOptions.waypointPlaceholders =
          WaypointPlaceholders.yes;
        break;
      }
    }
  }

  cycleOptWaypointPlaceholdersSetSelection() {
    this.cycleOptWaypointPlaceholders();
    this.setSelection();
  }
}
