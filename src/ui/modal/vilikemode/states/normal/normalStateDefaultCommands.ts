/**
 * Copyright (C) 2021-2023, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { ViLikeState } from "..";
import { selectCellsWithBrokenLinks } from "../../../../../customlink/check";
import { ChecklistSection } from "../../../../../util/html/checklist";
import { cellGeometrySetPreferredSizeIfAutosizeAndNotGroup } from "../../../../../util/mxgraph/cell/cellgeometry";
import { getAncestors } from "../../../../../util/mxgraph/cell/model";
import {
  waypointPlaceholderRemoveWaypointsForPlaceholders,
  waypointPlaceholderIs,
  waypointPlaceholderMoveWaypointToPlaceholder
} from "../../../../../util/mxgraph/cell/waypointplaceholder";
import {
  fitGroupToChildren,
  isGroup
} from "../../../../../util/mxgraph/geometry/group";
import { snapCellToGrid } from "../../../../../util/mxgraph/geometry/snap";
import { inkscapeOpenCurrentCellSVG } from "../../../../../util/platformspecific/nodejs/inkscape";
import { EditorUiManager } from "../../../../EditorUiManager";
import { KeyBinding, KeyboardEventWrapper, KeySequenceMap } from "../../../../keyboard";
import { openCustomLinksInSelection } from "../../../../util/customlinks";
import { Command } from "../../../Command";
import { ShowMode } from "../../../showmode/ShowMode";
import * as aceUtil from "./ace";
import { NormalState } from "./NormalState";

function reformatGraph(graph: mxGraph, selectionOnly: boolean = false) {
  const model: mxGraphModel = graph.model;
  const gridSizeOverride = graph.gridSize * 4;
  model.beginUpdate();
  try {
    const cellsToReformat = selectionOnly
      ? graph.getSelectionCells()
      : model.getDescendants(graph.getDefaultParent());
    // Autosize
    cellGeometrySetPreferredSizeIfAutosizeAndNotGroup(graph, cellsToReformat, true)
    const cellsToRemove: mxCell[] = [];
    cellsToReformat.forEach((cell: mxCell) => {
      // Snap cells to grid
      snapCellToGrid(graph, cell, gridSizeOverride);
      // Populate cellsToRemove
      if (waypointPlaceholderIs(cell)) {
        // Remove waypointPlaceholder cells
        waypointPlaceholderMoveWaypointToPlaceholder(graph, cell);
        cellsToRemove.push(cell);
      }
    });
    graph.removeCells(cellsToRemove, false);
    // Groups
    const allGroups = [];
    const topLevelGroups = [];
    graph.model.getDescendants(graph.getDefaultParent())
      .forEach((cell: mxCell) => {
        if (isGroup(graph, cell)) {
          allGroups.push(cell)
          if (cell.parent === graph.defaultParent) {
            topLevelGroups.push(cell)
          }
        }
      })
    // Fit all groups to children (fitGroupToChildren is recursive):
    // - must go after snapping so that it knows what geometry to fit to.
    // - must go after removeCells so that it doesn't fit around cells that
    //   are being removed.
    topLevelGroups.forEach((group: mxCell) => fitGroupToChildren(graph, group, true))
    // Move all groups to back: so they don't cover part of the edges to and
    // from contained cells. We need to reverse allGroups because:
    // 1. getDescendants returns outer before inner groups.
    // 2. If the outer group goes to back first, then when it's the inner
    //    group's turn to go to back, it brings along the edges it contains.
    // 3. What we want is the opposite:
    //    1. first: send to back the inner groups.
    //    2. then: send to back the outer groups, so that they don't cover the
    //       inner ones.
    allGroups.reverse()
    graph.orderCells(true, allGroups);
  } finally {
    model.endUpdate();
  }
}

export const nsFitGroupToChildren: Command<NormalState> = {
  name: "nsFitGroupToChildren",
  doc: "",
  run: (kew, normalState) => {
    const graph: mxGraph =
      normalState.viLikeMode.editorUiManager.getCurrentGraph();
    graph.model.beginUpdate();
    try {
      fitGroupToChildren(graph, graph.getSelectionCell(), true);
      graph.refresh();
    } finally {
      graph.model.endUpdate();
    }
    kew.stopPropagation = true;
  },
};
export const nsShowModeExample: Command<NormalState> = {
  name: "nsShowModeExample",
  doc: "",
  run: (kew, normalState) => {
    const euim = normalState.viLikeMode.editorUiManager;
    const showMode = ShowMode.getInstanceOrNew(euim);
    const divToShow = document.createElement("div");
    divToShow.innerHTML =
      "TODO (this is just a an example. Press Q to exit show-mode.)";
    showMode.show(
      normalState.viLikeMode,
      divToShow,
      "rgba(255,255,255,1)",
      "0.9"
    );
    kew.stopPropagation = true;
  },
};
export const nsSwitchToRubberband: Command<NormalState> = {
  name: "nsSwitchToRubberband",
  doc: "",
  run: (kew, normalState) => {
    normalState.switchToRubberband();
    kew.stopPropagation = true;
  },
};
export const nsInsertDefaultVertex: Command<NormalState> = {
  name: "nsInsertDefaultVertex",
  doc: "",
  run: (kew, normalState) => {
    normalState.viLikeMode.editorUiManager.insertDefaultVertex(
      normalState.viLikeMode.editorUiManager.editorUi.editor.graph
    );
    kew.stopPropagation = true;
  },
};
export const nsSnapAndRefitGroupsSelectionOnly: Command<NormalState> = {
  name: "nsSnapAndRefitGroupsSelectionOnly",
  doc: "",
  run: (kew, normalState) => {
    const graph: mxGraph =
      normalState.viLikeMode.editorUiManager.editorUi.editor.graph;
    reformatGraph(graph, true);
    kew.stopPropagation = true;
  },
};
export const nsSnapAndRefitGroupsAllGraph: Command<NormalState> = {
  name: "nsSnapAndRefitGroupsAllGraph",
  doc: "",
  run: (kew, normalState) => {
    const graph: mxGraph =
      normalState.viLikeMode.editorUiManager.editorUi.editor.graph;
    reformatGraph(graph, false);
    kew.stopPropagation = true;
  },
};
export const nsSwitchToScale: Command<NormalState> = {
  name: "nsSwitchToScale",
  doc: "",
  run: (kew, normalState) => {
    normalState.switchToScale();
    kew.stopPropagation = true;
  },
};
export const nsAceEditCurrentCellLabel: Command<NormalState> = {
  name: "nsAceEditCurrentCellLabel",
  doc: "",
  run: (kew, normalState) => {
    const euim = normalState.viLikeMode.editorUiManager;
    // We don't want the I to be inserted in the text edited
    kew.evt.preventDefault();
    aceUtil.editCurrentCellLabel(euim);
    kew.stopPropagation = true;
  },
};
export const nsApplyGraphMapper: Command<NormalState> = {
  name: "nsApplyGraphMapper",
  doc: "",
  run: (kew, normalState) => {
    normalState.viLikeMode.editorUiManager.graphMapper(
      normalState.viLikeMode.editorUiManager.editorUi.editor.graph
    );
    kew.stopPropagation = true;
  },
};
export const nsUiToggle: Command<NormalState> = {
  name: "nsUiToggle",
  doc: "",
  run: (kew, normalState) => {
    normalState.viLikeMode.editorUiManager.toggleUi();
    kew.stopPropagation = true;
  },
};
export const nsUiToggleGraphContainerFullscreen: Command<NormalState> = {
  name: "nsUiToggleGraphContainerFullscreen",
  doc: "",
  run: (kew, normalState) => {
    const graph: mxGraph =
      normalState.viLikeMode.editorUiManager.editorUi.editor.graph;
    EditorUiManager.toggleGraphContainerFullscreen(graph);
    kew.stopPropagation = true;
  },
};
export const nsPageVisibleToggle: Command<NormalState> = {
  name: "nsPageVisibleToggle",
  doc: "",
  run: (kew, normalState) => {
    EditorUiManager.togglePageVisible(
      normalState.viLikeMode.editorUiManager.editorUi.editor.graph
    );
    kew.stopPropagation = true;
  },
};
export const nsSwitchToGrab: Command<NormalState> = {
  name: "nsSwitchToGrab",
  doc: "",
  run: (kew, normalState) => {
    normalState.switchToGrab();
    kew.stopPropagation = true;
  },
};
export const nsSwitchToMovement: Command<NormalState> = {
  name: "nsSwitchToMovement",
  doc: "",
  run: (kew, normalState) => {
    normalState.switchToMovement();
    kew.stopPropagation = true;
  },
};
export const nsOpenCustomLinkInSelection: Command<NormalState> = {
  name: "nsOpenCustomLinkInSelection",
  doc: "",
  run: (kew, normalState) => {
    openCustomLinksInSelection(normalState.viLikeMode.editorUiManager);
    kew.stopPropagation = true;
  },
};
export const nsSaveFileAsRawXml: Command<NormalState> = {
  name: "nsSaveFileAsRawXml",
  doc: "",
  run: (kew, normalState) => {
    normalState.viLikeMode.editorUiManager.saveFileAsRawXml();
    kew.stopPropagation = true;
  },
};
export const nsSelectCellsWithBrokenLinks: Command<NormalState> = {
  name: "nsSelectCellsWithBrokenLinks",
  doc: "",
  run: (kew, normalState) => {
    selectCellsWithBrokenLinks(normalState.viLikeMode.editorUiManager.editorUi);
    kew.stopPropagation = true;
  },
};
export const nsMarkGraphCellsAddSelected: Command<NormalState> = {
  name: "nsMarkGraphCellsAddSelected",
  doc: "",
  run: (_kew, normalState) => {
    const euim = normalState.viLikeMode.editorUiManager
    euim.markGraphCellsSelections.addCurrentSelectionWithPrompt()
  }
}
export const nsMarkGraphCellsSetSelection: Command<NormalState> = {
  name: "nsMarkGraphCellsSetSelection",
  doc: "",
  run: (_kew, normalState) => {
    const vlm = normalState.viLikeMode
    const euim = vlm.editorUiManager
    euim.markGraphCellsSelections.selectMarkedCellsWithPicker(
      euim.getCurrentGraph(), vlm
    )
  }
}
export const nsShowHelp: Command<NormalState> = {
  name: "nsShowHelp",
  doc: "",
  run: (kew, normalState) => {
    nsShowHelp
    const vlm = normalState.viLikeMode
    const euim = vlm.editorUiManager;
    const states = [vlm.grab, vlm.normal, vlm.rubberband, vlm.scale]
    const getChecklistSection = (vls: ViLikeState) => {
      const sectionChecklist = vls.keySeqMap.map(
        (kb: KeyBinding<KeySequenceMap>) => {
          const ks = JSON.stringify(kb.keySequence)
          const cmd = kb.command
          if (cmd.name) {
            return ks + "  " + cmd.name
          } else {
            return ks + "  " + cmd.run.toString()
          }
        })
      const section: ChecklistSection = {
        checklist: sectionChecklist,
        headingTitle: vls.constructor.name,
        headingLevel: 'h5',
        formElementId: null
      }
      return section
    }
    const checklistData: ChecklistSection[] = states.map(getChecklistSection)
    const showMode = ShowMode.getInstanceOrNew(euim);
    const showedDiv = showMode.showChecklist(normalState.viLikeMode, checklistData)
    showedDiv.style.fontFamily = "Monospace"
    kew.stopPropagation = true;
  },
};
export const nsRemoveWaypointsForSelectedPlaceholders: Command<NormalState> = {
  name: "nsRemoveWaypointsForSelectedPlaceholders",
  doc: "",
  run: (kew, normalState) => {
    const graph: mxGraph =
      normalState.viLikeMode.editorUiManager.getCurrentGraph();
    graph.model.beginUpdate();
    try {
      waypointPlaceholderRemoveWaypointsForPlaceholders(
        graph, graph.getSelectionCells()
      );
    } finally {
      graph.model.endUpdate();
    }
    kew.stopPropagation = true;
  },
};
export const nsInkscapeEditSvgActiveCell: Command<NormalState> = {
  name: "nsInkscapeEditSvgActiveCell",
  doc: "",
  run: (kew, normalState) => {
    const graph: mxGraph =
      normalState.viLikeMode.editorUiManager.getCurrentGraph();
    graph.model.beginUpdate();
    try {
      inkscapeOpenCurrentCellSVG(graph)
      waypointPlaceholderRemoveWaypointsForPlaceholders(
        graph, graph.getSelectionCells()
      );
    } finally {
      graph.model.endUpdate();
    }
    kew.stopPropagation = true;
  },
};
export const nsSwitchToEdgeState: Command<NormalState> = {
  name: "nsSwitchToEdgeState",
  doc: "",
  run: (kew, normalState) => {
    normalState.switchToEdgeState()
    kew.stopPropagation = true;
  },
}
export const nsUnhideAll: Command<NormalState> = {
  name: "nsUnhideAll",
  doc: "",
  run: (kew, normalState) => {
    const graph = normalState.viLikeMode.editorUiManager.getCurrentGraph()
    graph.getModel().beginUpdate()
    try {
      graph.getModel().getDescendants(graph.getDefaultParent())
        .forEach((c: mxCell) => {
          c.setVisible(true)
        })
    } finally {
      graph.getModel().endUpdate()
      graph.refresh()
    }
    kew.stopPropagation = true;
  }
}
export const nsHideSelected: Command<NormalState> = {
  name: "nsHideNotSelected",
  doc: "",
  run: (kew, normalState) => {
    const graph = normalState.viLikeMode.editorUiManager.getCurrentGraph()
    graph.getModel().beginUpdate()
    try {
      graph.getModel().getDescendants(graph.getDefaultParent())
        .forEach((c: mxCell) => {
          if (graph.isCellSelected(c)) {
            c.setVisible(false)
          }
        })
      graph.clearSelection()
    } finally {
      graph.getModel().endUpdate()
      graph.refresh()
    }
    kew.stopPropagation = true;
  }
}
export const nsHideNotSelected: Command<NormalState> = {
  name: "nsHideNotSelected",
  doc: "",
  run: (kew, normalState) => {
    const graph = normalState.viLikeMode.editorUiManager.getCurrentGraph()
    graph.getModel().beginUpdate()
    const defaultParent = graph.getDefaultParent()
    try {
      graph.getModel().getDescendants(defaultParent)
        .forEach((c: mxCell) => {
          let hideCell = true
          if (c === defaultParent) {
            // The array returned by getDescendants includes the cell you passed
            // as parent.
            hideCell = false
          } else if (graph.isCellSelected(c)) {
            hideCell = false
          } else {
            for (let ancestor of getAncestors(c)) {
              if (graph.isCellSelected(ancestor)) {
                hideCell = false;
                break
              }
            }
          }
          if (hideCell) {
            c.setVisible(false)
          }
        });
    } finally {
      // TODO: scroll so that position of selection on screen does not change.
      graph.getModel().endUpdate()
      graph.refresh()
    }
    kew.stopPropagation = true;
  },
}