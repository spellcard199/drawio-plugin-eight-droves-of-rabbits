/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { KeyBinding } from "../../../../keyboard/KeyBinding";
import * as vlsComm from "../ViLikeStateDefaultCommands";
import { NormalState } from "./NormalState";
import * as nsComm from "./normalStateDefaultCommands";

export const normalStateDefaultMap: Array<KeyBinding<NormalState>> = [
  [[" ", "u", "u"], nsComm.nsFitGroupToChildren],
  // [["M"], nsComm.nsShowModeExample],
  [["b"], nsComm.nsSwitchToRubberband],
  [["v"], nsComm.nsInsertDefaultVertex],
  [["e",], nsComm.nsSwitchToEdgeState],
  [[" ", "Shift", "S"], nsComm.nsSnapAndRefitGroupsSelectionOnly],
  [["S"], nsComm.nsSnapAndRefitGroupsAllGraph],
  [[" ", " ", "m"], nsComm.nsApplyGraphMapper],
  [["m", "m"], nsComm.nsMarkGraphCellsAddSelected],
  [["'", "'"], nsComm.nsMarkGraphCellsSetSelection],
  [[" ", " ", "s"], nsComm.nsSwitchToScale],
  [["I"], nsComm.nsAceEditCurrentCellLabel],
  [[" ", "Shift", "f"], nsComm.nsUiToggleGraphContainerFullscreen],
  [[" ", "f"], nsComm.nsUiToggle],
  [["f"], vlsComm.vlsCenterOnPointer],
  [["P"], nsComm.nsPageVisibleToggle],
  [["g"], nsComm.nsSwitchToGrab],
  [[" ", " ", "z"], nsComm.nsSwitchToMovement],
  [[" ", "o"], nsComm.nsOpenCustomLinkInSelection],
  [[":", "w"], nsComm.nsSaveFileAsRawXml],
  [[" ", "l", "l"], nsComm.nsSelectCellsWithBrokenLinks],
  [[" ", " ", "d", "w"], nsComm.nsRemoveWaypointsForSelectedPlaceholders],
  [[" ", " ", "ì"], nsComm.nsInkscapeEditSvgActiveCell],
  [[" ", " ", "ì"], nsComm.nsInkscapeEditSvgActiveCell],
  [[" ", " ", "Shift", "?"], nsComm.nsShowHelp],
  [[" ", "h"], nsComm.nsHideSelected],
  [[" ", "Shift", "H"], nsComm.nsHideNotSelected],
  [[" ", "Alt", "h"], nsComm.nsUnhideAll],
].map(KeyBinding.fromTuple);
