/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

// TODO: learn to bind custom functions to ace's Vim
// NOTE: Other resources:
// - "Give up on dynamically importing themes":
//   - https://github.com/ajaxorg/ace-builds/issues/129
//   - Only way I could make themes work (and also modes but only until CSP raises error)
// - Vim example:
//   - https://www.overleaf.com/learn/how-to/How_can_I_define_custom_Vim_macros_in_a_vimrc_file_on_Overleaf%3F
// - Monkey patching require:
//   - https://stackoverflow.com/questions/33178715/ace-editor-why-does-the-theme-mode-not-change
// NOTE: Cannot use modes because they use code that violate CSP...:
//   Error: Refused to create a worker from
//   'blob:file:///15137329-404a-4e85-b8f9-de4f186628d8' because it violates the
//   following Content Security Policy directive: "default-src 'self'
//   'unsafe-inline'". Note that 'worker-src' was not explicitly set, so
//   'default-src' is used as a fallback.
//// These didn't seem to work:
// import "ace-builds/webpack-resolver/"
// import * as ace from "ace-builds/src-noconflict/ace";
// This worked (source):
// https://stackoverflow.com/questions/60177855/theme-and-mode-path-infer-issue-with-ace-js-and-angular)
import * as ace from "ace-builds";
import { cellGeometrySetPreferredSize } from "../../../../../util/mxgraph/cell/cellgeometry";
import {
  cellValueGetLabelString,
  cellValueSetLabel
} from "../../../../../util/mxgraph/cell/cellvalue";
import { isGroup } from "../../../../../util/mxgraph/geometry/group";
import { EditorUiManager } from "../../../../EditorUiManager";
import { KeyBinding } from "../../../../keyboard/KeyBinding";
import { ShowMode } from "../../../showmode/ShowMode";
import { ViLikeMode } from "../../ViLikeMode";
import { Vim } from "ace-builds/src-noconflict/keybinding-vim"

ace.config.set("basePath", "/assets/ui/");
ace.config.set("modePath", "");
ace.config.set("themePath", "");

// Not using themes for the moment.
// const themeIdleFingers = require('ace-builds/src-noconflict/theme-idle_fingers');

// Can't use modes (CSP forbids them):
// const modeJavascript = require("ace-builds/src-noconflict/mode-javascript").Mode;
// (note for future) To use modes:
//   editor.session.setMode(new modeJavascript());

Vim.map("jk", "<Esc>", "insert");

export var aceEditor: ace.Ace.Editor = null;

// Beginning of attempt at using ace as in-place editor. I don't understand why
// (html/css/js noob) but while you type the div gets re-centered and the
// visibility of the text is intermittent. Since typing wasn't pleasant I
// stopped before adding the logic to update cell's value.
// export function editCurrentCellLabel(euim: EditorUiManager) {
//     const g = euim.getCurrentGraph();
//     g.startEditing()
//     var div = g.cellEditor.textarea as HTMLDivElement
//     if (div) {
//         // div.contentEditable = "true";
//         div.style.position = "absolute"
//         div.style.textAlign = "left"
//         editor = ace.edit(div)
//         editor.setKeyboardHandler("ace/keyboard/vim");
//         editor.getAnimatedScroll
//         editor.setOptions({
//             maxLines: Infinity,
//         });
//         editor.renderer.setShowGutter(false);
//         editor.setDisplayIndentGuides(false);
//         editor.setHighlightActiveLine(false);
//         editor.setShowFoldWidgets(false);
//         editor.setShowInvisibles(false);
//         editor.setShowPrintMargin(false);
//         editor.setOption('showLineNumbers', false);
//         editor.focus();
//     } else {
//     }
// }

export function editCurrentCellLabel(euim: EditorUiManager) {
  // Since editing the g.cellEditor.textarea div element seems to be harder to
  // make it work smootly, we are using a div inside ShowMode.

  const graph = euim.getCurrentGraph();
  const cell: mxCell = graph.getSelectionCell();

  const showMode = ShowMode.getInstanceOrNew(euim);

  const divToShow = document.createElement("div");
  divToShow.contentEditable = "true";
  divToShow.id = "editor";
  divToShow.style.position = "absolute";
  divToShow.style.top = "0px";
  divToShow.style.bottom = "0px";
  divToShow.style.left = "0px";
  divToShow.style.right = "0px";

  showMode.keySeqMap = [
    new KeyBinding(["j", "k"], {
      name: "",
      doc: "",
      run: (kew, _ksm) => {
        cellValueSetLabel(graph, cell, aceEditor.getValue());
        kew.stopPropagation = true;
      },
    }),
    new KeyBinding(["F4"], {
      name: "",
      doc: "",
      run: (kew, _ksm) => {
        cellValueSetLabel(graph, cell, aceEditor.getValue());
        showMode.quit();
        kew.stopPropagation = true;
      },
    }),
    new KeyBinding(["Shift", "Shift", "Q", "Q"], {
      name: "",
      doc: "",
      run: (kew, _ksm) => {
        // quit discarding changes
        showMode.quit();
        kew.stopPropagation = true;
      },
    }),
  ];
  showMode.show(
    ViLikeMode.getInstance(euim),
    divToShow,
    "rgba(255,255,255,1)",
    "0.8"
  );

  aceEditor = ace.edit("editor");
  aceEditor.setKeyboardHandler("ace/keyboard/vim");
  aceEditor.setValue(cellValueGetLabelString(cell));
  //// For more minimalism:
  // editor.renderer.setShowGutter(false);
  // editor.setDisplayIndentGuides(false);
  // editor.setHighlightActiveLine(false);
  // editor.setShowFoldWidgets(false);
  // editor.setShowInvisibles(false);
  // editor.setShowPrintMargin(false);
  // editor.setOption('showLineNumbers', false);
  aceEditor.commands.addCommand({
    name: "saveFile",
    bindKey: {
      win: "Ctrl-S",
      mac: "Command-S",
      // sender: 'editor|cli'
    },
    exec: function (editor: ace.Ace.Editor, _args) {
      // console.log('saving...', env, args, request);
      graph.model.beginUpdate();
      try {
        cellValueSetLabel(graph, cell, editor.getValue());
        if (
          cell.style.indexOf("autosize=1") > -1 &&
          !isGroup(graph, cell) &&
          cell.getChildCount() === 0
        ) {
          cellGeometrySetPreferredSize(graph, cell, true);
        }
      } finally {
        graph.model.endUpdate();
      }
      showMode.quit();
    },
  });
  aceEditor.commands.addCommand({
    name: "quit",
    bindKey: {
      win: "Shift-Escape",
      mac: "Shift-Escape",
      // sender: 'editor|cli'
    },
    exec: function (_editor: ace.Ace.Editor, _args) {
      // console.log('saving...', env, args, request);
      showMode.quit();
    },
  });
  //// Not needed:
  // editor.resize();
  // editor.renderer.updateFull();
  aceEditor.focus();
}

export { ace };
