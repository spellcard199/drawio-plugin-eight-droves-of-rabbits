/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { KeyBinding } from "../../../../keyboard/KeyBinding";
import { KeySequenceMap } from "../../../../keyboard/KeySequenceMap";
import { ViLikeState } from "../ViLikeState";
import { normalStateDefaultMap } from "./normalStateDefaultMap";

export class NormalState extends ViLikeState {
  keySeqMap: KeyBinding<KeySequenceMap>[] = normalStateDefaultMap;

  startNormal() {
    this.viLikeMode.currentState = this;
    this.displayStatusIndicator();
  }

  switchToRubberband() {
    this.viLikeMode.rubberband.startRubberband();
  }

  switchToGrab() {
    this.viLikeMode.grab.startGrab();
  }

  switchToScale() {
    this.viLikeMode.scale.startScale();
  }

  switchToEdgeState() {
    this.viLikeMode.edgeState.startEdgeState();
  }

  switchToMovement() {
    this.viLikeMode.movement.startMovement();
  }
}
