/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { defaultHandleKeySequence } from "../../../keyboard/defaultHandleKeySequence";
import { KeyBinding } from "../../../keyboard/KeyBinding";
import { KeyboardEventWrapper } from "../../../keyboard/KeyEventWrapper";
import { KeySequenceHandler } from "../../../keyboard/KeySequenceHandler";
import { KeySequenceMap } from "../../../keyboard/KeySequenceMap";
import { ViLikeMode } from "../ViLikeMode";

export abstract class ViLikeState
  implements KeySequenceMap, KeySequenceHandler
{
  viLikeMode: ViLikeMode;
  abstract keySeqMap: Array<KeyBinding<KeySequenceMap>>;

  constructor(viLikeMode: ViLikeMode) {
    this.viLikeMode = viLikeMode;
  }

  displayStatusIndicator() {
    this.viLikeMode.displayStateIndicator(
      this.viLikeMode.editorUiManager.getCurrentGraph()
    );
  }

  handleKeySequence(kew: KeyboardEventWrapper): KeyboardEventWrapper {
    return defaultHandleKeySequence(kew, this, this.viLikeMode.editorUiManager);
  }
}
