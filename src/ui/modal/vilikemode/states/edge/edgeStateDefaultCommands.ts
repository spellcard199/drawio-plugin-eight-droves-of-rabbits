/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { Command } from "../../../Command";
import { EdgeState } from "./EdgeState";

export const esSwitchToNormal: Command<EdgeState> = {
    name: "esSwitchToNormal",
    doc: "",
    run: (kew, edgeState) => {
        edgeState.switchToNormal();
        kew.stopPropagation = true;
    },
};
export const esCycleOptRelationSetSelection: Command<EdgeState> = {
    name: "esCycleOptRelation",
    doc: "",
    run: (kew, edgeState) => {
        edgeState.edgeSelection.cycleOptRelationSetSelection()
        kew.stopPropagation = true;
    },
};
export const esCycleOptOperationSetSelection: Command<EdgeState> = {
    name: "esCycleOptOperation",
    doc: "",
    run: (kew, edgeState) => {
        edgeState.edgeSelection.cycleOptOperationSetSelection()
        kew.stopPropagation = true;
    },
};
export const esSetSelection: Command<EdgeState> = {
    name: "esSetSelection",
    doc: "",
    run: (kew, edgeState) => {
        edgeState.edgeSelection.setSelection()
        kew.stopPropagation = true;
    },
};
export const esResetSelection: Command<EdgeState> = {
    name: "esResetSelection",
    doc: "",
    run: (kew, edgeState) => {
        edgeState.edgeSelection.resetSelection()
        kew.stopPropagation = true;
    },
};
export const esInsertDefaultEdges: Command<EdgeState> = {
    name: "esInsertDefaultEdges",
    doc: "",
    run: (kew, normalState) => {
        normalState.viLikeMode.editorUiManager.insertDefaultEdges(
            normalState.viLikeMode.editorUiManager.editorUi.editor.graph
        );
        kew.stopPropagation = true;
    },
};
export const esLastInsertedEdgesReverse: Command<EdgeState> = {
    name: "esLastInsertedEdgesReverse",
    doc: "",
    run: (kew, edgeState) => {
        edgeState.viLikeMode.editorUiManager.lastInsertedEdges.reverseDirection();
        kew.stopPropagation = true;
    },
};
export const esLastInsertedEdgeCycleAlternativeTargets: Command<EdgeState> = {
    name: "esLastInsertedCycleAlternativeTargets",
    doc: "",
    run: (kew, edgeState) => {
        edgeState.viLikeMode.editorUiManager.lastInsertedEdges.cycleAlternativeTargets()
        kew.stopPropagation = true
    },
};
export const esLastInsertedEdgesAmendTargets: Command<EdgeState> = {
    name: "esLastInsertedEdgesAmendTargets",
    doc: "",
    run: (kew, edgeState) => {
        const graph: mxGraph =
            edgeState.viLikeMode.editorUiManager.getCurrentGraph();
        const selectionCells: mxCell[] = graph.getSelectionCells()
        edgeState.viLikeMode.editorUiManager.lastInsertedEdges.amendTargets(selectionCells)
        kew.stopPropagation = true
    },
};