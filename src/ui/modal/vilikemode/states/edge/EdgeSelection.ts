/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { getEdgesBetweenCells, getEdgesIngoing, getEdgesOutgoing } from "../../../../../util/mxgraph/edges/selection";
import { EdgeSelectionRelation, EdgeSelectionOperation, EdgeSelectionOptions } from "./EdgeStateOptions";
import { EdgeState } from "./EdgeState"


export class EdgeSelection {
    edgeState: EdgeState
    options: EdgeSelectionOptions = new EdgeSelectionOptions()
    selectionCellsBeforeEdgeState: mxCell[] = [];

    public constructor(edgeState: EdgeState) {
        this.edgeState = edgeState
    }

    public getSelectionCellsWithOpts(graph: mxGraph, cells: mxCell[]): mxCell[] {
        let edges: mxCell[];
        switch (this.options.relation) {
            case EdgeSelectionRelation.betweenCells: {
                edges = Array.from(getEdgesBetweenCells(cells))
                break
            } case EdgeSelectionRelation.ingoing: {
                edges = Array.from(getEdgesIngoing(cells))
                break
            } case EdgeSelectionRelation.outgoing: {
                edges = Array.from(getEdgesOutgoing(cells))
                break
            }
        }
        let selectionCells: mxCell[] = []
        switch (this.options.operation) {
            case EdgeSelectionOperation.append: {
                selectionCells = cells.concat(edges);
                break
            } case EdgeSelectionOperation.set: {
                selectionCells = edges;
                break
            } case EdgeSelectionOperation.deselect: {
                selectionCells = graph.getSelectionCells()
                    .filter((c: mxCell) => !edges.includes(c))
                break
            }
        }
        return selectionCells
    }

    public cycleOptRelation() {
        switch (this.options.relation) {
            case EdgeSelectionRelation.betweenCells: {
                this.options.relation = EdgeSelectionRelation.ingoing
                break
            }
            case EdgeSelectionRelation.ingoing: {
                this.options.relation = EdgeSelectionRelation.outgoing
                break
            }
            case EdgeSelectionRelation.outgoing: {
                this.options.relation = EdgeSelectionRelation.betweenCells
                break
            }
        }
    }

    public cycleOptOperation() {
        switch (this.options.operation) {
            case EdgeSelectionOperation.set: {
                this.options.operation = EdgeSelectionOperation.append
                break
            }
            case EdgeSelectionOperation.append: {
                this.options.operation = EdgeSelectionOperation.deselect
                break
            }
            case EdgeSelectionOperation.deselect: {
                this.options.operation = EdgeSelectionOperation.set
                break
            }
        }

    }

    public resetSelection() {
        this.edgeState.getGraph().setSelectionCells(this.selectionCellsBeforeEdgeState)
    }

    public setSelection() {
        const graph: mxGraph = this.edgeState.getGraph()
        const cells = this.selectionCellsBeforeEdgeState
        const selectionCells = this.getSelectionCellsWithOpts(graph, cells)
        graph.setSelectionCells(selectionCells);
    }

    public cycleOptRelationSetSelection() {
        this.cycleOptRelation()
        this.setSelection()
    }

    public cycleOptOperationSetSelection() {
        this.cycleOptOperation()
        this.setSelection()
    }

}
