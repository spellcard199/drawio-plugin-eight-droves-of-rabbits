/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { KeyBinding } from "../../../../keyboard/KeyBinding";
import { KeySequenceMap } from "../../../../keyboard/KeySequenceMap";
import * as esComm from "./edgeStateDefaultCommands"
import * as vlsComm from "../ViLikeStateDefaultCommands";

export const edgeStateDefaultMap: KeyBinding<KeySequenceMap>[] = [
    [["q"], esComm.esSwitchToNormal],
    [["a"], esComm.esCycleOptOperationSetSelection],
    [["x"], esComm.esCycleOptRelationSetSelection],
    [["e"], esComm.esInsertDefaultEdges],
    [[" ", "t"], esComm.esLastInsertedEdgesAmendTargets],
    [["t"], esComm.esLastInsertedEdgeCycleAlternativeTargets],
    [["r"], esComm.esLastInsertedEdgesReverse],
    [["Shift", "R"], esComm.esResetSelection],
    [["f"], vlsComm.vlsCenterOnPointer],
].map(KeyBinding.fromTuple);;