/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { ViLikeState } from "..";
import { KeyBinding, KeySequenceMap } from "../../../../keyboard";
import { EdgeSelection } from "./EdgeSelection";
import { edgeStateDefaultMap } from "./edgeStateDefaultMap";

export class EdgeState extends ViLikeState {
    edgeSelection: EdgeSelection = new EdgeSelection(this);
    keySeqMap: KeyBinding<KeySequenceMap>[] = edgeStateDefaultMap;

    public getGraph: () => mxGraph = () => this.viLikeMode.editorUiManager.getCurrentGraph()

    public startEdgeState() {
        this.edgeSelection = new EdgeSelection(this); // new fresh state
        this.edgeSelection.selectionCellsBeforeEdgeState = this.getGraph().getSelectionCells()
        this.viLikeMode.currentState = this;
        this.displayStatusIndicator();
    }

    public switchToNormal() {
        this.viLikeMode.normal.startNormal();
    }

}