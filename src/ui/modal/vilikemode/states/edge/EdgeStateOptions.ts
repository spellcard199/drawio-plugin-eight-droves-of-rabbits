/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export enum EdgeSelectionRelation {
    betweenCells,
    ingoing,
    outgoing,
}

export enum EdgeSelectionOperation {
    set,
    append,
    deselect,
}

export class EdgeSelectionOptions {
    public relation: EdgeSelectionRelation = EdgeSelectionRelation.betweenCells
    public operation: EdgeSelectionOperation = EdgeSelectionOperation.set
}