/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { Command } from "../../../Command";
import { MovementState } from "./MovementState";

export const msSwitchToNormal: Command<MovementState> = {
  name: "msSwitchToNormal",
  doc: "",
  run: (kew, movementState) => {
    kew.stopPropagation = true;
    movementState.switchToNormal()
  },
};
export const msScrollTowardsPointer: Command<MovementState> = {
  name: "msScrollTowardsPointer",
  doc: "",
  run: (kew, movementState) => {
    const euim = movementState.viLikeMode.editorUiManager
    const graph = euim.getCurrentGraph()
    movementState.viLikeMode.editorUiManager.centerOnPointer(graph)
    graph.model.beginUpdate();
    kew.stopPropagation = true;
  },
};
export const nsUiToggle: Command<MovementState> = {
  name: "msUiToggle",
  doc: "",
  run: (kew, movementState) => {
    movementState.viLikeMode.editorUiManager.toggleUi();
    kew.stopPropagation = true;
  },
};