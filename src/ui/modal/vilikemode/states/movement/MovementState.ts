/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { KeyBinding } from "../../../../keyboard/KeyBinding";
import { KeySequenceMap } from "../../../../keyboard/KeySequenceMap";
import { ViLikeState } from "../ViLikeState";
import { movementStateDefaultMap } from "./movementStateDefaultMap";
import { window } from "../../../../../util/types/window"
import { ViLikeMode } from "../../ViLikeMode";

type MouseListenerData = {
  container: Node,
  type: keyof HTMLElementEventMap,
  callback: EventListenerOrEventListenerObject,
  options: EventListenerOptions,
  // Listeners saved with uninstallOnQuit: false, do not get uninstalled when
  // quitting MovemenState. Example: OnOffListeners that listen to mouse buttons
  // to trigger activation are installed in MovementState constructor and remain
  // active even when MovementState is not.
  uninstallOnQuit: boolean,
}

export class MovementState extends ViLikeState {
  keySeqMap: KeyBinding<KeySequenceMap>[] = movementStateDefaultMap
  saveZoomWheel: boolean = false

  readonly listeners: Array<MouseListenerData> = []

  constructor(vlm: ViLikeMode) {
    super(vlm)
    this.installOnOffListener()
  }

  enable() {
    this.disable()  // clean listeners before installing again.
    this.installOnOffListener()
  }

  disable() {
    this.uninstallListeners(true)
    this.quitMovement()
  }

  startMovement() {
    this.saveZoomWheel = this.graphGetZoomWheel()
    this.graphSetZoomWheel(true)
    this.uninstallListeners(false)  // shouldn't be needed, just in case.
    this.installMovementListeners()
    this.viLikeMode.currentState = this
    this.displayStatusIndicator()
  }

  quitMovement() {
    this.switchToNormal()
  }

  switchToNormal() {
    this.graphSetZoomWheel(this.saveZoomWheel)
    this.uninstallListeners(false)
    this.viLikeMode.normal.startNormal();
  }

  uninstallListeners(includeOnOffListeners: boolean) {
    this.listeners.forEach(listenerData => {
      if (includeOnOffListeners || listenerData.uninstallOnQuit) {
        listenerData.container.removeEventListener(
          listenerData.type,
          listenerData.callback,
          listenerData.options
        )
      }
    })
  }

  installEventListenerInGraphContainer(
    type: keyof HTMLElementEventMap,
    callback: (evt: Event) => any,
    options: EventListenerOptions,
    uninstallOnQuit: boolean,
  ) {
    const euim = this.viLikeMode.editorUiManager
    const container = euim.getCurrentGraph().container
    container.addEventListener(type, callback, options)
    this.listeners.push({
      container: container,
      type: type,
      callback: callback,
      options: options,
      uninstallOnQuit: uninstallOnQuit,
    })

  }

  installOnOffListener() {
    const movementState = this
    const container =
      this.viLikeMode.editorUiManager.getCurrentGraph().container
    const options: EventListenerOptions = { capture: true, }

    const pointerupType: keyof HTMLElementEventMap = "pointerup"
    const pointerupCallback: (evt: PointerEvent) => any = (evt) => {
      if (evt.button == 3) {
        evt.stopPropagation()
        const vlm = movementState.viLikeMode
        if (vlm.currentState === vlm.normal) {
          vlm.normal.switchToMovement()
        } else if (vlm.currentState === movementState) {
          vlm.movement.switchToNormal()
        }
      }
    }
    const pointerdownType: keyof HTMLElementEventMap = "pointerup"
    const pointerdownCallback: (evt: PointerEvent) => any = (evt) => {
      if (evt.button == 3) {
        // Prevent rubberbanding, etc.. when clicking 3rd mouse button.
        evt.stopPropagation()
      }
    }

    this.installEventListenerInGraphContainer(
      pointerupType, pointerupCallback, options, false
    )
    this.installEventListenerInGraphContainer(
      pointerdownType, pointerdownCallback, options, false
    )
    container.addEventListener("pointerdown", (evt: PointerEvent) => {
      if (evt.button == 3) {
        evt.stopPropagation()
      }
    }, { capture: true })
  }

  installMovementListeners() {
    const euim = this.viLikeMode.editorUiManager
    const options: EventListenerOptions = { capture: true, }
    // - "click": only triggered by left clicks.
    // - "contextmenu": only triggered by right clicks.
    // - "pointerdown": triggered 2 times for every mouse button press.
    // - "pointerup": triggered x? times for every mouse button press.
    const listenerData: Array<{
      type: keyof HTMLElementEventMap,
      callback: (evt: Event) => any,
    }> = [
        {
          type: "dblclick",
          callback: (evt: PointerEvent) => {
            if (evt.button == 0) {
              // Disable left double click.
              evt.stopPropagation()
            }
          }
        },
        {
          type: "pointerdown",
          callback: (evt: PointerEvent) => {
            if (evt.button == 0) {
              // Disable left click rubberband etc..
              evt.stopPropagation()
            }
          }
        },
        {
          type: "click",
          callback: (evt: MouseEvent) => {
            if (evt.button == 0) {
              evt.stopPropagation()
              euim.centerOnPointer(euim.getCurrentGraph())
            }
          }
        },
        // {
        //   type: "wheel",
        //   callback: (_evt: WheelEvent) => { }
        // },
      ]
    listenerData.forEach(l =>
      this.installEventListenerInGraphContainer(
        l.type, l.callback, options, true
      )
    )
  }

  private graphGetZoomWheel() {
    return window.Graph.zoomWheel
  }

  private graphSetZoomWheel(value: boolean) {
    window.Graph.zoomWheel = value
  }
}
