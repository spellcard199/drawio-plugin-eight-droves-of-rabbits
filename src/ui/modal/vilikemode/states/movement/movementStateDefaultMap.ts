/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { KeyBinding } from "../../../../keyboard/KeyBinding";
import { MovementState } from "./MovementState";
import * as msComm from "./movementStateDefaultCommands";

export const movementStateDefaultMap: Array<KeyBinding<MovementState>> = [
  [["q"], msComm.msSwitchToNormal],
  [[" ", "f"], msComm.nsUiToggle],
].map(KeyBinding.fromTuple);
