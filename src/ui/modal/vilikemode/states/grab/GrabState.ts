/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import {
  waypointPlaceholderIs,
  waypointPlaceholderMoveWaypointToPlaceholder,
} from "../../../../../util/mxgraph/cell/waypointplaceholder";
import {
  addCellToGroupKeepAbsolutePosition,
  fitGroupToChildren,
  isGroup,
} from "../../../../../util/mxgraph/geometry/group";
import { KeyBinding } from "../../../../keyboard/KeyBinding";
import { KeySequenceMap } from "../../../../keyboard/KeySequenceMap";
import { ViLikeState } from "../ViLikeState";
import { grabStateDefaultMap } from "./grabStateDefaultMap";
import { GrabStateOptions } from "./GrabStateOptions";
import { SelectionDataAtGrabStart } from "./SelectionDataWhenGrabStart";

export class GrabState extends ViLikeState {
  // Main reference:
  // https://github.com/jsGraph/mxgraph/blob/master/javascript/src/js/handler/mxGraphHandler.js

  keySeqMap: KeyBinding<KeySequenceMap>[] = grabStateDefaultMap;

  selectionDataAtGrabStart: SelectionDataAtGrabStart[] = [];
  graphHandler: mxGraphHandler | undefined = undefined;
  defaultOptions: GrabStateOptions = new GrabStateOptions();
  currentGrabOptions: GrabStateOptions = new GrabStateOptions();

  static getMxGraphHandlers(graph: mxGraph): mxGraphHandler[] {
    return graph.mouseListeners.filter((l) => l instanceof mxGraphHandler);
  }

  quitGrabIfInterruptedFromOutside: {
    [key: string]: (es: mxEventSource, me: mxMouseEvent) => void;
  } = {
      mouseDown: (_sender, _evt) => { },
      mouseMove: mxUtils.bind(this, (_sender: mxGraph, _meEvt: mxMouseEvent) => {
        if (
          typeof this.graphHandler !== "undefined" &&
          this.graphHandler.first === null
        ) {
          this.graphHandler.graph.isMouseDown = false;
          this.graphHandler.graph.removeMouseListener(
            this.quitGrabIfInterruptedFromOutside
          );
          this.switchToNormal();
        }
      }) as (es: mxEventSource, me: mxMouseEvent) => void,
      mouseUp: (_sender, _evt) => {
        // Detect mouseclick: grab can also be interrupted that way. However, do
        // not apply postGrab: since user did not use our keybindings, assume
        // drawio's default behavior is desired.
        this.switchToNormal()
      },
    };

  startGrab() {
    const graph: mxGraph = this.getCurrentGraph();
    // https://stackoverflow.com/questions/1232040/how-do-i-empty-an-array-in-javascript
    this.selectionDataAtGrabStart.length = 0;
    graph.getSelectionCells().forEach((cell: mxCell) => {
      this.selectionDataAtGrabStart.push(new SelectionDataAtGrabStart(cell));
    });
    if (this.selectionDataAtGrabStart.length === 0) {
      // Only start grabbing if there are selection cells, otherwise
      // instead of cell previews it starts a rubberband, which needs a
      // similar but different workaround (see RubberbandState.ts).
      return;
    }
    this.viLikeMode.currentState = this;
    this.displayStatusIndicator();
    this.currentGrabOptions = { ...this.defaultOptions };
    const graphHandlers: mxGraphHandler[] = GrabState.getMxGraphHandlers(graph);
    if (graphHandlers.length > 1) {
      console.log(
        "[WARNING] graph.mouseListeners has more than one mxGraphHandler."
      );
    } else if (graphHandlers.length === 0) {
      console.log("[WARNING] graph.mouseListeners has no mxGraphHandler.");
    } else {
      // Older, keeping for reference:
      //   this.graphHandler.start(cell, p.x, p.y)
      //   graph.isMouseDown = true
      // Newer:
      this.graphHandler = graphHandlers[0];
      const mme = this.mkMxMouseEventFromLast(graph, "mousedown", 0);
      graph.container.dispatchEvent(mme.evt);
      graph.fireMouseEvent(mxEvent.MOUSE_DOWN, mme, graph);
      graph.addMouseListener(this.quitGrabIfInterruptedFromOutside);
      this.installWorkaroundMouseMove(graph);
    }
  }

  getCurrentGraph(): mxGraph {
    return this.viLikeMode.editorUiManager.editorUi.editor.graph;
  }

  getNonNullCellHandlersFromStartData(): any[] {
    const graph: mxGraph = this.getCurrentGraph();
    const cellHandlers = this.selectionDataAtGrabStart
      .map((cellData: SelectionDataAtGrabStart) => cellData.cell)
      .map((cell: mxCell) => {
        try {
          return graph.selectionCellsHandler.getHandler(cell);
        } catch {
          return null;
        }
      })
      .filter((handlerOrNull) => handlerOrNull !== null);
    return cellHandlers;
  }

  installWorkaroundMouseMove(graph: mxGraph) {
    // Dirty workaround: when resizing preview gets on front, making
    // resizing somewhat harder (situation is similar to the rubberband
    // one in EditorUiManager.setNewRubberbandStartBoxSelection).
    const grabState = this;
    const cellHandlers = this.getNonNullCellHandlersFromStartData();
    cellHandlers.forEach((handler) => {
      if (handler instanceof mxVertexHandler) {
        const vertexHandler: mxVertexHandler = handler as mxVertexHandler;
        const preview = vertexHandler["preview"];
        if (preview !== null && typeof preview !== "undefined") {
          const svge: SVGElement = preview.node.ownerSVGElement;
          svge.onmousemove = function (me) {
            const newMxMouseEvt = new mxMouseEvent(me, vertexHandler.state);
            graph.fireMouseEvent(mxEvent.MOUSE_MOVE, newMxMouseEvt);
          };
        }
      }
    });
    // TODO: Consider alternative approaches to this workaround:
    // - Maybe add new listener to
    //   vertexHandler.preview.node.ownerSVGElement.mxListenerList, Eg:
    //   // var selectionCellsHandler = graph.mouseListeners.filter(l => l instanceof mxSelectionCellsHandler)[0]
    //   var vertexHandler = graph.selectionCellsHandler.handlers.map['mxCell#377']
    //   vertexHandler.preview.node.ownerSVGElement.mxListenerList
    // - Maybe override graph.selectionCellsHandler.mouseMove with a
    //   wrapper, as we did in EditorUiManager's rubberband
  }

  uninstallWorkaroundMouseMove() {
    const cellHandlers = this.getNonNullCellHandlersFromStartData();
    cellHandlers.forEach((handler) => {
      if (handler instanceof mxVertexHandler) {
        const vertexHandler: mxVertexHandler = handler as mxVertexHandler;
        const preview = vertexHandler["preview"];
        if (preview !== null && typeof preview !== "undefined") {
          const svge: SVGElement = preview.node.ownerSVGElement;
          svge.onmousemove = null;
        }
      }
    });
  }

  private stopGrab() {
    // stopGrab is private because it doesn't handle switching to normal. Use switchToNormal instead.
    if (
      typeof this.graphHandler !== "undefined" &&
      this.graphHandler.first !== null
    ) {
      // Older, keeping for reference:
      //   this.graphHandler.reset()
      //   this.graphHandler.graph.isMouseDown = false
      //   this.graphHandler = undefined
      // Newer:
      const graph: mxGraph = this.graphHandler.graph;
      const mme = this.mkMxMouseEventFromLast(graph, "mouseup", 0);
      graph.container.dispatchEvent(mme.evt);
      graph.fireMouseEvent(mxEvent.MOUSE_UP, mme, graph);
      graph.removeMouseListener(this.quitGrabIfInterruptedFromOutside);
      this.uninstallWorkaroundMouseMove();
      graph.model.beginUpdate();
      try {
        this.postGrab();
      } finally {
        graph.model.endUpdate();
      }
    }
  }

  postGrab() {
    const graph: mxGraph = this.graphHandler.graph;
    let refreshNeeded = false;
    graph.model.beginUpdate();
    try {
      // Restore selection cells: for some reason simulating clicking
      // changes selection.
      const selectionCells = this.selectionDataAtGrabStart.map(
        (cellData: SelectionDataAtGrabStart) => cellData.cell
      );
      graph.setSelectionCells(selectionCells);
      this.selectionDataAtGrabStart.forEach(
        (cellData: SelectionDataAtGrabStart) => {
          // Postfixes for groups
          if (this.currentGrabOptions.forcePreserveParents) {
            addCellToGroupKeepAbsolutePosition(
              graph,
              cellData.cell,
              cellData.parent
            );
            fitGroupToChildren(graph, cellData.parent);
          }
          const deltaGeom: mxGeometry = cellData.getCellCoordinateDelta();
          // If group has been resized on the top or left side: move children to
          // compensate for change in coordinate system.
          if (
            isGroup(graph, cellData.cell) &&
            (deltaGeom.width !== 0 || deltaGeom.height !== 0) &&
            (deltaGeom.x !== 0 || deltaGeom.y !== 0)
          ) {
            cellData.cell.children.forEach((child: mxCell) => {
              const newGeom: mxGeometry = child.geometry.clone();
              if (child.isVertex()) {
                newGeom.x -= deltaGeom.x;
                newGeom.y -= deltaGeom.y;
              }
              if (child.isEdge() && newGeom.points) {
                newGeom.points.forEach((p: mxPoint) => {
                  p.x -= deltaGeom.x;
                  p.y -= deltaGeom.y;
                });
              }
              graph.model.setGeometry(child, newGeom);
            });
          }
          // If cell is a waypointPlaceholder: move edge's waypoint.
          if (waypointPlaceholderIs(cellData.cell)) {
            waypointPlaceholderMoveWaypointToPlaceholder(graph, cellData.cell);
          }
        }
      );
      // Unused for the moment: always false.
      if (refreshNeeded) {
        graph.view.refresh();
      }
    } finally {
      graph.model.endUpdate();
    }
  }

  mkMxMouseEventFromLast(
    graph: mxGraph,
    eventType: "mousedown" | "mousemove" | "mouseup",
    button: number
  ): mxMouseEvent {
    const lastMME: mxMouseEvent =
      this.viLikeMode.editorUiManager.mouseManager.lastMouseMoveData.mxMouseEvt;
    // I don't know why, but if you don't do both...:
    // - set evt.relatedTarget
    // - call graph.container.dispatchEvent
    // ... "TypeError: cannot read property of null" happens when calling
    // graph.fireMouseEvent(...):
    const evt = new MouseEvent(eventType, {
      view: window,
      bubbles: true,
      button: button, // 0: Left; 1: Middle; 2: Right
      cancelable: true,
      relatedTarget: graph.container, // see previous comment
      // Needed to correctly position cells relatively to mouse. Fallback
      // to graph.tooltipHandler's coords.
      clientX: lastMME
        ? lastMME.getX()
        : //@ts-ignore
        graph.tooltipHandler.lastX,
      clientY: lastMME
        ? lastMME.getY()
        : //@ts-ignore
        graph.tooltipHandler.lastY,
    });
    const cell: mxCell = graph.getSelectionCell();
    const newMxMouseEvt = new mxMouseEvent(evt, graph.view.getState(cell));
    return newMxMouseEvt;
  }

  switchToNormal() {
    this.stopGrab();
    this.viLikeMode.normal.startNormal();
  }
}
