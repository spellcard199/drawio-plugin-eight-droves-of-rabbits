/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { getGeometryAbsolute } from "../../../../../util/mxgraph/geometry/absolute";

export class SelectionDataAtGrabStart {
  readonly cell: mxCell;
  readonly parent: mxCell;
  readonly startGeometryAbsolute: mxGeometry;

  public constructor(cell: mxCell) {
    this.cell = cell;
    this.parent = cell.parent;
    this.startGeometryAbsolute = getGeometryAbsolute(cell);
  }

  public getCellCoordinateDelta(): mxGeometry {
    const newGeometryAbsolute = getGeometryAbsolute(this.cell);
    newGeometryAbsolute.x -= this.startGeometryAbsolute.x;
    newGeometryAbsolute.y -= this.startGeometryAbsolute.y;
    newGeometryAbsolute.width -= this.startGeometryAbsolute.width;
    newGeometryAbsolute.height -= this.startGeometryAbsolute.height;
    if (
      newGeometryAbsolute.points &&
      this.startGeometryAbsolute.points &&
      newGeometryAbsolute.points.length ===
        this.startGeometryAbsolute.points.length
    ) {
      newGeometryAbsolute.points.forEach((p: mxPoint, i: number) => {
        p.x -= this.startGeometryAbsolute.points[i].x;
        p.y -= this.startGeometryAbsolute.points[i].y;
      });
    }
    return newGeometryAbsolute;
  }
}
