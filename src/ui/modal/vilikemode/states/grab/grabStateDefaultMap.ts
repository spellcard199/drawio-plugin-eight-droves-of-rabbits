/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { KeyBinding } from "../../../../keyboard/KeyBinding";
import * as vlsComm from "../ViLikeStateDefaultCommands";
import { GrabState } from "./GrabState";
import * as gsComm from "./grabStateDefaultCommands";

export const grabStateDefaultMap: Array<KeyBinding<GrabState>> = [
  [["g"], gsComm.gsSwitchToNormal],
  [["p"], gsComm.gsToggleForcePreserveParents],
  [["f"], vlsComm.vlsCenterOnPointer],
].map(KeyBinding.fromTuple);
