/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { Command } from "../../../Command";
import { GrabState } from "./GrabState";

export const gsSwitchToNormal: Command<GrabState> = {
  name: "gsSwitchToNormal",
  doc: "",
  run: (kew, grabState) => {
    grabState.switchToNormal();
    kew.stopPropagation = true;
  },
};
export const gsToggleForcePreserveParents: Command<GrabState> = {
  name: "gsToggleForcePreserveParents",
  doc: "",
  run: (kew, grabState) => {
    grabState.switchToNormal();
    kew.stopPropagation = true;
  },
};
