/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { Command } from "../../Command";
import { ViLikeState } from "./ViLikeState";

export const vlsCenterOnPointer: Command<ViLikeState> = {
  name: "vlsCenterOnPointer",
  doc: "",
  run: (kew, vlmState) => {
    vlmState.viLikeMode.editorUiManager.centerOnPointer(
      vlmState.viLikeMode.editorUiManager.editorUi.editor.graph
    );
    kew.stopPropagation = true;
  },
};