/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

export enum MultiplyOrDivide {
  multiply,
  divide,
}

export class ScaleStateOptions {
  multiplyOrDivide: MultiplyOrDivide = MultiplyOrDivide.multiply;
  scaleFactorString: string = "";
}
