/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { KeyBinding } from "../../../../keyboard/KeyBinding";
import { KeySequenceMap } from "../../../../keyboard/KeySequenceMap";
import { ViLikeState } from "../ViLikeState";
import { scaleStateDefaultMap } from "./scaleStateDefaultMap";
import { ScaleStateOptions } from "./ScaleStateOptions";

export class ScaleState extends ViLikeState {
  keySeqMap: KeyBinding<KeySequenceMap>[] = scaleStateDefaultMap;
  defaultOptions: ScaleStateOptions = new ScaleStateOptions();
  currentScaleOptions: ScaleStateOptions = new ScaleStateOptions();

  startScale() {
    this.viLikeMode.currentState = this;
    this.currentScaleOptions = { ...this.defaultOptions };
    // displayStatusIndicator() must go after resetting
    // currentScaleOptions, because it displays scaleFactorString.
    this.displayStatusIndicator();
  }

  switchToNormal() {
    this.viLikeMode.normal.startNormal();
  }
}
