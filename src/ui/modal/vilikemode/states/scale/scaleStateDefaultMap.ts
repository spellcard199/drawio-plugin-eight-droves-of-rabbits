/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { KeyBinding } from "../../../../keyboard/KeyBinding";
import { ScaleState } from "./ScaleState";
import * as ssComm from "./scaleStateDefaultCommands";

export const scaleStateDefaultMap: Array<KeyBinding<ScaleState>> = [
  [["s"], ssComm.ssScaleSelectionDistances],
  [["/"], ssComm.ssCycleScaleUpDown],
  [["q"], ssComm.ssSwitchToNormal],
  [["Backspace"], ssComm.ssPopLastDigit],
].map(KeyBinding.fromTuple);

// When a digit or a dot is pressed, append it to scaleFactorString
".1234567890".split("").forEach((s: string) => {
  scaleStateDefaultMap.push(
    new KeyBinding([s], {
      name: "scale state type: " + s,
      doc: "",
      run: (kew, scaleState: ScaleState) => {
        scaleState.currentScaleOptions.scaleFactorString += s;
        const graph: mxGraph =
          scaleState.viLikeMode.editorUiManager.getCurrentGraph();
        scaleState.viLikeMode.displayStateIndicator(graph);
        kew.stopPropagation = true;
      },
    })
  );
});
