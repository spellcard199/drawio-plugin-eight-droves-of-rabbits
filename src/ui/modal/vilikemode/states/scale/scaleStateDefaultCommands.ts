/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { scaleSelectionDistances } from "../../../../../util/mxgraph/geometry/scale";
import { Command } from "../../../Command";
import { ScaleState } from "./ScaleState";
import { MultiplyOrDivide } from "./ScaleStateOptions";

export const ssScaleSelectionDistances: Command<ScaleState> = {
  name: "ssScaleSelectionDistances",
  doc: "",
  run: (kew, scaleState) => {
    const graph: mxGraph =
      scaleState.viLikeMode.editorUiManager.getCurrentGraph();
    const scaleFactorParsed = parseFloat(
      scaleState.currentScaleOptions.scaleFactorString
    );
    const scaleFactor: number = isNaN(scaleFactorParsed)
      ? 1
      : scaleFactorParsed;
    scaleSelectionDistances(graph, scaleFactor);
    kew.stopPropagation = true;
  },
};
export const ssCycleScaleUpDown: Command<ScaleState> = {
  name: "ssCycleScaleUpDown",
  doc: "",
  run: (kew, scaleState) => {
    if (
      scaleState.currentScaleOptions.multiplyOrDivide ===
      MultiplyOrDivide.multiply
    ) {
      scaleState.currentScaleOptions.multiplyOrDivide = MultiplyOrDivide.divide;
    } else {
      scaleState.currentScaleOptions.multiplyOrDivide =
        MultiplyOrDivide.multiply;
    }
    kew.stopPropagation = true;
  },
};
export const ssSwitchToNormal: Command<ScaleState> = {
  name: "ssSwitchToNormal",
  doc: "",
  run: (kew, scaleState) => {
    scaleState.switchToNormal();
    kew.stopPropagation = true;
  },
};
export const ssPopLastDigit: Command<ScaleState> = {
  name: "ssPopLastDigit",
  doc: "",
  run: (kew, scaleState) => {
    const scaleFactorDigitsAsStrings: string[] =
      scaleState.currentScaleOptions.scaleFactorString.split("");
    console.log(scaleFactorDigitsAsStrings);
    // Pop last digit from scaleFactorString
    if (scaleFactorDigitsAsStrings.length !== 0) {
      scaleState.currentScaleOptions.scaleFactorString =
        scaleFactorDigitsAsStrings
          .slice(0, scaleFactorDigitsAsStrings.length - 1)
          .join("");
    }
    const graph: mxGraph =
      scaleState.viLikeMode.editorUiManager.getCurrentGraph();
    scaleState.viLikeMode.displayStateIndicator(graph);
    // If we don't stop propagation, Backspace's behavior is to delete selection cells.
    kew.evt.stopPropagation();
    kew.stopPropagation = true;
  },
};
