/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { EditorUiManager } from "../EditorUiManager";
import { KeySequenceHandler } from "../keyboard/KeySequenceHandler";

export abstract class Mode extends KeySequenceHandler {
  abstract active: boolean;
  abstract modeName: string;
  editorUiManager: EditorUiManager;
  constructor(editorUiManager: EditorUiManager) {
    super();
    this.editorUiManager = editorUiManager;
  }
  enable() { this.active = true }
  disable() { this.active = false }
}
