/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { Command } from "../modal/Command";
import { KeySequenceMap } from "./KeySequenceMap";

export class KeyBinding<KSM extends KeySequenceMap> {
  keySequence: Array<String>;
  command: Command<KSM>;

  constructor(keySequence: Array<string>, command: Command<KSM>) {
    this.keySequence = keySequence;
    this.command = command;
  }

  public static fromTuple(
    tuple2: [keySequence: Array<string>, command: Command<KeySequenceMap>]
  ): KeyBinding<KeySequenceMap> {
    return new KeyBinding(tuple2[0], tuple2[1]);
  }

  public formatSimple() {
    const ks = JSON.stringify(this.keySequence)
    const cmd = this.command
    if (cmd.name) {
      return ks + "  " + cmd.name
    } else {
      return ks + "  " + cmd.run.toString()
    }
  }
}

// // todo: specify that ksm is subclass of KeySequenceMap
// export class KeyBinding<KSM extends KeySequenceMap> {
//     keySequence: Array<String>
//     action: (kew: KeyboardEventWrapper, ksm: KSM) => any
//     constructor(
//         keySequence: Array<String>,
//         action: (keyboardEventWrapper: KeyboardEventWrapper, ksm: KSM) => any
//     ) {
//         this.keySequence = keySequence
//         this.action = action
//     }
// }
