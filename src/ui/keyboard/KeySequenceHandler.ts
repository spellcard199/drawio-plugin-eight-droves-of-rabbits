/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { KeyboardEventWrapper } from "./KeyEventWrapper";

export abstract class KeySequenceHandler {
  abstract handleKeySequence(kew: KeyboardEventWrapper): KeyboardEventWrapper;
}
