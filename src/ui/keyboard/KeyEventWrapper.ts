/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { Mode } from "../modal/Mode";

export class KeyboardEventWrapper {
  evt: KeyboardEvent;
  activeModesWhenFired: Map<string, Mode>;
  hasTriggeredAction: boolean = false;
  stopPropagation: boolean = false;
  constructor(evt: KeyboardEvent, activeModesWhenFired: Map<string, Mode>) {
    this.evt = evt;
    this.activeModesWhenFired = activeModesWhenFired;
  }
}
