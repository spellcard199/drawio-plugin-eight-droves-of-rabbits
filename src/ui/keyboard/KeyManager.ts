/**
 * Copyright (C) 2020-2023, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { mxKeyHandler, mxKeyHandlerKeys } from "../../util/types/drawio/mxKeyHandler";
import { EditorUiManager } from "../EditorUiManager";
import { KeyboardEventWrapper } from "./KeyEventWrapper";

export class KeyManager {
  isDebugKeys: boolean = false;
  editorUiManager: EditorUiManager;

  savedEditorUiKeyHandler: {
    normalKeys: mxKeyHandlerKeys
    controlKeys: mxKeyHandlerKeys
    shiftKeys: mxKeyHandlerKeys
    controlShiftKeys: mxKeyHandlerKeys
  } = {
      normalKeys: {},
      controlKeys: {},
      shiftKeys: {},
      controlShiftKeys: {},
    };

  constructor(editorUiManager: EditorUiManager) {
    this.editorUiManager = editorUiManager;
  }

  // kewBuffer is built by KeyManager.keydownHandler and is used to decide
  // if/which functions should be triggered.
  // 7 keys should be enough to match on key sequences.
  kewBuffer: Array<KeyboardEventWrapper> = new Array(7);
  documentBodyKeydownFunc: (keyEvt: KeyboardEvent) => void = mxUtils.bind(
    this,
    (keyEvt: KeyboardEvent) => {
      // When wet as the value of document.body.keydown, takes care of
      // building keyEvtBuffer. The reason we are not doing it with
      // editorUi.keypress is that some keys do no arrive there (e.g. the
      // space key). Actually some keys do not arrive here neither (e.g. DEL,
      // BACKSPACE) but at least here we can listen to the space bar. TODO:
      // find a way to listen to all keys.
      this.isDebugKeys && console.log("[document.body.keydown]");
      this.kewBuffer = this.kewBuffer.slice(1, this.kewBuffer.length);
      this.kewBuffer.push(
        new KeyboardEventWrapper(keyEvt, this.editorUiManager.modes)
      );
      this.isDebugKeys && KeyManager.logKeyEvtBuffer(this.kewBuffer);
      // TODO: just to try, I moved this.keyHandleKeySequence from
      // editoruiOnkeypressSubstitute to here and put a return in the latter. Try
      // to make an actual diagram and see if it works. If it does work it's
      // preferable because document.body is reached by some keys (e.g.
      // Escape, etc...) that do not reach editorUi.onKeyPress.
      this.handleKeySequence(keyEvt);
    }
  ) as (keyEvt: KeyboardEvent) => void;
  // Function produced by mxEvent.addListener that wraps documentBodyKeydownFunc.
  documentBodyKeydownListener: Function = null;

  editoruiOnkeypressOriginal: (keyEvt: KeyboardEvent) => void = null;
  editoruiOnkeypressSubstitute: (keyEvt: KeyboardEvent) => void = mxUtils.bind(
    this,
    (keyEvt: KeyboardEvent) => {
      return; // See TODO in 'documentBodyKeydownFunc'
      this.isDebugKeys && console.log("[editorUi.keypress]");
      // Handle key press...
      var kew: KeyboardEventWrapper = this.handleKeySequence(keyEvt);
      // ... then call original function the KeyboardEvent we passed is
      // returned.
      // TODO:
      // 1. Does anything bad happens if letters and
      //    numbers are stopped from being propagated?
      // 2. Maybe reserve letters and numbers for ourselves?
      //    You could still edit labels with F2.
      // To enable propagation of keys and numbers:
      //   return keyEvt;
      // To disable propagation of keys and numbers:
      //   return null;
      const propagateToDrawio: boolean =
        !KeyManager.isKeyEventAlphaNumeric(keyEvt);
      if (propagateToDrawio && this.editoruiOnkeypressOriginal !== null) {
        this.editoruiOnkeypressOriginal(kew.evt);
      }
    }
  ) as (keyEvt: KeyboardEvent) => void;

  private removeEditorUiKeyHandlerKeys() {
    const keyHandler: mxKeyHandler = this.editorUiManager.editorUi.keyHandler
    const savedNormalKeys: mxKeyHandlerKeys = this.savedEditorUiKeyHandler.normalKeys
    const normalKeys: mxKeyHandlerKeys = keyHandler.normalKeys as mxKeyHandlerKeys;

    Object.keys(normalKeys).forEach(key => {
      savedNormalKeys[key] = normalKeys[key]
      delete normalKeys[key]
    });

    // We don't want to remove controlKeys, shiftKeys, controlShiftKeys, because
    // these do not collide with our keybindings.  Actually, if we remove these,
    // our own keybindings starting with Ctrl or Shift stop working.
  }

  private restoreEditorUiKeyHandler() {
    const keyHandler: mxKeyHandler = this.editorUiManager.editorUi.keyHandler
    const savedNormalKeys: mxKeyHandlerKeys = this.savedEditorUiKeyHandler.normalKeys
    const normalKeys: mxKeyHandlerKeys = keyHandler.normalKeys as mxKeyHandlerKeys

    Object.keys(savedNormalKeys).forEach(key => {
      normalKeys[key] = savedNormalKeys[key]
      delete savedNormalKeys[key]
    })
  }

  public enable() {
    // Using mxEvent.addListener( this.emacsManager.editorUi.container, ... )
    // would make the listener end up in document.body.mxListenerList as well.
    mxEvent.addListener(document.body, "keydown", this.documentBodyKeydownFunc);
    // mxEvent.addListener doesn't return the listener it adds. Since the
    // listener we are interested in has been added just in the previous
    // line it must be the last in document.body['mxListenerList].
    const len = document.body["mxListenerList"].length;
    this.documentBodyKeydownListener =
      document.body["mxListenerList"][len - 1]["f"];
    // Replace editorUi.onKeyPress to trigger key bindings and stop further
    // propagation of key events.
    this.replaceOnKeyPress();
    this.removeEditorUiKeyHandlerKeys()
  }

  public disable() {
    mxEvent.removeListener(
      document.body,
      "keydown",
      this.documentBodyKeydownListener
    );
    this.restoreOnKeyPress();
    this.restoreEditorUiKeyHandler()
  }

  public replaceOnKeyPress() {
    if (this.editoruiOnkeypressOriginal === null) {
      // Save original function so we can still call it
      this.editoruiOnkeypressOriginal =
        this.editorUiManager.editorUi.onKeyPress;
      // Replace original function with ours.
      this.editorUiManager.editorUi.onKeyPress =
        this.editoruiOnkeypressSubstitute;
    }
  }

  public restoreOnKeyPress() {
    if (this.editoruiOnkeypressOriginal !== null) {
      // Restore original function
      this.editorUiManager.editorUi.onKeyPress =
        this.editoruiOnkeypressOriginal;
      this.editoruiOnkeypressOriginal = null;
    }
  }

  static logKeyEvtBuffer(keyEvtWrapperBuffer: Array<KeyboardEventWrapper>) {
    console.log(
      keyEvtWrapperBuffer.map((kew: KeyboardEventWrapper) =>
        typeof kew !== "undefined" && typeof kew.evt !== "undefined"
          ? kew.evt.key
          : undefined
      )
    );
  }

  static isKeyEventAlphaNumeric(keyEvt: KeyboardEvent) {
    return (
      typeof keyEvt !== "undefined" &&
      (EditorUiManager.letters.includes(keyEvt.key) ||
        EditorUiManager.numbers.includes(keyEvt.key))
    );
  }

  handleKeySequence(keyEvt: KeyboardEvent) {
    this.isDebugKeys && KeyManager.logKeyEvtBuffer(this.kewBuffer);
    for (let mode of this.editorUiManager.modes.values()) {
      if (mode.active) {
        const newKew: KeyboardEventWrapper = new KeyboardEventWrapper(
          keyEvt,
          this.editorUiManager.modes
        );
        const returnedKew: KeyboardEventWrapper =
          mode.handleKeySequence(newKew);
        if (returnedKew.hasTriggeredAction) {
          // Reset keyBuffer. Example:
          // 1. A sequence of ["A", "A"] is bound to a
          //    certain function and is triggered
          // 2. If we didn't reset keyBuffer, the next "A"
          //    would trigger the function again
          for (let i = 0; i < this.kewBuffer.length; ++i) {
            this.kewBuffer[i] = undefined;
          }
        }
        return returnedKew;
      }
    }
  }
}
