/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { KeyBinding } from "./KeyBinding";

export interface KeySequenceMap {
  keySeqMap: Array<KeyBinding<KeySequenceMap>>;
}
