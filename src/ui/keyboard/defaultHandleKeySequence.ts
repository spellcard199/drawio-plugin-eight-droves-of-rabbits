/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { arraysEqual } from "../../util/array";
import { EditorUiManager } from "../EditorUiManager";
import { KeyboardEventWrapper } from "./KeyEventWrapper";
import { KeySequenceMap } from "./KeySequenceMap";

export function defaultHandleKeySequence(
  kew: KeyboardEventWrapper,
  ksm: KeySequenceMap,
  editorUiManager: EditorUiManager
): KeyboardEventWrapper {
  var g: mxGraph = editorUiManager.editorUi.editor.graph;
  const kewBuffer: Array<KeyboardEventWrapper> =
    editorUiManager.keyManager.kewBuffer;
  if (g.isEnabled() && !g.isCellLocked(g.getDefaultParent())) {
    // Iterate and match over UI.activeKeyBindings; stop after first match.
    const keyBuffer = kewBuffer.map((kew: KeyboardEventWrapper) =>
      typeof kew !== "undefined" && typeof kew.evt !== "undefined"
        ? kew.evt.key
        : "undefined"
    );

    // Some runtime type-checking before looping over possibly undefined
    // ksm.keySeqMap.
    const checkingType = typeof ksm.keySeqMap;
    if (checkingType === "undefined") {
      console.log("[ERROR] ksm.keySeqMap is undefined (should be an Array).");
      kew.stopPropagation = true;
      return kew;
    } else if (!(ksm.keySeqMap instanceof Array)) {
      console.log("[ERROR] ksm.keySeqMap is not an instance of Array:");
      console.log(
        "- actual constructor name: " + ksm.keySeqMap["constructor"]["name"]
      );
      kew.stopPropagation = true;
      return kew;
    }

    for (var i = 0; i < ksm.keySeqMap.length; i++) {
      const keyBinding = ksm.keySeqMap[i];
      const kbSeqLen = keyBinding.keySequence.length;
      // Compare:
      // - last kbSeqLen elements of keyBuffer
      // - keySequence
      if (arraysEqual(keyBuffer.slice(-kbSeqLen), keyBinding.keySequence)
        && !kew.stopPropagation
      ) {
        keyBinding.command.run(kew, ksm);
        kew.hasTriggeredAction = true;
      }
    }
  }
  return kew;
}
