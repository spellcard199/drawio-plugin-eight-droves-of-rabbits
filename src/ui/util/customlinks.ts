/**
 * Copyright (C) 2021-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import { LinkData } from "../../customlink/LinkData";
import { openDiagramFile } from "../../util/drawio/editorui";
import { EditorUiManager } from "../EditorUiManager";
import { KeyBinding } from "../keyboard/KeyBinding";
import { KeySequenceMap } from "../keyboard/KeySequenceMap";
import { Command } from "../modal/Command";
import { ShowMode } from "../modal/showmode/ShowMode";
import { ViLikeMode } from "../modal/vilikemode/ViLikeMode";

export function openCustomLinksInSelection(editorUiManager: EditorUiManager) {
  const selectionCell: mxCell =
    editorUiManager.editorUi.editor.graph.getSelectionCell();
  if (typeof selectionCell === "undefined") {
    return;
  }
  // Old: delete when sure to remember what I learned here.
  // anchors.forEach((a: HTMLAnchorElement) => {
  //     // When you append it to anchorContainer, "base" path gets replaced
  //     // by editor's document's base.
  //     const clonedAnchor: HTMLAnchorElement = a.cloneNode(true) as HTMLAnchorElement;
  //     clonedAnchor.onclick = async (ev: MouseEvent) => {
  //         LinkOpen.openFile(editorUi, a.href)
  //     }
  //     anchorContainer.appendChild(clonedAnchor);
  // })

  const links: LinkData[] = LinkData.newFromEditorUi(editorUiManager.editorUi, [
    selectionCell,
  ])
    // We are only interedted in links that we can open in diagrams.net
    .filter(
      (ld: LinkData) => ld.attributes.get("type") === "application/drawio"
    );

  const showDiv: HTMLDivElement = document.createElement("div");
  const showMode = ShowMode.getInstanceOrNew(editorUiManager);
  links.forEach((linkData: LinkData) => {
    const a = document.createElement("a");
    // If you set a.href when you click on it it's opened by the browser
    // as a regular xml file: we want diagrams.net to open it as a
    // diagram instead.
    //   a.href = linkData.href
    if (linkData.innerText === "") {
      a.innerText = decodeURI(linkData.href);
    } else {
      a.innerText = linkData.innerText;
    }
    Object.entries(linkData.attributes).forEach(([attrName, attrValue]) =>
      a.setAttribute(attrName, attrValue)
    );
    a.href = linkData.href;
    a.onclick = (ev: MouseEvent) => {
      ev.preventDefault();
      openDiagramFile(editorUiManager.editorUi, linkData.href);
      showMode.quit();
    };
    showDiv.appendChild(a);
    const br = document.createElement("br");
    showDiv.appendChild(br);
  });

  const anchors = showDiv.getElementsByTagName("a");
  var anchorIndex = 0;
  const clUpLink: Command<KeySequenceMap> = {
    name: "",
    doc: "",
    run: (kew, _ksm) => {
      if (anchors.length > 0) {
        anchorIndex = anchorIndex === 0 ? anchors.length - 1 : anchorIndex - 1;
        anchors[anchorIndex].focus();
        kew.stopPropagation = true;
      }
    },
  };
  const clDownLink: Command<KeySequenceMap> = {
    name: "",
    doc: "",
    run: (kew, _ksm) => {
      if (anchors.length > 0) {
        anchorIndex = (anchorIndex + 1) % anchors.length;
        anchors[anchorIndex].focus();
        kew.stopPropagation = true;
      }
    },
  };
  showMode.keySeqMap.push(
    new KeyBinding(["h"], clUpLink),
    new KeyBinding(["l"], clDownLink)
  );
  const viLikeMode = ViLikeMode.getInstance(editorUiManager);
  showMode.show(viLikeMode, showDiv);
  if (anchors.length > 0) {
    anchors[0].focus();
  }
}
