/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

import * as customlink from "./customlink";
import * as ui from "./ui";
import { EditorUiManager } from "./ui/EditorUiManager";
import * as util from "./util";
import { EditorUi } from "./util/types/drawio/EditorUi";

export class DrawioPluginEightDrovesOfRabbits {
  customlink = customlink;
  ui = ui;
  util = util;

  euim: EditorUiManager;
  eui: EditorUi;
  g: mxGraph;

  constructor(euiManager: EditorUiManager) {
    this.euim = euiManager;
    this.eui = euiManager.editorUi;
    this.g = euiManager.editorUi.editor.graph;
  }

  static loadPlugin(): DrawioPluginEightDrovesOfRabbits | undefined {
    let edr: DrawioPluginEightDrovesOfRabbits;
    //@ts-ignore
    Draw.loadPlugin(function (editorUi: any) {
      if (window) {
        if (window["edr"]) {
          (window["edr"] as DrawioPluginEightDrovesOfRabbits).euim.quit();
        }
        edr = new DrawioPluginEightDrovesOfRabbits(
          EditorUiManager.new(editorUi)
        );
        window["edr"] = edr;
        edr.euim.enable();
      }
    });
    return edr;
  }
}
