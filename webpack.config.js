/**
 * Copyright (C) 2020-2023, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: Apache-2.0
 */

const path = require("path");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");

module.exports = {
  mode: "development",
  target: "electron-main", // needed if you use `require("electron")'
  // target: "web",
  // TODO: eventually move to "web" target when we can live without
  // adding to drawio's electron-preload.js:
  //   contextBridge.exposeInMainWorld(
  //      'require', require,
  //   );
  entry: "./src/index.ts",
  devtool: "inline-source-map",
  externals: {
    canvas: {},
    bufferutil: {},
    "utf-8-validate": {},
    "file-loader": {},
    jest: {},
    jsdom: {},
    mxgraph: {},
    "mxgraph-type-definitions": {},
    pako: {},
    "ts-jest": {},
    "ts-loader": {},
    typescript: {},
  },
  optimization: {
    // https://stackoverflow.com/questions/42300147/how-to-disable-in-webpack-to-rename-of-function-names-typescript-javascript
    minimize: false,
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [{ loader: "ts-loader", options: { transpileOnly: true } }],
        // exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist"),
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin({
      eslint: true
    }),
  ],
  // To manually enable use:
  // node_modules/webpack-cli/bin/cli.js --watch
  // --progress --info-verbosity verbose
  // TODO: ignore emacs temp files
  watch: false,
  watchOptions: {
    aggregateTimeout: 200,
    poll: 1000,
    ignored: ["dest/**/*.js", "node_modules/"],
  },
};
